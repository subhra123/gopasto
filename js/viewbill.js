var viewType = 1;

function selectViewField(){
	
	clearField();
	
	var value = document.getElementById("viewtype").value;
	viewType = value;
	if(viewType == 1)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 2)
	{
		document.getElementById("dateid").style.display = "block";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 3)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "block";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 4)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "block";
	}
	
	
	}
	
	
function searchBillOnClick(){
	var startdate="";
	var enddate="";
	var billno="";
	if(viewType == 2 ){
		var value = document.getElementById("datefieldid").value;
		var avalue = document.getElementById("hiddatefieldid").value;
		if(value.length == 0 ){
			alert("Please select a date...");
			return false;
		}
		startdate = avalue;
	}
	
	if(viewType == 3 ){
		var value1 = document.getElementById("startdatefieldid").value;
		var value2 = document.getElementById("enddatefieldid").value;
		var avalue1 = document.getElementById("hidstartdatefieldid").value;
		var avalue2 = document.getElementById("hidenddatefieldid").value;
		
		if(value1.length == 0 ){
			alert("Please select Start date...");
			return false;
		}
		if(value2.length == 0 ){
			alert("Please select End date...");
			return false;
		}
		startdate = avalue1;
		enddate = avalue2;
	}
	
	if(viewType == 4 ){
		var value1 = document.getElementById("billnofield").value;
		if(value1.length == 0 ){
			alert("Please enter bill no...");
			return false;
		}
		billno = value1;
	}
	
	
	   var dataString = "act=getbill&viewtype="+viewType+"&startdate="+startdate+"&enddate="+enddate+"&billno="+billno;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 0 )
			{
				var error = dobj.error;
				if(error.length > 0)
					alert(""+error);
			}
			else
			{
				updateField(dobj);
			}
		} 
		});
	
	}
	
	
function updateField(dobj){
	clearField();
	$("#detailsstockid").html(dobj.data);
	
	}
	
function clearField(){
	
	$("#detailsstockid").html("");
	
	}
	
function updateDetailBill(id){
	
	var dataString = "act=sessiondata&id="+id;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{
		return true;		
	} 
	});
	
}	


function printOnClick(){
	displayFade();
	var str="";
	
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	if(rowCount == 1){
		showWarningDialog("No record found to print... ");
		return false;
	}else if(rowCount == 2){
		var value = table.rows[1].cells[0].innerHTML;
		if(value == "No records found"){
			showWarningDialog("No record found to print... ");
			return false;
		}
	}
	
	var type = document.getElementById('viewtype').value;
	if(type == 1){
		str = "For All";
	}else if(type == 2){
		var value = document.getElementById("datefieldid").value;
		var avalue = document.getElementById("hiddatefieldid").value;
		str = "For Date: " +value;
	}else if(type == 3){
		var value1 = document.getElementById("startdatefieldid").value;
		var value2 = document.getElementById("enddatefieldid").value;
		str = "From " +value1+"  to  "+value2;
	}else if(viewType == 4 ){
		var value1 = document.getElementById("billnofield").value;
		str = "For Bill No	 : " +value1;
	}
	
	document.getElementById("totalitemid").value = ""+rowCount;
	
	document.getElementById('printbillconfirm').style.display = "block";
	document.getElementById('printbillmgsid').innerHTML = str;
	
	}


function printBill(){
	closeBillDialog();
	showLoading();
	addHiddenFields();
	var surl="printBillReport.php";
	
	var sdata=$( "#billdata" ).serialize();
	//alert(""+sdata);
	$.post( surl, sdata, function(data){
		closeLoading();
		var dobj=jQuery.parseJSON(data);
		
		printPdf(dobj.filename);
		//printBill(dobj.data);
		//showSucessDialog(dobj);
		//reloadPage();
		
	});
	}


function printPdf(filename){
	var w = window.open(filename);
	w.print();
}	


function closeBillDialog(){
	removeFade();
	document.getElementById('printbillconfirm').style.display="none";
	}
	
function showWarningDialog(str_value){
		displayFade();
		document.getElementById('opendialog').value = "viewdialog";
		document.getElementById('viewdialog').style.display="block";
		document.getElementById('warningbodyid').innerHTML = str_value;
		
		}

function closeWarningDialog(){
		removeFade();
		document.getElementById('opendialog').value = "";
		document.getElementById('viewdialog').style.display="none";
		document.getElementById('warningbodyid').innerHTML = "";
		}



function addHiddenFields(){
	var tbody = document.getElementById("hiddenfieldid");
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	
	for(i=1; i< rowCount;i++)
	{
		var date_time =  table.rows[i].cells[1].innerHTML;
		var billno =  table.rows[i].cells[2].innerHTML;
		var tqty =  table.rows[i].cells[3].innerHTML;
		var tsale =  table.rows[i].cells[4].innerHTML;
		var tcost =  table.rows[i].cells[5].innerHTML;
		var tprofit =  table.rows[i].cells[6].innerHTML;
		
		new_val = '<input type="hidden" id="datetime'+i+'"  name="datetime'+i+'" value="'+date_time+'">';
		new_val += '<input type="hidden" id="billno'+i+'"  name="billno'+i+'" value="'+billno+'">';
		new_val += '<input type="hidden" id="tqty'+i+'"  name="tqty'+i+'" value="'+tqty+'">';
		new_val += '<input type="hidden" id="tsale'+i+'"  name="tsale'+i+'" value="'+tsale+'">';
		new_val += '<input type="hidden" id="tcost'+i+'"  name="tcost'+i+'" value="'+tcost+'">';
		new_val += '<input type="hidden" id="tprofit'+i+'"  name="tprofit'+i+'" value="'+tprofit+'">';
		//alert(""+date_time);
		tbody.innerHTML = tbody.innerHTML+new_val;
	}
	
	
	}