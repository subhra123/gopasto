var viewType = 1;

	
	
	

function printOnClick(){
	displayFade();
	var str="";
	
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	if(rowCount == 1){
		showWarningDialog("No record found to print... ");
		return false;
	}else if(rowCount == 2){
		var value = table.rows[1].cells[0].innerHTML;
		if(value == "No records found"){
			showWarningDialog("No record found to print... ");
			return false;
		}
	}
	//alert(""+rowCount);
	document.getElementById("totalitemid").value = ""+rowCount;
	document.getElementById('printbillconfirm').style.display = "block";
	document.getElementById('printbillmgsid').innerHTML = "Would you like to print stock report?";
	
	}


function printBill(){
	closeBillDialog();
	showLoading();
	addHiddenFields();
	var surl="printStockReport.php";
	var sdata=$( "#stockdata" ).serialize();
	$.post( surl, sdata, function(data){
		closeLoading();
		var dobj=jQuery.parseJSON(data);
		printPdf(dobj.filename);
		//printBill(dobj.data);
		//showSucessDialog(dobj);
		//reloadPage();
		
	});
	}


function printPdf(filename){
	var w = window.open(filename);
	w.print();
}	


function closeBillDialog(){
	removeFade();
	document.getElementById('printbillconfirm').style.display="none";
	}
	
function showWarningDialog(str_value){
		displayFade();
		document.getElementById('opendialog').value = "viewdialog";
		document.getElementById('viewdialog').style.display="block";
		document.getElementById('warningbodyid').innerHTML = str_value;
		
		}

function closeWarningDialog(){
		removeFade();
		document.getElementById('opendialog').value = "";
		document.getElementById('viewdialog').style.display="none";
		document.getElementById('warningbodyid').innerHTML = "";
		}



function addHiddenFields(){
	var tbody = document.getElementById("hiddenfieldid");
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	
	for(i=1; i< rowCount;i++)
	{
		var item_code =  table.rows[i].cells[1].innerHTML;
		var item_name =  table.rows[i].cells[2].innerHTML;
		var ucp =  table.rows[i].cells[3].innerHTML;
		var usp =  table.rows[i].cells[4].innerHTML;
		var qty =  table.rows[i].cells[5].innerHTML;
		
		new_val = '<input type="hidden" id="itemcode'+i+'"  name="itemcode'+i+'" value="'+item_code+'">';
		new_val += '<input type="hidden" id="itemname'+i+'"  name="itemname'+i+'" value="'+item_name+'">';
		new_val += '<input type="hidden" id="ucp'+i+'"  name="ucp'+i+'" value="'+ucp+'">';
		new_val += '<input type="hidden" id="usp'+i+'"  name="usp'+i+'" value="'+usp+'">';
		new_val += '<input type="hidden" id="qty'+i+'"  name="qty'+i+'" value="'+qty+'">';
		//alert(""+new_val);
		tbody.innerHTML = tbody.innerHTML+new_val;
	}
	
	
	}