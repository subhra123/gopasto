var viewType = 1;

function selectViewField(){
	
	clearField();
	
	var value = document.getElementById("viewtype").value;
	viewType = value;
	if(viewType == 1)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 2)
	{
		document.getElementById("dateid").style.display = "block";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 3)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "block";
		document.getElementById("billnofieldid").style.display = "none";
	}
	else if(viewType == 4)
	{
		document.getElementById("dateid").style.display = "none";
		document.getElementById("daterangeid").style.display = "none";
		document.getElementById("billnofieldid").style.display = "block";
	}
	
	
	}
	
	
function searchBillOnClick(){
	
	
	
	 
	
	}
	
	
function updateField(dobj){
	clearField();
	$("#detailsstockid").html(dobj.data);
	
	}
	
function clearField(){
	
	$("#detailsstockid").html("");
	
	}
	
function updateDetailBill(id){
	
	var dataString = "act=sessiondata&id="+id;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{
		return true;		
	} 
	});
	
}	


function printOnClick(){
	var startdate="";
	var enddate="";
	var billno="";
	
	var value1 = document.getElementById("startdatefieldid").value;
	var value2 = document.getElementById("enddatefieldid").value;
	var avalue1 = document.getElementById("hidstartdatefieldid").value;
	var avalue2 = document.getElementById("hidenddatefieldid").value;
		
	if(value1.length == 0 ){
		alert("Please select Start date...");
		return false;
	}
	if(value2.length == 0 ){
		alert("Please select End date...");
		return false;
	}
	startdate = avalue1;
	enddate = avalue2;
	document.getElementById('reportstartdate').value = startdate;
	document.getElementById('reportenddate').value = enddate;
	
	var str= "Start Date : "+value1+"<BR> End Date : "+value2;
	displayFade();
	document.getElementById('printbillconfirm').style.display = "block";
	document.getElementById('printbillmgsid').innerHTML = str;
	
	}


function printBill(){
	//alert("printBill");
	closeBillDialog();
	showLoading();
	//addHiddenFields();
	var surl="generatedBillReport.php";
	var sdata=$( "#billdata" ).serialize();
	//alert(""+sdata);
	$.post( surl, sdata, function(data){
		closeLoading();
		var dobj=jQuery.parseJSON(data);
		$("#detailsstockid").html(dobj.data);
		//alert("Done");
		//var dobj=jQuery.parseJSON(data);
		
		//printPdf(dobj.filename);
		//printBill(dobj.data);
		//showSucessDialog(dobj);
		//reloadPage();
		
	});
	}


function printPdf(id){
	var dataString = "act=getSaleFilename&id="+id;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{
		var dobj=jQuery.parseJSON(html);
		var w = window.open(dobj.filename);
		w.print();	
	} 
	});
	
}	


function closeBillDialog(){
	removeFade();
	document.getElementById('printbillconfirm').style.display="none";
	}
	
function showWarningDialog(str_value){
		displayFade();
		document.getElementById('opendialog').value = "viewdialog";
		document.getElementById('viewdialog').style.display="block";
		document.getElementById('warningbodyid').innerHTML = str_value;
		
		}

function closeWarningDialog(){
		removeFade();
		document.getElementById('opendialog').value = "";
		document.getElementById('viewdialog').style.display="none";
		document.getElementById('warningbodyid').innerHTML = "";
		}



function addHiddenFields(){
	var tbody = document.getElementById("hiddenfieldid");
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	alert("::"+rowCount);
	for(i=1; i< rowCount;i++)
	{
		var date_time =  table.rows[i].cells[1].innerHTML;
		var billno =  table.rows[i].cells[2].innerHTML;
		var tqty =  table.rows[i].cells[3].innerHTML;
		var tsale =  table.rows[i].cells[4].innerHTML;
		var tcost =  table.rows[i].cells[5].innerHTML;
		var tprofit =  table.rows[i].cells[6].innerHTML;
		
		new_val = '<input type="hidden" id="datetime'+i+'"  name="datetime'+i+'" value="'+date_time+'">';
		new_val += '<input type="hidden" id="billno'+i+'"  name="billno'+i+'" value="'+billno+'">';
		new_val += '<input type="hidden" id="tqty'+i+'"  name="tqty'+i+'" value="'+tqty+'">';
		new_val += '<input type="hidden" id="tsale'+i+'"  name="tsale'+i+'" value="'+tsale+'">';
		new_val += '<input type="hidden" id="tcost'+i+'"  name="tcost'+i+'" value="'+tcost+'">';
		new_val += '<input type="hidden" id="tprofit'+i+'"  name="tprofit'+i+'" value="'+tprofit+'">';
		//alert(""+date_time);
		tbody.innerHTML = tbody.innerHTML+new_val;
	}
	
	
	}
	
function deleteRecord(id){
	displayFade();
	document.getElementById('deleteid').value = id;
	document.getElementById('confirmdialog').style.display = "block";
	document.getElementById('confirmdialogbody').innerHTML = "Do you want to Delete Record ?";
	}
	
function actionConfirmDialog(){
	closeConfirmDialog();
	showLoading();
	var id = document.getElementById('deleteid').value;
	var dataString = "act=deleteSaleRecord&id="+id;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{
		closeLoading();
		var dobj=jQuery.parseJSON(html);
		//alert(""+dobj.data);
		$("#detailsstockid").html(dobj.data);
		
	} 
	});
	
	}
	
	
function closeConfirmDialog(){
		removeFade();
		document.getElementById('confirmdialog').style.display="none";
		document.getElementById('confirmdialogbody').innerHTML = "";
	}	
	