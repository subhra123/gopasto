
window.onload = function(){
  var text_input = document.getElementById ('itemsearch');
  text_input.focus ();
  text_input.select ();
}

function calculateUSP(){
	
	var ucp =parseFloat(document.getElementById('popnewitemucp').value,10).toFixed(4);
	var profit = parseFloat(document.getElementById('hdprofit').value,10).toFixed(4);
	var profit_value = parseFloat(ucp*(profit/100),10).toFixed(4);
	
	var usp = parseFloat(ucp,10) + parseFloat(profit_value,10) ;
	//usp  = Math.ceil(usp);
	usp = usp.toFixed(4);
	document.getElementById('popnewitemusp').value = usp;
	
	}


function calculateEditStockUSP(){
	
	var ucp =parseFloat(document.getElementById('editstockitemucp').value,10).toFixed(4);
	var profit = parseFloat(document.getElementById('hdprofit').value,10).toFixed(4);
	var profit_value = parseFloat(ucp*(profit/100),10).toFixed(4);
	
	var usp = parseFloat(ucp,10) + parseFloat(profit_value,10) ;
	//usp  = Math.ceil(usp);
	usp = usp.toFixed(4);
	document.getElementById('editstockitemusp').value = usp;
	
	}



function calculateUSP2(){
	
	var ucp =parseFloat(document.getElementById('itemucp').value,10).toFixed(4);
	var profit = parseFloat(document.getElementById('hdprofit').value,10).toFixed(4);
	var profit_value = parseFloat(ucp*(profit/100),10).toFixed(4);
	
	var usp = parseFloat(ucp,10) + parseFloat(profit_value,10) ;
	//usp  = Math.ceil(usp);
	usp = usp.toFixed(4);
	document.getElementById('itemusp').value = usp;
	
	}
	
	
function searchItemOnClick()
{
	resetFields();
	var itemno =document.getElementById('itemsearch').value;
		if(itemno.length == 0)
		{
			alert("Add Item code...");
			return false;
		}
		
		var dataString = "act=checkitemno&itemno="+itemno;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 0 )
			{
				alert("No Item found...");
			}
			else
			{
				updateField(dobj);
			}
		} 
		});
		
        return false;
}

function searchItem(e){
	
	 if(e.keyCode == 13) {
		return searchItemOnClick();
    }

}

function updateField(dobj)
{
	//document.getElementById('itemname').readOnly=false;
	document.getElementById('itemucp').readOnly=false;
	//document.getElementById('itemusp').readOnly=false;
	document.getElementById('itemnewquantity').readOnly=false;
	
	document.getElementById('itemname').value = dobj.name;
	document.getElementById('itemucp').value = dobj.ucp;
	document.getElementById('itemusp').value = dobj.usp;
	document.getElementById('itemquantity').value = dobj.quantity;
	document.getElementById('itemnewquantity').value = "1";
	//$("#detailsstockid").html(dobj.data);
		
	 var text_input = document.getElementById ('itemnewquantity');
  	 text_input.focus ();
  	 text_input.select ();
}

function oditekClear()
{
	document.getElementById('itemname').readOnly=true;
	document.getElementById('itemucp').readOnly=true;
	document.getElementById('itemusp').readOnly=true;
	document.getElementById('itemnewquantity').readOnly=true;


	document.getElementById ('itemsearch').value = "";
	document.getElementById('itemname').value = "";
	document.getElementById('itemucp').value = "";
	document.getElementById('itemusp').value = "";
	document.getElementById('itemquantity').value = "";
	document.getElementById('itemnewquantity').value = "";
	var text_input = document.getElementById ('itemsearch');
	//$("#detailsstockid").html("");
  	text_input.focus ();
  	text_input.select ();
}

function resetFields()
{
	document.getElementById('itemname').readOnly=true;
	document.getElementById('itemucp').readOnly=true;
	document.getElementById('itemusp').readOnly=true;
	document.getElementById('itemnewquantity').readOnly=true;


	document.getElementById('itemname').value = "";
	document.getElementById('itemucp').value = "";
	document.getElementById('itemusp').value = "";
	document.getElementById('itemquantity').value = "";
	//$("#detailsstockid").html("");
}

function addItemOnClick()
{
	var itemno =document.getElementById('itemsearch').value;
		//alert("1111");
		if(!checkValidation())
		{
			return false;
		}
		//alert("2222");
		var item_name = document.getElementById('itemname').value;
		var item_ucp  = document.getElementById('itemucp').value;
		var item_usp  = document.getElementById('itemusp').value;
		var item_newquantity  = document.getElementById('itemnewquantity').value;
		var dataString = "act=additem&itemno="+itemno+"&itemname="+item_name+"&itemucp="+item_ucp+"&itemusp="+item_usp+"&newquantity="+item_newquantity;
		//alert("dataString:"+dataString);
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			if(dobj.result == 0 )
			{
				
				var text_input = document.getElementById ('itemsearch');
  				text_input.focus ();
  				text_input.select ();
				alert("No Item found...");
			}
			else
			{
				//alert(""+dobj.data);
				$("#detailsstockid").html(dobj.data);
				document.getElementById('totalcp').value = dobj.tcp;
				document.getElementById('totalsp').value = dobj.tsp;
				document.getElementById('totalqty').value = dobj.tqty;
				//alert("Added Successfully...");
				
				oditekClear();
			}
		} 
		});
		
        return false;
}


function addItem(e){
	
	 if(e.keyCode == 13) {
		return addItemOnClick();
    }

}


function checkValidation()
{
	var item_code = document.getElementById ('itemsearch').value;
	var item_name = document.getElementById('itemname').value;
	var item_ucp  = document.getElementById('itemucp').value;
	var item_usp  = document.getElementById('itemusp').value;
	var item_newquantity  = document.getElementById('itemnewquantity').value;
	if(item_code.length == 0 )
	{
		alert("Item Code should no be empty...");
		return false;	
	}
	
	if(item_name.length == 0 )
	{
		alert("Item Name should not be empty...");
		return false;	
	}
	
	if(item_ucp.length == 0 )
	{
		alert("Item Unit Cost Price should not be empty...");
		return false;	
	}
	
	if(item_usp.length == 0 )
	{
		alert("Item Unit Sale Price should not be empty...");
		return false;	
	}
	
	if(item_newquantity.length == 0 )
	{
		alert("Item New Quantity should not be empty...");
		return false;	
	}
	
	if(item_ucp > item_usp)
	{
		alert("Unit Sale Price should not be less than Unit Cost Price...");
		return false;
	}
	
	
	return true;
		
	
}


function showDialog(){
	displayFade();
	document.getElementById('addnewstock').style.display="block";
	var text_input = document.getElementById('popnewitemadd');
  	text_input.focus ();
  	text_input.select ();
	text_input.value="";
	return true;
	}
	
function showEditDialog(){
	displayFade();
	document.getElementById('editname').style.display="block";
	var text_input = document.getElementById('popedititemcode');
  	text_input.focus ();
  	text_input.select ();
	text_input.value="";
	return true;
	}	
	
	
 function updatepage(){
	resetPopup();
	var text_input = document.getElementById ('itemsearch');
  	text_input.focus ();
  	text_input.select ();
	 }
	 
function checkItem(e){
	var name_input = document.getElementById('popnewitemname');
	var text_input = document.getElementById('popnewitemadd');
	var usp_input = document.getElementById('popnewitemusp');
	var ucp_input = document.getElementById('popnewitemucp');
	var quantity_input = document.getElementById('popnewitemquantity');
	name_input.value="";
	usp_input.value="0.0000";
	ucp_input.value="0.0000";
	
	 if(e.keyCode == 13) {
		 
		var itemno =document.getElementById('popnewitemadd').value;
		itemno = itemno.trim();
		
		
		if(itemno.length == 0)
		{
			alert("Add/Scan Item code...");
			return false;
		}
		
		if(!parseInt(itemno, 10))
		{
			alert("Add/Scan Integer value only...");
			return false;
		}
		
		var dataString = "act=checkitemno&itemno="+itemno;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 0 )
			{
				text_input.readOnly=true;
				name_input.readOnly=false;
				ucp_input.readOnly=false;
				//usp_input.readOnly=false;
				quantity_input.readOnly=false;
				name_input.focus ();
  				name_input.select ();
			}
			else
			{
				alert("Item code already exists...");
				name_input.readOnly=true;
			}
		} 
		});
  	
	text_input.focus ();
  	text_input.select ();
		
    }

}


function checkItemToEdit(e){
	 if(e.keyCode == 13) {
		var itemno =document.getElementById('popedititemcode').value;
		var name_input = document.getElementById('popedititemname');
		if(itemno.length == 0)
		{
			alert("Add/Scan Item code...");
			return false;
		}
		
		var dataString = "act=checkitemno&itemno="+itemno;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 1 )
			{
				name_input.value= dobj.name;
				name_input.readOnly=false;
				name_input.focus ();
  				name_input.select ();
			}
			else
			{
				alert("Item code does not exists...");
			}
		} 
		});
  	
	text_input.focus ();
  	text_input.select ();
		
    }

}



function saveNewItemOnClick(){
	
		var itemno =document.getElementById('popnewitemadd').value;
		var itemname =document.getElementById('popnewitemname').value;
		var itemucp =document.getElementById('popnewitemucp').value;
		var itemusp =document.getElementById('popnewitemusp').value;
		var itemqty =document.getElementById('popnewitemquantity').value;
		itemucp = parseFloat(itemucp,10).toFixed(4);
		itemusp = parseFloat(itemusp,10).toFixed(4);
		
		if(itemname.length == 0)
		{
			alert("Add Item name...");
			return false;
		}
		
		var dataString = "act=saveNewItemno&itemno="+itemno+"&itemname="+itemname+"&itemucp="+itemucp+"&itemusp="+itemusp+"&itemqty="+itemqty;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			$("#detailsstockid").html(dobj.data);
			document.getElementById('totalcp').value = dobj.tcp;
			document.getElementById('totalsp').value = dobj.tsp;
			document.getElementById('totalqty').value = dobj.tqty;
			resetPopup();
			alert("Added Succesfully...");
		} 
		});

}

function saveNewItem(e){
	
	if(e.keyCode == 13) {
		 saveNewItemOnClick();
	}

}


function addMyListToStock(){
	
	var total_cost_price =document.getElementById('totalcp').value;
	var total_sale_price =document.getElementById('totalsp').value;
	var total_qty =document.getElementById('totalqty').value;
	if(total_cost_price == "")
	{
		alert("Add Item...");
		return false;
	}
	displayFade();
	document.getElementById('addstockconfirm').style.display = "block";
	document.getElementById('newstocktcost').value = total_cost_price;
	document.getElementById('newstocktsale').value = total_sale_price;
	document.getElementById('newstocktqty').value = total_qty;
	
	}

function addToStock(){
	showLoading();
	var total_cost_price =document.getElementById('totalcp').value;
	var total_sale_price =document.getElementById('totalsp').value;
	var total_qty =document.getElementById('totalqty').value;
	
	var dataString = "act=addtoStock&tcp="+total_cost_price+"&tsp="+total_sale_price+"&tqty="+total_qty;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{ 
		closeLoading();
		clearAll();
		closeAddStockDialog();
		alert("Successfully added all new stocks...");
	} 
	});
	
	}

	 
function resetPopup(){
	clearNewItemOnClick();
	
	}

function clearNewItemOnClick(){
	var name_input = document.getElementById('popnewitemname');
	var text_input = document.getElementById('popnewitemadd');
	var usp_input = document.getElementById('popnewitemusp');
	var ucp_input = document.getElementById('popnewitemucp');
	var quantity_input = document.getElementById('popnewitemquantity');
	
	text_input.readOnly=false;
	name_input.readOnly=true;
	ucp_input.readOnly=true;
	usp_input.readOnly=true;
	quantity_input.readOnly=true;
	text_input.value = "";
	name_input.value = "";
	ucp_input.value = "0.0000";
	usp_input.value = "0.0000";
	quantity_input.value = "1";
	text_input.focus ();
  	text_input.select ();
	
	}
	
function closeNewItemDialog(){
	removeFade();
	clearNewItemOnClick();
	document.getElementById('addnewstock').style.display="none";
	var text_input = document.getElementById('itemsearch');
  	text_input.focus ();
  	text_input.select ();
	}


	
function clearAll(){	
	clearNewItemOnClick();
	document.getElementById('totalcp').value = "";
	document.getElementById('totalsp').value = "";
	document.getElementById('totalqty').value = "";
	$("#detailsstockid").html("");
}


function closeAddStockDialog(){
	removeFade();
	document.getElementById('addstockconfirm').style.display = "none";
	var text_input = document.getElementById ('itemsearch');
 	text_input.focus ();
  	text_input.select ();
	
	}

function saveEditItem(e){
	if(e.keyCode == 13) {
		 saveEditItemOnClick();
	}
}


function saveEditItemOnClick(){
	
	var code = document.getElementById('popedititemcode').value;
	var name = document.getElementById('popedititemname').value;
		if(code.length == 0 && name.length == 0){
			alert("Scan Item or Enter Item Code...");
			return false;
		}
		else if(name.length == 0){
			alert("Enter Item Name...");
			return false;
		}
		showLoading();
		var dataString = "act=editName&code="+code+"&name="+name;
		//alert(dataString);
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			closeLoading();
			closeEditNameDialog();
			alert("Updated Item Name...");
		} 
		});
		
	}
	
function closeEditNameDialog(){
	removeFade();
	document.getElementById('popedititemname').value="";
	document.getElementById('popedititemname').readOnly ="true";
	document.getElementById('editname').style.display = "none";
	var text_input = document.getElementById ('itemsearch');
 	text_input.focus ();
  	text_input.select ();
	}	
	
	
function actionEdit(id , db_id){
	var table = document.getElementById("dataTable");
	var code =  table.rows[id].cells[2].innerHTML;
	var name =  table.rows[id].cells[3].innerHTML;
	var ucp =  table.rows[id].cells[4].innerHTML;
	var usp =  table.rows[id].cells[5].innerHTML;
	var qty =  table.rows[id].cells[6].innerHTML;
	
	displayFade();
	document.getElementById('editstock').style.display = "block";
	document.getElementById('editstockLabel').innerHTML = "Edit Stock";
	document.getElementById('hideditstocktype').value = "EDIT";
	document.getElementById('editstockitemcode').value = code;
	document.getElementById('editstockitemname').value = name;
	document.getElementById('editstockitemucp').value = ucp;
	document.getElementById('editstockitemusp').value = usp;
	document.getElementById('editstockitemqty').value = qty;
	document.getElementById('hideditstockitemucp').value = ucp;
	document.getElementById('hideditstockitemusp').value = usp;
	document.getElementById('hideditstockitemqty').value = qty;
	document.getElementById('hiddbid').value = db_id;
	
	
	var text_input = document.getElementById ('editstockitemucp');
 	text_input.focus ();
  	text_input.select ();
	
}

function actionDelete(id , db_id){
	
	var table = document.getElementById("dataTable");
	var code =  table.rows[id].cells[2].innerHTML;
	var name =  table.rows[id].cells[3].innerHTML;
	var ucp =  table.rows[id].cells[4].innerHTML;
	var usp =  table.rows[id].cells[5].innerHTML;
	var qty =  table.rows[id].cells[6].innerHTML;
	
	displayFade();
	document.getElementById('editstock').style.display = "block";
	document.getElementById('editstockLabel').innerHTML = "Delete Stock";
	document.getElementById('hideditstocktype').value = "DELETE";
	document.getElementById('editstockitemcode').value = code;
	document.getElementById('editstockitemname').value = name;
	document.getElementById('editstockitemucp').value = ucp;
	document.getElementById('editstockitemusp').value = usp;
	document.getElementById('editstockitemqty').value = qty;
	document.getElementById('hiddbid').value = db_id;
	document.getElementById('editstockitemucp').readOnly = "true";
	document.getElementById('editstockitemusp').readOnly = "true";
	document.getElementById('editstockitemqty').readOnly = "true";
}




function saveEditStockOnClick(){
	
	var action_type = document.getElementById('hideditstocktype').value;
	var db_id = document.getElementById('hiddbid').value;
	//alert(""+action_type);
	//var table = document.getElementById("dataTable");
	
	//var old_ucp = document.getElementById('hideditstockitemucp').value;
	//var old_usp = document.getElementById('hideditstockitemusp').value;
	//var old_qty = document.getElementById('hideditstockitemqty').value;
	//old_ucp =parseFloat(old_ucp,10).toFixed(4);
	//old_usp =parseFloat(old_usp,10).toFixed(4);
	//old_qty =parseInt(old_qty,10);
	//var t_old_ucp = old_ucp * old_qty;
	//var t_old_usp = old_usp * old_qty;
	
	
	var ucp = document.getElementById('editstockitemucp').value;
	var usp = document.getElementById('editstockitemusp').value;
	var qty = document.getElementById('editstockitemqty').value;
	//ucp =parseFloat(ucp,10).toFixed(4);
	//usp =parseFloat(usp,10).toFixed(4);
	//qty =parseInt(qty,10);
	//var t_ucp = ucp * qty;
	//var t_usp = usp * qty;
	
	//var tucp = document.getElementById('totalcp').value;
	//var tusp = document.getElementById('totalsp').value;
	//var tqty = document.getElementById('totalqty').value;
	//alert(""+tucp);
	//tucp = parseFloat(tucp,10).toFixed(2);
	//tusp = parseFloat(tusp,10).toFixed(2);
	//tqty =parseInt(tqty,10);
	
	//remove old
	//tucp = tucp - t_old_ucp;
	//tusp = tusp - t_old_usp;
	//tqty = tqty - old_qty;
	
	//add new 
	//tucp = tucp + t_ucp;
	//tusp = tusp + t_usp;
	//tqty = tqty + qty;
	//tucp = tucp.toFixed(0);
	//tusp = tusp.toFixed(0);
	
	
	//document.getElementById('totalcp').value = tucp;
	//document.getElementById('totalsp').value = tusp;
	//document.getElementById('totalqty').value = tqty;
	
	//table.rows[id].cells[4].innerHTML = ucp;
	//table.rows[id].cells[5].innerHTML = usp;
	//table.rows[id].cells[6].innerHTML = qty;
	
	if(action_type == "EDIT" )
	{
		var dataString = "act=updateMyStock&id="+db_id+"&ucp="+ucp+"&usp="+usp+"&qty="+qty;
		//alert(dataString);
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html);
			$("#detailsstockid").html(dobj.data);
			document.getElementById('totalcp').value = dobj.tcp;
			document.getElementById('totalsp').value = dobj.tsp;
			document.getElementById('totalqty').value = dobj.tqty;
			closeEditStockDialog();
		} 
		});
	}
	else {
		
		var dataString = "act=deleteMyStock&id="+db_id;
		//alert(""+dataString);
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{	 
			var dobj=jQuery.parseJSON(html);
			$("#detailsstockid").html(dobj.data);
			document.getElementById('totalcp').value = dobj.tcp;
			document.getElementById('totalsp').value = dobj.tsp;
			document.getElementById('totalqty').value = dobj.tqty;
			closeEditStockDialog();
		}	 
		});
	}
	
}


function closeEditStockDialog(){
		removeFade();
		document.getElementById('editstock').style.display = "none";
		var text_input = document.getElementById ('itemsearch');
 		text_input.focus ();
  		text_input.select ();
	}