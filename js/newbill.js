var old_quantity=0;
var old_price=0;
var old_actual_price=0;
var old_profit=0;
var fade = 1;

window.onload = function(){
  selectItemCode();
}


function selectItemCode(){
	var text_input = document.getElementById ('itemsearch');
  	text_input.focus ();
  	text_input.select ();
	}
	
function searchItemOnClick()
{
	resetFields();
	var itemno =document.getElementById('itemsearch').value;
	itemno = itemno.trim();
		if(itemno.length == 0)
		{
			alert("Add Item code...");
			return false;
		}
		
		var dataString = "act=addtoBillItemno&itemno="+itemno;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 0 )
			{
				showWarningDialog("No item found... ");
			}
			else
			{
				updateField(dobj);
			}
		} 
		});
		
        return false;
}



function searchItemName(){
	var htmlvalue  = document.getElementById ('chosen-single-id').innerHTML;
	var text = $(htmlvalue).text();
	//alert("aaaaa"+text);
	if(text.length > 0)
	{
		var dataString = "act=getItemCode&itemname="+text;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			if(dobj.result == 0 )
			{
				showWarningDialog("No item found... ");
			}
			else
			{
				var code = dobj.code;
				//alert(""+code);
				document.getElementById('itemsearch').value = code ;
				searchItemOnClick();
			}
		} 
		});
		
        return false;
	}
	
	}
	
	

function searchItem(e){
	
		if(checkAndcloseDialog())
		{
	 		if(e.keyCode == 13) {
				return searchItemOnClick();
		}
    }

}

function updateField(dobj)
{
	if(dobj.count == 0)
	{
		var item_name = dobj.name;
		///alert("No stock found of Item "+item_name);
		showWarningDialog("Available stock of \""+item_name+"\" is 0");
		return;
	}
	else if(dobj.count == 1)
	{
		var item_name = dobj.name;
		var item_code = dobj.code;
		var item_id = dobj.id;
		var item_ucp = dobj.ucp;
		var item_usp = dobj.usp;
		var item_avail_quantity = dobj.quantity;
		
		var avail_quantity = checkActualAvailQuantity(item_code,item_ucp,item_usp,item_avail_quantity);
		if(avail_quantity > 0)
		{
			//alert(""+item_ucp);
			document.getElementById('itemname').value = item_name;
			document.getElementById('itemusp').value = item_usp;
			document.getElementById('hitemucp').value = item_ucp;
			document.getElementById('havailquantity').value = item_avail_quantity;
			document.getElementById('hitemid').value = item_id;
			document.getElementById('itemnewquantity').value = 1;
			
			addItemOnClick();
		}
		else
		{
			showWarningDialog("Available stock of \""+item_name+"\" is 0");
		}
		//var text_input = document.getElementById('itemnewquantity');
  		//text_input.focus ();
  		//text_input.select ();
	}
	else
	{
		var item_code = dobj.code;
		var text_input = document.getElementById ('itemsearch');
  		text_input.blur();
  		$("#stockdetailslistid").html("");
		
		var dataString = "act=mutipleListCount&itemno="+item_code;
		$.ajax({ 
		type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			var dobj=jQuery.parseJSON(html)
			var name = dobj.name;
			document.getElementById('mymultiplelabel').innerHTML= "\" "+name+" \" Item contains multiple entry... Please select one";
			$("#stockmultipleitems").html(dobj.data);
			updatePostTableData();
		} 
		});
		displayFade();
		document.getElementById('viewmultipleitems').style.display="block";
		//alert("More than one");
	}
}

function updatePostTableData(){
	var table = document.getElementById("stockmultipleitems");
	var rowCount = table.rows.length;
	//alert("111:"+rowCount);
	for(j=0;j<rowCount;j++)
	{
		var code = table.rows[j].cells[1].innerHTML;
		var ucp =  table.rows[j].cells[2].innerHTML;
		var usp =  table.rows[j].cells[3].innerHTML;
		var quantity =  table.rows[j].cells[4].innerHTML;
		//alert(code+"::"+ucp+"::"+usp);
		var avail_quantity = checkActualAvailQuantity(code,ucp,usp,quantity);
		//alert(j+"::1111:"+avail_quantity);
		table.rows[j].cells[4].innerHTML = ""+avail_quantity;
		//alert(j+"::2222:"+avail_quantity);
	}	
	
	}

function oditekClear()
{
	//alert("oditekClear");
	document.getElementById ('itemsearch').value = "";
	document.getElementById('itemname').value = "";
	document.getElementById('itemusp').value = "";
	document.getElementById('itemnewquantity').value = "";
	document.getElementById('havailquantity').value = "";
	document.getElementById('hitemid').value = "";
	document.getElementById('hitemucp').value = "";
	document.getElementById ('itemsearch').value = "";
	$("#chosen-single-id").html("<span>Select Item Name</span><div><b></b></div>");
	
	selectItemCode();
}

function resetFields()
{
	document.getElementById('itemname').value = "";
	document.getElementById('itemusp').value = "";
	document.getElementById('itemnewquantity').value = "";
	//$("#detailsstockid").html("");
}

function checkActualAvailQuantity(item_code,item_ucp,item_usp,item_avail_quantity){
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	var quantity =0;
	for(i=1;i<rowCount;i++)
	{
		var code =  table.rows[i].cells[1].innerHTML;
		var ucp =  table.rows[i].cells[3].innerHTML;
		var usp =  table.rows[i].cells[4].innerHTML;
		//alert(""+code+"::"+item_code);
		//alert(""+ucp+"::"+item_ucp);
		//alert(""+usp+"::"+item_usp);
		if(code == item_code && ucp == item_ucp && usp == item_usp)
		{
			quantity +=  parseInt(table.rows[i].cells[5].innerHTML,10);
			//alert("QQQQQ:"+quantity);
		}
	}
	var avail_quantity = item_avail_quantity - quantity;
	return avail_quantity;
}

		
function addItemOnClick()
{
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	var count = rowCount ;
	
	var item_name = document.getElementById('itemname').value;
	var item_code = document.getElementById('itemsearch').value;
	var item_ucp = document.getElementById('hitemucp').value;
	var item_usp = document.getElementById('itemusp').value;
	var item_avail_quantity = document.getElementById('havailquantity').value;
	var item_id = document.getElementById('hitemid').value;
	var item_quantity = document.getElementById('itemnewquantity').value;
	var total_sp = parseFloat(item_quantity * item_usp,10).toFixed(2);
	var total_cp = parseFloat(item_quantity * item_ucp,10).toFixed(2);
	
	var new_val = '<tr><td style="padding: 2px 8px;">'+count+'</td><td style="padding: 2px 8px;" >'+item_code+'</td><td style="padding: 2px 8px;" >'+item_name+'</td><td style="padding: 2px 8px;" >'+item_ucp+'</td><td style="padding: 2px 8px;" >'+item_usp+'</td><td style="padding: 2px 8px;" >'+item_quantity+'</td><td style="padding: 2px 8px;" >'+total_sp+'</td><td style="padding: 2px 8px;"><a  href="javascript:void(0)" onClick="return editItem('+count+');" ><input type="button" class="btn btn-xs btn-green" value="Edit"></a></td><td style="padding: 2px 8px;"><a href="javascript:void(0)" onClick="return removeItem('+count+');" ><input type="button" class="btn btn-xs btn-red"  value="Remove"></a></td></tr>';
	
	var tbody = document.getElementById("detailsstockid");
	tbody.innerHTML = tbody.innerHTML+new_val;
	
	var tquantity = parseInt(document.getElementById("tquantity").value,10);
	
	//Total Cost to Company
	var tactualcost = parseFloat(document.getElementById("tactualcostprice").value,10).toFixed(2);
	tactualcost = parseFloat(tactualcost) + parseFloat(total_cp);
	tactualcost = tactualcost.toFixed(2);
	
	//Total Sale to user
	var tcost = parseFloat(document.getElementById("tcost").value,10).toFixed(2);
	tcost = parseFloat(tcost) + parseFloat(total_sp);
	tcost = Math.round(tcost);
	tcost = tcost.toFixed(2);
	
	tquantity+=parseInt(item_quantity,10);
	
	/*var profit = item_usp - item_ucp;
	var tprofit = parseFloat(document.getElementById("tprofit").value,10).toFixed(2);
	var new_profit = parseFloat(profit,10).toFixed(2);
	tprofit = parseFloat(tprofit) + parseFloat(new_profit);
	tprofit = tprofit.toFixed(2);
	*/
	var tprofit = tcost - tactualcost;
	//tprofit = parseFloat(tprofit) + parseFloat(new_profit);
	tprofit = tprofit.toFixed(2);
	
	//alert("tactualcost:"+tactualcost);
	document.getElementById("tactualcostprice").value = ""+tactualcost;
	document.getElementById("tcost").value = ""+tcost;
	document.getElementById("tquantity").value = ""+tquantity;
	document.getElementById("tprofit").value = ""+tprofit;
	
	
	var rowCount = table.rows.length;
	document.getElementById("totalitemid").value = ""+rowCount;
	
	oditekClear();

    return false;
}


function addItem(e){
	
	 if(e.keyCode == 13) {
		return addItemOnClick();
    }

}


function editItem(count){
	//chinu
	displayFade();
	document.getElementById('viewedititem').style.display="block";
	document.getElementById('heditcount').value = count;
	
	var table = document.getElementById("dataTable");
	var code =  table.rows[count].cells[1].innerHTML;
	var name =  table.rows[count].cells[2].innerHTML;
	var ucp =  table.rows[count].cells[3].innerHTML;
	var usp =  table.rows[count].cells[4].innerHTML;
	var quantity =  table.rows[count].cells[5].innerHTML;
	var total_sp =  table.rows[count].cells[6].innerHTML;
	document.getElementById('vieweditLabel').innerHTML= "Edit Quantity of \""+name+"\" ";
	document.getElementById('viewedititemno').value = count;
	document.getElementById('viewedititemcode').value = code;
	document.getElementById('viewedititemname').value = name;
	document.getElementById('viewedititemucp').value = ucp;
	document.getElementById('viewedititemusp').value = usp;
	document.getElementById('viewedititemquantity').value = quantity;
	document.getElementById('viewedittsp').value = total_sp;
	old_quantity = quantity;
	old_price = total_sp;
	old_actual_price = parseFloat(ucp,2) * parseFloat(quantity,0);
	//alert(""+old_actual_price);
	old_profit = usp - ucp;
	var dataString = "act=getquantity&code="+code+"&ucp="+ucp+"&usp="+usp;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{ 
		var dobj=jQuery.parseJSON(html);
		var item_avail_quantity = dobj.value;
		var avail_quantity = checkActualAvailQuantity(code,ucp,usp,item_avail_quantity);
		document.getElementById('availquantity').value = avail_quantity;
		//alert(""+avail_quantity);
	} 
	});
		
	
	
	}
	
	
	function updateItemOnClick(){
	//alert("aaa");
	var quantity = document.getElementById('viewedititemquantity').value;
	var avail_quantity = document.getElementById("availquantity").value;
	var item_name = document.getElementById("viewedititemname").value;
	if(quantity <= 0 || !isNumber(quantity) )
	{
		//alert("Quantity should be numeric value...");
		fade =0;
		showWarningDialog("Quantity should be numeric value...");
		return;
	}
	if(old_quantity != quantity )
	{
		if(quantity > old_quantity)
		{
			var diff = quantity - old_quantity;
			if(avail_quantity < diff)
			{
				var value = parseInt(old_quantity,10) + parseInt(avail_quantity,10);
				fade = 0;
				showWarningDialog("\""+item_name+"\" quantity is not available... <br>Available quantity is "+value+" <BR>Required quantity is "+quantity);
				return;
			}
		}
		//alert("aaa");
		var pop_ucp = document.getElementById('viewedititemucp').value;
		var pop_usp = document.getElementById('viewedititemusp').value;
		
		
		var tquantity = parseInt(document.getElementById("tquantity").value,10);
		var tactualcost = parseFloat(document.getElementById("tactualcostprice").value,10).toFixed(2);
		var tcost = parseFloat(document.getElementById("tcost").value,10).toFixed(2);
		var tprofit = parseFloat(document.getElementById("tprofit").value,10).toFixed(2);
		
		
		//old_actual_price
		tactualcost = parseFloat(tactualcost) - parseFloat(old_actual_price);
		tactualcost = tactualcost.toFixed(2);
		
		
		old_price = parseFloat(old_price,10).toFixed(2);
		tcost = parseFloat(tcost) - parseFloat(old_price);
		tcost = tcost.toFixed(2);
		tquantity -= parseInt(old_quantity,10);
		
		var new_cp = document.getElementById('viewedititemucp').value;
		new_cp = parseFloat(new_cp,2) * parseFloat(quantity,0);
		tactualcost = parseFloat(tactualcost) + parseFloat(new_cp);
		tactualcost = tactualcost.toFixed(2);
		
		var total_sp = document.getElementById('viewedittsp').value;
		var new_cost = parseFloat(total_sp,10).toFixed(2);
		tcost = parseFloat(tcost) + parseFloat(total_sp);
		tcost = Math.round(tcost);
		tcost = tcost.toFixed(2);
	
		tquantity+=parseInt(quantity,10);
		
		tprofit = parseFloat(tprofit) - parseFloat(old_profit*old_quantity);
		tprofit = tprofit.toFixed(2);
		var new_profit = ( pop_usp - pop_ucp ) * quantity;
		//tprofit = parseFloat(tprofit) + parseFloat(new_profit);
		
		tprofit = parseFloat(tcost) - parseFloat(tactualcost);
		tprofit = tprofit.toFixed(2);
	
		//alert(""+total_sp);
		var count = document.getElementById('heditcount').value;
		var table = document.getElementById("dataTable");
		table.rows[count].cells[5].innerHTML = quantity ;
		table.rows[count].cells[6].innerHTML = total_sp ;
		
		document.getElementById("tactualcostprice").value = ""+tactualcost;
		document.getElementById("tcost").value = ""+tcost;
		document.getElementById("tquantity").value = ""+tquantity;
		document.getElementById("tprofit").value = ""+tprofit;
	}
	
	closeEditDialog();
	}
	
function closeEditDialog(){
	removeFade();
	document.getElementById('viewedititem').style.display="none";
	selectItemCode();
	}


function removeItem(count){
	displayFade();
	document.getElementById('viewremoveconfirm').style.display="block";
	document.getElementById('hremovecount').value = count;
	
	
	var table = document.getElementById("dataTable");
	var code =  table.rows[count].cells[1].innerHTML;
	var name =  table.rows[count].cells[2].innerHTML;
	var usp =  table.rows[count].cells[4].innerHTML;
	var quantity =  table.rows[count].cells[5].innerHTML;
	var total_sp =  table.rows[count].cells[6].innerHTML;
	document.getElementById('myremoveLabel').innerHTML= "Are you sure want to remove Item  \""+name+"\" ";
	document.getElementById('removeitemno').value = count;
	document.getElementById('removeitemcode').value = code;
	document.getElementById('removeitemname').value = name;
	document.getElementById('removeitemusp').value = usp;
	document.getElementById('removeitemquantity').value = quantity;
	document.getElementById('removetsp').value = total_sp;
	
	
	}
	

function removeItemFromList(){
	
		var count =  document.getElementById("hremovecount").value;
		
		var table = document.getElementById("dataTable");
		var name =  table.rows[count].cells[2].innerHTML;
		var rowCount = table.rows.length;
		var total = rowCount - 1 ;
		
		var ucp =  table.rows[count].cells[3].innerHTML;
		var usp =  table.rows[count].cells[4].innerHTML;
		var quantity =  table.rows[count].cells[5].innerHTML;
		var total_sp =  table.rows[count].cells[6].innerHTML;
		
		var unit_profit = usp - ucp;
		
		var tcost = parseFloat(document.getElementById("tcost").value,10).toFixed(2);
		var tactualcost = parseFloat(document.getElementById("tactualcostprice").value,10).toFixed(2);
		var tquantity = parseInt(document.getElementById("tquantity").value,10);
		var tprofit = parseInt(document.getElementById("tprofit").value,10);
		
		var old_tcp = parseFloat(ucp,2) *  parseFloat(quantity); 
		tactualcost = parseFloat(tactualcost) - parseFloat(old_tcp);
		tactualcost = tactualcost.toFixed(2);
		
		
		tcost = parseFloat(tcost) -  parseFloat(total_sp,10);
		tcost = Math.round(tcost);
		tcost = tcost.toFixed(2);
		tquantity -=parseInt(quantity,10);
		
		var profit = parseFloat(tcost) -  parseFloat(tactualcost);
		profit = profit.toFixed(2);
		
		document.getElementById("tactualcostprice").value = ""+tactualcost;
		document.getElementById("tcost").value = ""+tcost;
		document.getElementById("tquantity").value = ""+tquantity;
		document.getElementById("tprofit").value = ""+profit;
		
		table.deleteRow(count);
		
		var new_rowCount = table.rows.length;
		
		for(i=count;i<new_rowCount;i++)
		{
			table.rows[i].cells[0].innerHTML = i;
			table.rows[i].cells[7].innerHTML = '<a  href="javascript:void(0)" onClick="return editItem('+i+');" ><input type="button" class="btn btn-xs btn-green"  value="Edit"></a>';
			table.rows[i].cells[8].innerHTML = '<a href="javascript:void(0)" onClick="return removeItem('+i+');" ><input type="button" class="btn btn-xs btn-red"  value="Remove"></a>';
		}
		
		closeRemoveDialog();
	}

	
function closeRemoveDialog(){
	removeFade();
	document.getElementById('viewremoveconfirm').style.display="none";
	selectItemCode();
	}
	
function recalculate(){
	var quantity = document.getElementById('viewedititemquantity').value;
	var usp = document.getElementById('viewedititemusp').value;
	var total_sp = parseFloat(quantity * usp,10).toFixed(2);
	document.getElementById('viewedittsp').value = total_sp;
	}	


	
	
function updateItem(e){
	 if(e.keyCode == 13) {
		return updateItemOnClick();
    }

}	





	
	
function closeDialog(){
	removeFade();
	document.getElementById('viewmultipleitems').style.display="none";
	var text_input = document.getElementById ('itemsearch');
	text_input.value="";
  	text_input.focus ();
  	text_input.select ();
		
	}

function setSelectedItem(name,item_code,item_id,row_no){
	
		var table = document.getElementById("stockmultipleitems");
		var quantity =  table.rows[row_no -1 ].cells[4].innerHTML;
		//alert("P:"+quantity);
		if(quantity > 0)
		{
			removeFade();
			var dataString = "act=addtoBillSelectedItemno&itemid="+item_id+"&itemno="+item_code;
			$.ajax({ 
			type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				
				var item_name = dobj.name;
				var item_code = dobj.code;
				var item_id = dobj.id;
				var item_ucp = dobj.ucp;
				var item_usp = dobj.usp;
				var item_avail_quantity = dobj.quantity;
				
				document.getElementById('itemname').value = item_name;
				document.getElementById('itemusp').value = item_usp;
				document.getElementById('hitemucp').value = item_ucp;
				document.getElementById('havailquantity').value = item_avail_quantity;
				document.getElementById('hitemid').value = item_id;
				document.getElementById('itemnewquantity').value = 1;
				
				document.getElementById('viewmultipleitems').style.display="none";
				
				addItemOnClick();
				
				//var text_input = document.getElementById ('itemnewquantity');
				//text_input.focus ();
				//text_input.select ();
			
				
			} 
			});
		}
		else
		{
			fade = 0;
			showWarningDialog("Available stock of \""+name+"\" is 0");
		}
        return false;
	
	}



	
function checkBillValidation(){
	var name  = document.getElementById('cname').value;
	var addr  = document.getElementById('caddress').value;
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	
	if(rowCount == 1 )
	{
		showWarningDialog("Add Item...");
		return false;
	}
	
	if(name.length == 0)
	{
		showWarningDialog("Enter Customer Name...");
		return false;
	}
	if(addr.length == 0)
	{
		showWarningDialog("Enter Customer Address...");
		return false;
	}
	
	displayFade();
	document.getElementById('viewbillconfirm').style.display = "block";
	document.getElementById('billtcost').value = document.getElementById('tcost').value;
	document.getElementById('billtitems').value = document.getElementById('tquantity').value;
	document.getElementById('billcname').value = name;
	document.getElementById('billcaddr').value = addr;
	
	//generateBill();
	
	return true;
}	


function closeBillDialog(){
	removeFade();
	document.getElementById('viewbillconfirm').style.display="none";
	var text_input = document.getElementById ('itemsearch');
	text_input.value="";
  	text_input.focus ();
  	text_input.select ();
	}
	
function generateBill(){
	closeBillDialog();
	showLoading();
	addHiddenFields();
	var surl="billgenerate.php";
	var sdata=$( "#billdata" ).serialize();
	$.post( surl, sdata, function(data){
		var dobj=jQuery.parseJSON(data);
		//alert("aaa");
		printBill(dobj.data);
		closeLoading();
		showSucessDialog(dobj);
		//reloadPage();
		
	});
	
}


function addHiddenFields(){
	var tbody = document.getElementById("hiddenfieldid");
	var table = document.getElementById("dataTable");
	var rowCount = table.rows.length;
	
	//var tcost = document.getElementById("tcost").value;
	//var tprofit = document.getElementById("tprofit").value;
	//var tcost_price = parseFloat(tcost) -  parseFloat(tprofit);
	//var tcost_price = document.getElementById("tactualcostprice").value;
	//tcost_price = tcost_price.toFixed(2);
	//document.getElementById("tcostprice").value = tcost_price ;
	
	for(i=1; i< rowCount;i++)
	{
		var item_code =  table.rows[i].cells[1].innerHTML;
		var item_name =  table.rows[i].cells[2].innerHTML;
		var item_ucp =  table.rows[i].cells[3].innerHTML;
		var item_usp =  table.rows[i].cells[4].innerHTML;
		var item_qty =  table.rows[i].cells[5].innerHTML;
		var item_tsp =  table.rows[i].cells[6].innerHTML;
		//alert("Code:"+item_code+"::TSP:"+item_tsp);
		new_val = '<input type="hidden" id="itemcode'+i+'"  name="itemcode'+i+'" value="'+item_code+'">';
		new_val += '<input type="hidden" id="itemname'+i+'"  name="itemname'+i+'" value="'+item_name+'">';
		new_val += '<input type="hidden" id="itemucp'+i+'"  name="itemucp'+i+'" value="'+item_ucp+'">';
		new_val += '<input type="hidden" id="itemusp'+i+'"  name="itemusp'+i+'" value="'+item_usp+'">';
		new_val += '<input type="hidden" id="itemqty'+i+'"  name="itemqty'+i+'" value="'+item_qty+'">';
		new_val += '<input type="hidden" id="itemtsp'+i+'"  name="itemtsp'+i+'" value="'+item_tsp+'">';
		tbody.innerHTML = tbody.innerHTML+new_val;
	}
	
	
	}
	
function printBill(printContent){
	//var printContent = document.getElementById('printsection').innerHTML;
	//alert(""+printContent);
	
	var display_setting="toolbar=no,menubar=no,"; 
	display_setting+="scrollbars=no,width=600, height=600, left=5, top=10"; 
	var printpage=window.open("","",display_setting); 
	printpage.document.open(); 
	printpage.document.write('<html><head><title>Print Page</title></head>'); 
	printpage.document.write('<body onLoad="self.print()" align="center" style="width: 3in; margin:0; padding:0;">'+ printContent +'</body></html>'); 
	printpage.document.close(); 
	printpage.focus(); 
	
	}
	
	function checkAndcloseDialog(){
			var id = document.getElementById('opendialog').value;
			if(id.length > 0)
			{
				if(id == "viewdialog")
					closeWarningDialog();
				return false;	
			}
			else
				return true;
		}
		
	function showWarningDialog(str_value){
		displayFade();
		document.getElementById('opendialog').value = "viewdialog";
		document.getElementById('viewdialog').style.display="block";
		document.getElementById('warningbodyid').innerHTML = str_value;
		var text_input = document.getElementById ('itemsearch');
  		text_input.focus ();
  		text_input.select ();
		}
		
	function closeWarningDialog(){
		if(fade == 1)
		{
			removeFade();
			document.getElementById ('itemsearch').value = "";
			$("#chosen-single-id").html("<span>Select Item Name</span><div><b></b></div>");
		}
		fade = 1;	
		document.getElementById('opendialog').value = "";
		document.getElementById('viewdialog').style.display="none";
		document.getElementById('warningbodyid').innerHTML = "";
		selectItemCode();
		}
		
	function isNumber(n) {
  	return !isNaN(parseFloat(n)) && isFinite(n);
	}	
	


	function reloadPage(){
		location.reload();
			
		}
		
		
	function showSucessDialog(dobj){
		displayFade();
		document.getElementById('viewsuccessdialog').style.display="block";
		var billno = dobj.billno;
		document.getElementById('successbodyid').innerHTML = "Bill NO : "+billno;
		}	
		
	function closeSucessDialog(){
		removeFade();
		document.getElementById('viewsuccessdialog').style.display="none";
		document.getElementById('successbodyid').innerHTML = "";
		reloadPage();
		}	