

function generateBill(){
	showLoading();
	addHiddenFields();
	var surl="billprint.php";
	var sdata=$( "#billdata" ).serialize();
	$.post( surl, sdata, function(data){
		var dobj=jQuery.parseJSON(data);
		printBill(dobj.data);
		closeLoading();
		showSucessDialog(dobj);
		//reloadPage();
		
	});
	
}


function addHiddenFields(){
	var tbody = document.getElementById("hiddenfieldid");
	var table = document.getElementById("dataTable123");
	var rowCount = table.rows.length;
	document.getElementById("totalitemid").value = ""+rowCount;
	/*
	var tsale = document.getElementById("tsale").value;
	var tcost = document.getElementById("tcost").value;
	var tprofit = parseFloat(tsale) -  parseFloat(tcost);
	tprofit = tprofit.toFixed(2);
	document.getElementById("tprofit").value = tprofit ;
	*/
	for(i=1; i< rowCount;i++)
	{
		
		var item_code =  table.rows[i].cells[1].innerHTML;
		var item_name =  table.rows[i].cells[2].innerHTML;
		var item_ucp =  table.rows[i].cells[3].innerHTML;
		var item_usp =  table.rows[i].cells[4].innerHTML;
		var item_qty =  table.rows[i].cells[5].innerHTML;
		var item_tsp =  table.rows[i].cells[6].innerHTML;
		//alert("Code:"+item_code+"::TSP:"+item_tsp);
		new_val = '<input type="hidden" id="itemcode'+i+'"  name="itemcode'+i+'" value="'+item_code+'">';
		new_val += '<input type="hidden" id="itemname'+i+'"  name="itemname'+i+'" value="'+item_name+'">';
		new_val += '<input type="hidden" id="itemucp'+i+'"  name="itemucp'+i+'" value="'+item_ucp+'">';
		new_val += '<input type="hidden" id="itemusp'+i+'"  name="itemusp'+i+'" value="'+item_usp+'">';
		new_val += '<input type="hidden" id="itemqty'+i+'"  name="itemqty'+i+'" value="'+item_qty+'">';
		new_val += '<input type="hidden" id="itemtsp'+i+'"  name="itemtsp'+i+'" value="'+item_tsp+'">';
		tbody.innerHTML = tbody.innerHTML+new_val;
	}
	
	
	}
	
function printBill(printContent){
	//var printContent = document.getElementById('printsection').innerHTML;
	
	var display_setting="toolbar=no,menubar=no,"; 
	display_setting+="scrollbars=no,width=600, height=1200, left=5, top=10"; 
	var printpage=window.open("","",display_setting); 
	printpage.document.open(); 
	printpage.document.write('<html><head><title>Print Page</title></head>'); 
	printpage.document.write('<body onLoad="self.print()" align="center" style="width: 3in; margin:0; padding:0;">'+ printContent +'</body></html>'); 
	printpage.document.close(); 
	printpage.focus(); 
	
	}
	
	
		
	
		
	function isNumber(n) {
  	return !isNaN(parseFloat(n)) && isFinite(n);
	}	
	
	
	function showLoading(){
		document.getElementById('loading_data').style.display="block";
		}
		
	function closeLoading(){
		
		document.getElementById('loading_data').style.display="none";
		}


	
		
		
	function showSucessDialog(dobj){
		displayFade();
		document.getElementById('viewsuccessdialog').style.display="block";
		var billno = dobj.billno;
		document.getElementById('successbodyid').innerHTML = "Bill NO : "+billno;
		}	
		
	function closeSucessDialog(){
		removeFade();
		document.getElementById('viewsuccessdialog').style.display="none";
		document.getElementById('successbodyid').innerHTML = "";
		//reloadPage();
		}	
		
	function displayFade(){
	document.getElementById('modal-backdrop-id').style.display = "block";
	}

function removeFade(){
	document.getElementById('modal-backdrop-id').style.display = "none";
	}
	
	function reloadPage(){
		location.reload();
			
		}
		

function searchBillOnClick(){
	
	var billno = document.getElementById("billno").value;
	if(billno.length == 0)
	{
		alert("Enter Bill No...");
		return false; 
	}
	
	var dataString = "act=getbilldetail&billno="+billno;
	$.ajax({ 
	type: "POST",url: "ajaxadmin.php" ,data: dataString,cache: false,
	success: function(html)
	{ 
		var dobj=jQuery.parseJSON(html)
		if(dobj.result == 0 )
		{
			var error = dobj.error;
			if(error.length > 0)
				alert(""+error);
		}
		else
		{
			updateField(dobj);
		}
	} 
	});
	
}		
		
function updateField(obj){
	
	document.getElementById("generatedon").innerHTML = "[Generated On : "+obj.date+"]" ;
	document.getElementById("tquantity").value = obj.tqty;
	document.getElementById("tsale").value = obj.tsale;
	document.getElementById("tcost").value = obj.tcost;
	document.getElementById("cname").value = obj.cname;
	document.getElementById("caddress").value = obj.caddr;
	document.getElementById("detailsstockid").innerHTML = obj.data;
	}	