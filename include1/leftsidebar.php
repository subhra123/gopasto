<?php
//include_once '../dbcon/DBConnection.php';

$pname=trim(basename($_SERVER['REQUEST_URI']));
$path_parts = pathinfo($_SERVER['REQUEST_URI']);
$pname= $path_parts['filename'];
$login_id = $_SESSION["admin_loginid"];
$admin_restrictions=0;
	//$dbobj = new DBConnection();
	//$dbobj->connect();
	
if($login_id != "admin")
{
	$admin_restrictions = 1;
	$adminobj = $dbobj->getAdminLogin($login_id);
}
//echo ":::".$admin->lr1;
//die();

function isConfigFile($page)
{
	$ret=0;
	if( $page == "configda" )
		$ret=1;
	else if( $page == "configuration" )
		$ret=1;
	else if( $page == "configration" )
		$ret=1;
	else if( $page == "configcity" )
		$ret=1;	
	else if( $page == "configcitycategory" )
		$ret=1;
	else if( $page == "rank" )
		$ret=1;
	else if( $page == "basicincrease" || $page == "viewbasicincease"  )
		$ret=1;		
		
		
	return $ret;
}

function isStockFile($page)
{
	$ret=0;
	if( $page == "addstock" )
		$ret=1;
	else if( $page == "viewstock" )
		$ret=1;
	else if( $page == "viewemployee" )
		$ret=1;
	else if( $page == "stocktransaction" )
		$ret=1;
			
	return $ret;
}


function isBillFile($page)
{
	$ret=0;
	if( $page == "newbill" )
		$ret=1;
	else if( $page == "viewbill" )
		$ret=1;
	else if( $page == "viewdetailbill" )
		$ret=1;	
			
	return $ret;
}



function isGenerateSalary($page)
{
	$ret=0;
	if(  $page == "forceupdate" || $page == "editmanual" || $page == "manualcalculation" || $page == "manualupdate" || $page == "forceorder" || $page == "editloan" || $page == "loancalculation" || $page == "loan" || $page == "generatesalary" || $page == "viewaroll" || $page == "viewemployeearoll")
		$ret=1;
		
		
	return $ret;
}

function isReportSalary($page)
{
	$ret=0;
	if(  $page == "bankercopy" || $page == "schedule" || $page == "savings" || $page == "masterroll" ||  $page == "paybill" ||  $page == "aroll" )
		$ret=1;
		
		
	return $ret;
}

?>
<nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
                        <div class="profileimage">
                        <?php
						$admin = $_SESSION["admin_loginid"];
						if($admin ==  "admin")
						{
							echo '<img class="img-circle" src="img/profile-pic-blank.png" alt="">';
						}
						else
						{	$image = $dbobj->getImage($admin);
							if(strlen($image)  > 0)
								echo '<img class="img-circle" src="../'.$image.'" alt="">';
							else
								echo '<img class="img-circle" src="img/profile-pic-blank.png" alt="">';
						}
						
						?>
                        
                        
                        </div>
                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                        <p class="name tooltip-sidebar-logout">
                           
                            <span class="last-name"><?php echo $_SESSION["admin_name"];?></span> <a style="color: inherit; outline:none;" class="logout_open outlinedivnone" href="logout.php" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->
                    <!-- begin SIDE NAV SEARCH -->
                    <li class="nav-search">
                        <form role="form">
                            <input type="search" class="form-control" placeholder="Search...">
                            <button class="btn">
                                <i class="fa fa-search"></i>                            </button>
                        </form>
                    </li>
                    <!-- end SIDE NAV SEARCH -->
                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a <?php if($pname=='dashboard') echo 'class="active"';?> href="dashboard.php"><i class="fa fa-hand-o-right"></i> Dashboard</a>
                    </li>
<?php	if(!$admin_restrictions)
		{
		if($_SESSION["admin_rank"] == ''){	?>
                    <li>
                        <a <?php if($pname=='manageadmin')echo 'class="active"';?> href="manageadmin.php"><i class="fa fa-hand-o-right"></i> Manage Admin</a>
                    </li>
<?php		} }		?>
                    <!-- end DASHBOARD LINK -->
                    <!--add_employee_div-->
                    
     <?php  if($admin_restrictions == 0 || $adminobj->lr1 == 1 || $adminobj->lr2 == 1  ){  ?>               
                    
                     <li class="panel">
                    <a href="javascript:;" <?php if(isStockFile($pname) > 0)echo 'class="accordion-toggle active"'; else echo 'class="accordion-toggle"';?> data-parent="#side" data-toggle="collapse"  data-target="#employeeupdate">
                        <i class="fa fa-hand-o-right"></i> Manage Stocks <i class="fa fa-caret-down"></i>
                        </a>
                        <ul <?php if(isStockFile($pname))echo 'class="in nav"'; else echo 'class="collapse nav"';?> id="employeeupdate">
                           <?php if($admin_restrictions == 0 || $adminobj->lr1 == 1 ) { ?> 
							<li>
							<a <?php if($pname=='addstock' ) { echo 'class="active"'; }?> href="addstock.php"><i class="fa fa-angle-double-right"></i> Add Stock</a>
							</li>
                            <?php } ?>
                            <?php if($admin_restrictions == 0 || $adminobj->lr1 == 1 ) { ?> 
							<li>
							<a <?php if($pname=='viewstock' ) { echo 'class="active"'; }?> href="viewstock.php"><i class="fa fa-angle-double-right"></i> View Stock</a>
							</li>
                            <?php } ?>
                            
                            <?php if($admin_restrictions == 0 || $adminobj->lr1 == 1 ) { ?> 
							<li>
							<a <?php if($pname=='stocktransaction' ) { echo 'class="active"'; }?> href="stocktransaction.php"><i class="fa fa-angle-double-right"></i> Stock Transaction</a>
							</li>
                            <?php } ?>
                            
                        </ul>
                    </li>
     <?php  }  ?>  
     
     
     
     <?php  if($admin_restrictions == 0 || $adminobj->lr1 == 1 || $adminobj->lr2 == 1  ){  ?>               
                    
                     <li class="panel">
                    <a href="javascript:;" <?php if(isBillFile($pname) > 0)echo 'class="accordion-toggle active"'; else echo 'class="accordion-toggle"';?> data-parent="#side" data-toggle="collapse"  data-target="#billmanage">
                        <i class="fa fa-hand-o-right"></i> Manage Bills <i class="fa fa-caret-down"></i>
                        </a>
                        <ul <?php if(isBillFile($pname))echo 'class="in nav"'; else echo 'class="collapse nav"';?> id="billmanage">
                           <?php if($admin_restrictions == 0 || $adminobj->lr1 == 1 ) { ?> 
							<li>
							<a <?php if($pname=='newbill' ) { echo 'class="active"'; }?> href="newbill.php"><i class="fa fa-angle-double-right"></i> New Bill</a>
							</li>
                            <?php } ?>
                            <li>
							<a <?php if($pname=='viewbill' || $pname=='viewdetailbill' ) { echo 'class="active"'; }?> href="viewbill.php"><i class="fa fa-angle-double-right"></i> View Bill</a>
							</li>
                           
                            
                        </ul>
                    </li>
     <?php  }  ?>               
                    
                  
                    
                    
                   
                    
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>