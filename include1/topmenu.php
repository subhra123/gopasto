<?php
//include_once '../dbcon/DBConnection.php';

$pname=trim(basename($_SERVER['REQUEST_URI']));
$path_parts = pathinfo($_SERVER['REQUEST_URI']);
$pname= $path_parts['filename'];
$login_id = $_SESSION["admin_loginid"];
$admin_restrictions=0;
	//$dbobj = new DBConnection();
	//$dbobj->connect();
	
if($login_id != "admin")
{
	$admin_restrictions = 1;
	$adminobj = $dbobj->getAdminFieldRestriction($login_id);
}
//echo ":::".$admin->lr1;
//die();
?>
		<div class="row" style="margin-bottom:10px">
		<div class="tpaddingdiv2">
    	
		<?php if(!$admin_restrictions || $adminobj->r1) { ?>
        <a href="newbill.php" class="tile tile-themed  <?php if($pname=='newbill' ) { echo 'tile-active'; }?> ">
		<i class="fa fa-inr"></i>
		<div class="tile-info">
		<div class="center">New Bill</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r2) { ?>
    	<a href="viewbill.php" class="tile tile-themed <?php if($pname=='viewbill' ) { echo 'tile-active'; }?>">
		<i class="fa fa-file-o"></i>
		<div class="tile-info">
		<div class="center">View Past Bill</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r3) { ?>
        <a href="viewdetailbill.php" class="tile tile-themed <?php if($pname=='viewdetailbill' ) { echo 'tile-active'; }?>">
		<i class="fa fa-file-o"></i>
		<div class="tile-info">
		<div class="center">Detail Bill</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r4) { ?>
        <a href="addstock.php" class="tile tile-themed <?php if($pname=='addstock' ) { echo 'tile-active'; }?>">
		<i class="fa fa-stack-overflow"></i>
		<div class="tile-info">
		<div class="center">Add Stock</div>
		</div>
      	</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r5) { ?>
        <a href="viewstock.php" class="tile tile-themed <?php if($pname=='viewstock' ) { echo 'tile-active'; }?>">
		<i class="fa fa-list-ul"></i>
		<div class="tile-info">
		<div class="center">View Stock</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r6) { ?>
        <a href="stocktransaction.php" class="tile tile-themed <?php if($pname=='stocktransaction' ) { echo 'tile-active'; }?>">
		<i class="fa fa-list-alt"></i>
		<div class="tile-info">
		<div class="center"> Stock Transaction</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r7) { ?>
        <a href="salereport.php" class="tile tile-themed <?php if($pname=='salereport' ) { echo 'tile-active'; }?>">
		<i class="fa fa-file-text"></i>
		<div class="tile-info">
		<div class="center"> Bill Sale Report</div>
		</div>
		</a>
        <?php } ?>
        
         <?php if(!$admin_restrictions || $adminobj->r7) { ?>
        <a href="itemsalereport.php" class="tile tile-themed <?php if($pname=='itemsalereport' ) { echo 'tile-active'; }?>">
		<i class="fa fa-file-text"></i>
		<div class="tile-info">
		<div class="center"> Item Sale Report</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r8) { ?>
        <a href="stockreport.php" class="tile tile-themed <?php if($pname=='stockreport' || $pname=='stockreportdetail') { echo 'tile-active'; }?>">
		<i class="fa fa-file-text"></i>
		<div class="tile-info">
		<div class="center"> Entry Stock Report</div>
		</div>
		</a>
        <?php } ?>
        
        <?php if(!$admin_restrictions || $adminobj->r8) { ?>
        <a href="balancestockreport.php" class="tile tile-themed <?php if($pname=='balancestockreport' ) { echo 'tile-active'; }?>">
		<i class="fa fa-file-text"></i>
		<div class="tile-info">
		<div class="center"> Balance Stock Report</div>
		</div>
		</a>
        <?php } ?>
		
        </div>
       
       
       
       <?php if($admin_restrictions == 0){ ?> 
        <div class="tpaddingdiv2" style="float:right">
        <a href="adminuser.php" class="tile tile-themed  <?php if($pname=='adminuser' ) { echo 'tile-active'; }?> ">
		<i class="fa fa-users"></i>
		<div class="tile-info">
		<div class="center">Manage User</div>
		</div>
		</a>
        <!--
        <a href="settings.php" class="tile tile-themed  <?php if($pname=='settings' ) { echo 'tile-active'; }?> ">
		<i class="fa fa-cog"></i>
		<div class="tile-info">
		<div class="center">Settings</div>
		</div>
		</a>
        -->
        </div>
		<?php } ?>
        
        
        
		<div class="clearfix"></div>
		</div>
		