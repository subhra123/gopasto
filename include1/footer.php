
<div class="modal-backdrop fade in" id="modal-backdrop-id" style="display:none"></div>
<input type="hidden" name="opendialog" id="opendialog"  value="" onKeyPress="checkAndcloseDialog();">


 <div class="modal" id="confirmdialog" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:30%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
              
          <h3 class="modal-title" id="confirmdialogLabel" style="font-size:18px">Confirm?</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;" id="confirmdialogbody" >
	      </div>
          <div class="modal-footer">
           <input type="hidden" id="deleteid" name="deleteid"  >
           <button type="button" class="btn btn-default btn-red" onClick="actionConfirmDialog()" >OK</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeConfirmDialog()" ><i class="fa fa-times"></i>Cancel</button>
          </div>
        </div>
      </div>
    </div>



	<div class="modal" id="viewdialog" tabindex="-1" style="display:none; z-index:1111;" >
	<div class="modal-dialog" style="width:30%; margin-top:150px;">
	<div class="modal-content">
    <div class="modal-header" style="padding:5px; padding-left:20px;">
              
    <h3 class="modal-title" id="mywarningLabel" style="font-size:18px">Warning</h3>
    </div>
    <div class="modal-body" style="padding-bottom:0px;" id="warningbodyid" >
	</div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default btn-green" onClick="closeWarningDialog()" ><i class="fa fa-times"></i>CLOSE</button>
    </div>
    </div>
    </div>
    </div>





<div class="modal" id="viewmultipleitems" tabindex="-1" role="dialog" style="display:none" >
      <div class="modal-dialog" style="width:60%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeDialog();">&times;</button>
              
          <h3 class="modal-title" id="mymultiplelabel" style="font-size:18px"></h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      
          <table class="table table-bordered table-striped table-hover">
		   <colgroup>
           <col class="col-md-1 col-sm-1">
           <col class="col-md-4 col-sm-4">
           <col class="col-md-2 col-sm-2">
           <col class="col-md-2 col-sm-2">
           <col class="col-md-1 col-sm-1">
           <col class="col-md-1 col-sm-1">
           </colgroup>
           <thead>
		   <tr>
		   <th>Sl. No</th>
           <th>Item Code</th>
		   <th>Unit Cost Price</th>
           <th>Unit Sale Price</th>
		   <th>Quantity</th>
           <th></th>
		   </tr>
		   </thead>
		   <tbody id="stockmultipleitems" >
          
			</tbody>
			</table>
         
         
          </div>
          
          
          <div class="modal-footer">
           <input type="hidden" name="heditcount" id="heditcount"  value="">
           <button type="button" class="btn btn-default btn-green" onClick="closeDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>    


<div class="modal" id="viewedititem" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:40%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeEditDialog();">&times;</button>
              
          <h3 class="modal-title" id="vieweditLabel" style="font-size:18px"></h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Sl No :</span>
		  <input type="text" name="viewedititemno" id="viewedititemno" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Code :</span>
		  <input type="text" name="viewedititemcode" id="viewedititemcode" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Name :</span>
		  <input type="text" name="viewedititemname" id="viewedititemname" readonly class="form-control" placeholder="" value="" >	
          </div>
         
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Unit CP :</span>
		  <input type="text" name="viewedititemucp" id="viewedititemucp" readonly class="form-control" placeholder="" value="" >	
          </div>
           
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Unit SP :</span>
		  <input type="text" name="viewedititemusp" id="viewedititemusp" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Quantity :</span>
		  <input type="text" name="viewedititemquantity" id="viewedititemquantity"  class="form-control" placeholder="" value="" onKeyUp="recalculate();"  onKeyPress="return updateItem(event)">	
          </div>
          
           <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total SP :</span>
		  <input type="text" name="viewedittsp" readonly id="viewedittsp"  class="form-control" placeholder="" value="" >	
          </div>
         
         
          </div>
          
          
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="updateItemOnClick();">Update</button>
           <input type="hidden" name="heditcount" id="heditcount"  value="">
           <button type="button" class="btn btn-default btn-green" onClick="closeEditDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>





<div class="modal" id="viewremoveconfirm" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:40%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeRemoveDialog();">&times;</button>
              
          <h3 class="modal-title" id="myremoveLabel" style="font-size:18px"></h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Sl No :</span>
		  <input type="text" name="removeitemno" id="removeitemno" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Code :</span>
		  <input type="text" name="removeitemcode" id="removeitemcode" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Name :</span>
		  <input type="text" name="removeitemname" id="removeitemname" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Unit SP :</span>
		  <input type="text" name="removeitemusp" id="removeitemusp" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Quantity :</span>
		  <input type="text" name="removeitemquantity" id="removeitemquantity"  readonly class="form-control" placeholder="" value="" >	
          </div>
          
           <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total SP :</span>
		  <input type="text" name="removetsp" readonly id="removetsp"  class="form-control" placeholder="" value="" >	
          </div>
         
         
          </div>
          
          
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="removeItemFromList();">Remove</button>
           <input type="hidden" name="hremovecount" id="hremovecount"  value="">
           <button type="button" class="btn btn-default btn-green" onClick="closeRemoveDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    


<div class="modal" id="viewsuccessdialog" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:30%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
              
          <h3 class="modal-title" style="font-size:18px">Bill Generate Success</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;" id="successbodyid" >
	      </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-default btn-green" onClick="closeSucessDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    


<div class="modal" id="viewbillconfirm" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:30%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeBillDialog();">&times;</button>
              
          <h3 class="modal-title" style="font-size:18px">Bill Generation Confirm?</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      
         
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Customer Name :</span>
		  <input type="text" name="billcname" id="billcname" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Customer Address :</span>
		  <input type="text" name="billcaddr" id="billcaddr" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total Cost :</span>
		  <input type="text" name="billtcost" id="billtcost" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total Items :</span>
		  <input type="text" name="billtitems" id="billtitems" readonly class="form-control" placeholder="" value="" >	
          </div>
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="generateBill();">Generate</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeBillDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    <div class="modal" id="addnewstock" tabindex="-1"  style="display:none" >
      <div class="modal-dialog" style="width:40%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeNewItemDialog();">&times;</button>
              
          <h3 class="modal-title" id="myModalLabel" style="font-size:18px">Add New Item</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      <div class="input-group bmargindiv1 col-md-12">
	      <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Code (Scan Here):</span>
		  <input type="text" class="form-control" name="popnewitemadd" id="popnewitemadd" placeholder="" value="" onkeypress="return checkItem(event)" onFocus="this.select()"  >
          </div>
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Name :</span>
		  <input type="text" name="popnewitemname" id="popnewitemname" readonly class="form-control" placeholder="" value="" onkeypress="return moveToNext(event,'popnewitemucp')" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item UCP :</span>
		  <input type="text" name="popnewitemucp" id="popnewitemucp" readonly class="form-control" placeholder="" value="0.0000" onKeyUp="calculateUSP();" onkeypress="return moveToNext(event,'popnewitemquantity')" >	
          </div>
          
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item USP :</span>
		  <input type="text" name="popnewitemusp" readonly id="popnewitemusp" readonly class="form-control" placeholder="" value="0.0000" >	
          </div>
          
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Quantity :</span>
		  <input type="text" name="popnewitemquantity" id="popnewitemquantity" readonly class="form-control" placeholder="" value="1" onkeypress="return saveNewItem(event)">	
          </div>
         
         
          </div>
          
          
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-red outlinedivnone"  onClick="clearNewItemOnClick();">Clear</button>
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="saveNewItemOnClick();">ADD</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeNewItemDialog();"><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    
    <div class="modal" id="editname" tabindex="-1"  style="display:none" >
      <div class="modal-dialog" style="width:40%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeEditNameDialog();">&times;</button>
              
          <h3 class="modal-title" id="myModalLabel" style="font-size:18px">Edit Item Name</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      <div class="input-group bmargindiv1 col-md-12">
	      <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Code (Scan Here):</span>
		  <input type="text" class="form-control" name="popedititemcode" id="popedititemcode" placeholder="" value="" onkeypress="return checkItemToEdit(event)" onFocus="this.select()"  >
          </div>
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Name :</span>
		  <input type="text" name="popedititemname" id="popedititemname" readonly class="form-control" placeholder="" value="" onkeypress="return saveEditItem(event)" >	
          </div>
          
          </div>
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="saveEditItemOnClick();">Update</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeEditNameDialog();"><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    
    
    
    <div class="modal" id="addstockconfirm" tabindex="-1" style="display:none" >
      <div class="modal-dialog" style="width:30%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeAddStockDialog();">&times;</button>
              
          <h3 class="modal-title" style="font-size:18px">Add To Stock Confirm?</h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total Cost :</span>
		  <input type="text" name="newstocktcost" id="newstocktcost" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total Sale :</span>
		  <input type="text" name="newstocktsale" id="newstocktsale" readonly class="form-control" placeholder="" value="" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Total Items :</span>
		  <input type="text" name="newstocktqty" id="newstocktqty" readonly class="form-control" placeholder="" value="" >	
          </div>
          </div>
          
          
          <div class="modal-footer">
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="addToStock();">Add To Stock</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeAddStockDialog()" ><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    
    
    
    
     <div class="modal" id="editstock" tabindex="-1"  style="display:none" >
      <div class="modal-dialog" style="width:40%; margin-top:150px;">
        <div class="modal-content">
          <div class="modal-header" style="padding:5px; padding-left:20px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeEditStockDialog();">&times;</button>
              
          <h3 class="modal-title" id="editstockLabel" style="font-size:18px">Edit Stock </h3>
          </div>
          <div class="modal-body" style="padding-bottom:0px;">
	      <div class="input-group bmargindiv1 col-md-12">
	      <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Code :</span>
		  <input type="text" class="form-control" name="editstockitemcode"  readonly id="editstockitemcode" placeholder="" value=""  >
          </div>
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Name :</span>
		  <input type="text" name="editstockitemname" id="editstockitemname" readonly class="form-control" placeholder="" value=""  >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item UCP :</span>
		  <input type="text" name="editstockitemucp" id="editstockitemucp" class="form-control" placeholder="" value=""  onKeyUp="calculateEditStockUSP();" onkeypress="return moveToNext(event,'editstockitemqty')" >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item USP :</span>
		  <input type="text" name="editstockitemusp" id="editstockitemusp" readonly class="form-control" placeholder="" value=""  >	
          </div>
          
          <div class="input-group bmargindiv1 col-md-12">
          <span class="input-group-addon ndrftextwidth text-right" style="width:180px">Item Quantity :</span>
		  <input type="text" name="editstockitemqty" id="editstockitemqty" class="form-control" placeholder="" value=""  onkeypress="return saveEditStock(event)" >	
          </div>
          
          
          </div>
          <div class="modal-footer">
           <input type="hidden" id="hiddbid" name="hiddbid" value="" />
           <input type="hidden" id="hideditstocktype" name="hideditstocktype" value="" />
           <input type="hidden" id="hideditstockitemucp" name="hideditstockitemucp" value="" />
           <input type="hidden" id="hideditstockitemusp" name="hideditstockitemusp" value="" />
           <input type="hidden" id="hideditstockitemqty" name="hideditstockitemqty" value="" />
           
           <button type="button" class="btn btn-bg btn-green outlinedivnone"  onClick="saveEditStockOnClick();">Update</button>
           <button type="button" class="btn btn-default btn-green" onClick="closeEditStockDialog();"><i class="fa fa-times"></i>CLOSE</button>
          </div>
        </div>
      </div>
    </div>
    
    
    
    

</div>


    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
    
    <script src="<?php echo ADMIN_SITE_URL;?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo ADMIN_SITE_URL;?>js/jquery.popupoverlay.js"></script>
    <script src="<?php echo ADMIN_SITE_URL;?>js/shortcut.js"></script>
	<script src="<?php echo ADMIN_SITE_URL;?>js/defaults.js"></script>
    <script src="<?php echo ADMIN_SITE_URL;?>js/logout.js"></script>
    <!-- HISRC Retina Images -->

    <!-- THEME SCRIPTS -->
    <script src="<?php echo ADMIN_SITE_URL;?>js/flex.js"></script>
    <script src="<?php echo ADMIN_SITE_URL;?>js/dashboard-demo.js"></script>
	<script src="<?php echo ADMIN_SITE_URL;?>js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo ADMIN_SITE_URL;?>js/prism.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ADMIN_SITE_URL;?>calendar/tcal.css" />
	<script type="text/javascript" src="<?php echo ADMIN_SITE_URL;?>calendar/tcal.js"></script> 
    <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
	function getforceno()
	{
		document.getElementById('userid').value=document.getElementById('forceno').value;
	}
    function showviewdata(num,fnumber,name,rank,panno,mobno,dob,fjoin,ndjoin,btjoin)
    {
        document.getElementById('slno').value=num;
        document.getElementById('forceno').value=fnumber;
        document.getElementById('name').value=name;
        document.getElementById('rank').value=rank;
        document.getElementById('dob').value=dob;
        document.getElementById('panno').value=panno;
        document.getElementById('mobileno').value=mobno;
        document.getElementById('fjoin').value=fjoin;
        document.getElementById('ndrfjoin').value=ndjoin;
        document.getElementById('btjoin').value=btjoin;
    }
	function showmeditdata(id, msg)
	{
        document.getElementById('editmessage').value=msg;	
		document.getElementById('messageid').value=id;
	}
	function searchforse()
	{
		if(document.getElementById('searchno').value == '')
		{
			alert("Enter Force No In Search Box");
			return false;
		}
		else{
			var fr_no=document.getElementById('searchno').value;
			dv = document.getElementById('load_subadmin');
			$("#load_subadmin").load("ajaxadmin.php?act=admindata&fr_no="+fr_no);	
		}
	}
	
	function findsearchforse(forceno)
	{
		var fr_no=forceno;
		dv = document.getElementById('load_subadmin');
		$("#load_subadmin").load("ajaxadmin.php?act=editadmindata&fr_no="+fr_no);	
		
	}
	
    function showeditdata(ids,fnum,name,rank,status)
    {
        document.getElementById('hidadminid').value=ids;
        document.getElementById('edtirla').value=fnum;
        document.getElementById('editname').value=name;
        document.getElementById('edtrank').value=rank;
        document.getElementById('edtstatus').value=status;
    }
	function AlertMessage(val)
	{
		if(val == 'Selected'){
			document.getElementById('selectemails').style.visibility = 'visible';
		}else{
			document.getElementById('selectemails').style.visibility = 'hidden';
		}
	}
	
	function salaryFor(val)
	{
		if(val == 'Selected'){
			document.getElementById('selectemails').style.visibility = 'visible';
			document.getElementById('selectename').style.visibility = 'visible';
			document.getElementById('selecterank').style.visibility = 'visible';
		}else{
			document.getElementById('selectemails').style.visibility = 'hidden';
			document.getElementById('selectename').style.visibility = 'hidden';
			document.getElementById('selecterank').style.visibility = 'hidden';
		}
	}
	
	function salaryForSelected(val)
	{
		if(val == 'Selected'){
			document.getElementById('selectemails').style.display = 'block'
			document.getElementById('selectename').style.display = 'block';
			document.getElementById('selecterank').style.display = 'block';
		}else{
			document.getElementById('selectemails').style.display = 'none';
			document.getElementById('selectename').style.display = 'none';
			document.getElementById('selecterank').style.display = 'none';
		}
	}
	
	
	function msgdelete(id)
	{
		if(confirm('Are you sure to delete this record?'))
		{
			dv = document.getElementById('SHOWMESSLIST');
			$("#SHOWMESSLIST").load("ajaxadmin.php?act=messagedata&str=delete&id="+id);	
		}	
	}
	function adminstatus(par, aid)
	{
		dv = document.getElementById('SHOWADMINLIST');
		$("#SHOWADMINLIST").load("ajaxadmin.php?act=adminlist&sta="+par+"&aid="+aid);	
	}
	function admindelete(aid)
	{
		if(confirm('Are you sure to delete this record?'))
		{
			dv = document.getElementById('SHOWADMINLIST');
			$("#SHOWADMINLIST").load("ajaxadmin.php?act=adminlist&str=delete&aid="+aid);	
		}	
	}
	function EmpSearch()
	{
		if(document.getElementById('empsearch').value == '')
		{
			alert("Enter Force No In Search Box");
			return false;
		}
		else{
			var fcno = document.getElementById('empsearch').value; 
			//$("#SHOWHISTORY").load("ajaxadmin.php?act=showhistory&forceno="+fcno);
			
		var dataString = "forceno="+fcno;
		$.ajax({ 
		type: "POST",url: "ajaxemployee.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			//alert(""+html);
			$("#SHOWDATA").html(html);
			//$('#battalion_joining_date').removeClass('form-control tcal');
			//$('#battalion_joining_date').addClass('form-control tcal tcalInput');
			//$("#SHOWINFO").html("<h4><i class='fa fa-exchange fa-fw'></i> Edit Employee </h4>");
			calcsalary();
		} 
		});
		
			//updateGPFLevel();
		}
	}
	
	function EmpFNSearch()
	{
		//alert("aaaaaaaa");
		if(document.getElementById('empsearch').value == '')
		{
			alert("Enter Force No In Search Box");
			return false;
		}
		else{
			var fcno = document.getElementById('empsearch').value; 
			
			var dataString = "forceno="+fcno;
			$.ajax({ 
			type: "POST",url: "ajaxFNSearch.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				//alert("hhhhh:"+html);
				var dobj=jQuery.parseJSON(html);
				var status = dobj.status;
				//alert("nnnnnnn:"+status);
				if(status == "1")
				{
					document.getElementById('namefield').value = dobj.name;
					document.getElementById('rankfield').value = dobj.rank;
				}
				else
					alert(fcno +" force no does not exists...");
			} 
			});
		
			//updateGPFLevel();
			}
	}
	
	
	function ModSearch(id)
	{
		dv = document.getElementById('SHOWDATA');
		var forceno = document.getElementById('empsearch').value;
		//$("#SHOWDATA").load("ajaxemployee.php?id="+id);
		var dataString = "id="+id;
		$.ajax({ 
		type: "POST",url: "ajaxemployee.php" ,data: dataString,cache: false,
		success: function(html)
		{ 
			$("#SHOWDATA").html(html);
			calcsalary();
		} 
		});
		
	}
	function showForceNo(par)      
	{
	    dv = document.getElementById('showEmployee');
		$("#showEmployee").load("ajaxforce.php?fnum="+par);
	}
	function showRank(par)      
	{
		dv = document.getElementById('showEmployee');
		$("#showEmployee").load("ajaxforce.php?rkid="+par);
	}
	function showCoy(par)      
	{
		dv = document.getElementById('showEmployee');
		$("#showEmployee").load("ajaxforce.php?coy="+par);
	}
	function showStatus(par)      
	{
		dv = document.getElementById('showEmployee');
		$("#showEmployee").load("ajaxforce.php?status="+par);
	}
	function ajaxpagination(page)
	{
		$("#showEmployee").load("ajaxforce.php?page="+page);	
		$("#SHOWADMINLIST").load("ajaxadmin.php?act=adminlist&page="+page);	
		$("#SHOWMESSLIST").load("ajaxadmin.php?act=messagedata&page="+page);	
	}
	function imgupload()
	{
		var str = document.frmemployee;
		var flag = true;
		document.getElementById('image').value=document.getElementById('bigimage1').src;
		
		if(flag == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	function isNumberKey(evt)
  	{
  		var charCode = (evt.which) ? evt.which : event.keyCode
  		if (charCode > 31 && (charCode < 48 || charCode > 57))
  			return false;
		return true;
  	}
    </script>
</body>
</html>
