var dashboard=angular.module('Channabasavashwara');
dashboard.controller('princpalPlanTimetableController',function($scope,$http,$state,$window,$rootScope){
	//$scope.session="2015/16";
	//$scope.type;
	
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	
	
	$scope.listOfdept=null;
	$scope.listOfdept=[{
		name:'Select Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfdept[0];
	
	$scope.noCourse=null;
	$scope.noCourse=[{
		name:'select Course',
		value:''
	}]
	$scope.courseName=$scope.noCourse[0];
		
	
	
	$scope.listOfCourseName=[{
		name: 'Select Couse Name',
		value: ''
	}]
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];	
	
	
	$scope.listPublish=[];
	$scope.listPublish=[{
		name: 'NO',
		value: '0'
	},{
		name: 'YES',
		value: '1'
	}]
	
	$scope.publish_name=$scope.listPublish[0];	
	
	
	$scope.listSection=[];
	$scope.listSection=[{
		name: 'Select Section',
		value: ''
	}]
	$scope.section_name=$scope.listSection[0];
	
	
	$scope.noSemesters=[];
	$scope.noSemesters = [{
    name :'Select Semester',
    value: ''
    }];	
	
	$scope.semester=$scope.noSemesters[0];
	
	
	$http({
		method: 'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	
	
	$scope.GetDeptData=function(){
		
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'stream_id':$scope.stream_name.value}
		$http({
			method:'POST',
			url:"php/department/readCollegeDeptData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
			});
		},function errorCallback(response) {
		});
		
		$scope.noCourse=null;
		$scope.noCourse=[{
			name:'select Course',
			value:''
		}]
		$scope.courseName=$scope.noCourse[0];
		
		var courseid={'stream_id':$scope.stream_name.value};
		$http({
			method:'POST',
			url:"php/course/readCollegeCourseData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourse.push(data);
			});
		},function errorCallback(response) {
		});
		
		//check data
		$scope.$scope.checkTableData();
	}
	
	$http({
		method: 'GET',
		url: "php/timetable/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	

/*
	$http({
		method: 'GET',
		url: "php/timetable/readVenue.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.venue_name);									
			var Venue={'name':obj.venue_name , 'value':obj.venue_id};
			$scope.listVenues.push(Venue);
		});
	},function errorCallback(response) {
		
	});
	*/
	
	$http({
		method: 'GET',
		url: "php/timetable/readSection.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.section_name);									
			var Section={'name':obj.section_name , 'value':obj.section_id};
			$scope.listSection.push(Section);
		});
	},function errorCallback(response) {
		
	});
	
	
	
	$http({
		method: 'GET',
		url: "php/timetable/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var user={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourseName.push(user);
		});
	},function errorCallback(response) {
	});
	
	$scope.course_name  = $scope.listOfCourseName[0];
	
	
	
	$scope.selectSemester=function(){
		
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select Semester',
			value:''
		}]
		
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':$scope.courseName.value};
		$http({
			method:'POST',
			url:"php/course/getCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				//alert("no_semester:"+no_semester);
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
				}
			});
		},function errorCallback(response) {
		});
		//check data
		$scope.$scope.checkTableData();
	}
	
	
	 $scope.noOfDays = [];
     $scope.listOfTimeData=[];
       /* $scope.days = {
          '0': "Monday",
          '1': 'Tuesday',
          '2': 'Wednesday',
          '3': 'Thursday',
          '4': 'Friday'
        }*/
        
        /*$scope.hours = [
          '9AM :: 10AM',
          '10AM :: 11AM',
          '11:15AM :: 12:15PM',
          '12:15PM :: 01:15PM',
          '02PM :: 03PM',
          '03PM :: 04PM',
          '04PM :: 05PM'
        ]*/
        $http({
			method: 'GET',
			url: "php/timetable/gethour.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hour',response.data);
			$scope.hours=response.data;
		},function errorCallback(response) {
			
		})
		$http({
			method: 'GET',
			url: "php/timetable/getdays.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			 $scope.days=response.data;
			 //console.log('days',$scope.days);
			for (var i = 0; i < 5; i++) {
               $scope.noOfDays.push($scope.days[i]);
            }
		},function errorCallback(response) {
		});
		$scope.listOfSubjectName=[{
		name: 'Select Subject',
		value: ''
	    }]
		$scope.sub_name  = $scope.listOfSubjectName[0];
		
		$scope.listOfFacultyName=[{
		name: 'Select Faculty',
		value: ''
	    }]
		$scope.fac_name=$scope.listOfFacultyName[0];
		
		
		$scope.listOfVenueName=[{
		name: 'Select Venue',
		value: ''
	    }]
		$scope.venue_name  = $scope.listOfVenueName[0];
		
		
		
		
		
		////////////////////////////////////////////////////////////////check table data
		
		$scope.checkTableData=function(){
		
		if(  $scope.dept_name.value != "" && $scope.courseName.value != "" && $scope.semester.value != "" && $scope.session_name.value != "" && $scope.section_name.value != "" )
		{
			
			var dataString = "stream_id="+$scope.stream_name.value+"&dept_id="+$scope.dept_name.value+"&course_id="+$scope.courseName.value+"&semester_id="+$scope.semester.value+"&session_id="+$scope.session_name.value+"&section_id="+$scope.section_name.value;
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/getPrincipalTimetableData.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
				//alert("Received data");
				var dobj=jQuery.parseJSON(html);
				//alert(":::Subject CNT::"+dobj.subject_count);
				$scope.updateFields(dobj);
				
				//alert("::"+dobj.subject_record);
				//alert("::"+dobj.subject_record1);
			} 
			});
			
			/*
			$http({
			method: 'POST',
			url:"php/timetable/getTimetableData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			//console.log('subject',response);
			alert("Received data");
			// $scope.tableData=response.data;
			var dobj=jQuery.parseJSON(response);
			alert(":::"+$scope.tableData);
			
			
			},function errorCallback(response) {
			
			});
			
			*/
		
		}
		
		/*
		var userdata={'course_name':$scope.course_name.value,'semester':$scope.semester.value}
		console.log('user',userdata);
		
		
		
		$http({
			method: 'POST',
			url:"php/timetable/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('subject',response);
			angular.forEach(response.data,function(obj1){
			var user={'name':obj1.subject_name,'value':obj1.subject_id};
			$scope.listOfSubjectName.push(user);
		});
			
		},function errorCallback(response) {
			
		});
		
		
		$http({
			method:'GET',
			url:"php/timetable/getFacultyData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj2){
			var user={'name':obj2.user_name,'value':obj2.user_id};
			$scope.listOfFacultyName.push(user);
		});
		},function errorCallback(response) {
		});
		*/
		
		}
		
		
		
		
		$scope.addTimeTableData=function(){
			console.log('add time',$scope.listOfTimeData);
			for(var i=0;i<= $scope.listOfTimeData.length;i++){
				$http({
					method:'POST',
					url: "php/timetable/addTimeTable.php",
					data:$scope.listOfTimeData[i],
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//alert(response.data['msg']);
				},function errorCallback(response) {
					//alert(response.data['msg']);
				});
			}
		}
		$scope.checkData=function(subname,facname,hour,day){
			//console.log('data',subname,facname,hour,day);
			if(subname.value==''){
				alert("select subject from list");
				$scope.fac_name=$scope.listOfFacultyName[0];
			}else{
				var subdata={'subtype':subname.value};
				$http({
					method:'POST',
					url: "php/timetable/getSubjectType.php",
					data:subdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('TYPE',response);
					$rootScope.type=response.data[0].subject_type;
					var time={'sub_name':subname.value,'fac_name':facname.value,'hour':hour.id,'day':day.id,'sub_type':$rootScope.type};
			       $scope.listOfTimeData.push(time);
				},function errorCallback(response) {
				});
			}
			//console.log('data',$scope.listOfTimeData);
		}
		
		
		$scope.updateFields=function(dobj){
			$("#detailsstockid").html(dobj.data);
			$("#data_hide").html(dobj.data_hide);
			//alert("::"+dobj.status)
			if(dobj.status == 1)
				document.getElementById("publish_name").value = 1;
			else
				document.getElementById("publish_name").value = 0;
			if(dobj.hod_status == 1)	
				document.getElementById("hod_approve").value = "1";
			else
				document.getElementById("hod_approve").value = "0";
			
			if(dobj.principal_status == 1)	
				document.getElementById("principal_approve").value = "1";
			else
				document.getElementById("principal_approve").value = "0";
			
			//alert()
			document.getElementById("tableid").value  = dobj.table_id;
			document.getElementById("hod_remarks").value  = dobj.hod_comment;
			document.getElementById("principal_remarks").value  = dobj.principal_comment;
			
		}
		
		

		
		
});



function updatePublish(){
		
		var session_id = document.getElementById("session_name").value;
		var section_id = document.getElementById("section_name").value;
		var course_id = document.getElementById("course_name").value;
		var semester_id = document.getElementById("semester").value;
		if(session_id > 0 && section_id > 0 && course_id > 0 && semester_id > 0)
		{
			if(confirm("Would you like to publish Time Table ?"))
			{
				var value = document.getElementById("publish_name").value;
				var dataString = "session_id="+session_id+"&section_id="+section_id+"&course_id="+course_id+"&semester_id="+semester_id+"&value="+value;
				//alert("Data:"+dataString);
				$.ajax({ 
				type: "POST",url: "php/timetable/updatePublish.php" ,data: dataString,cache: false,
					success: function(html)
					{ 
						
					} 
					});
			}
		}
		
	}
 

	
function resetField(tableid,period,day,time){
		var i=1;
		for(i ; i < period ;i++)
		{
			var value = time+i;
			document.getElementById("lab_subject_"+day+value).style.display = "none";
			document.getElementById("lab_faculty_"+day+value).style.display = "none";
			document.getElementById("lab_venue_"+day+value).style.display = "none";
			
			document.getElementById("subject_"+day+value).style.display = "block";
			document.getElementById("faculty_"+day+value).style.display = "block";
			document.getElementById("venue_"+day+value).style.display = "block";
		
			var subject_id = document.getElementById("subject_"+day+value).value;
			var faculty_id = document.getElementById("faculty_"+day+value).value;
			var venue_id = document.getElementById("venue_"+day+value).value;
			var subject_type = 1;
			var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
			} 
			});
		}
	
	}

function setField(tableid,period,day,time){
	var subject_name="";
	var faculty_name="";
	var venue_name="";
	var subject_id = document.getElementById("subject_"+day+time).value;
	var faculty_id = document.getElementById("faculty_"+day+time).value;
	var venue_id = document.getElementById("venue_"+day+time).value;
	
	if( subject_id > 0)
		subject_name = getSubjectname(day,time);
	if(faculty_id > 0)
		faculty_name = getFacultyName(day,time);
	if(venue_id > 0)
		venue_name = getVenueName(day,time);	
	
	var i=1;
	for(i ; i < period ;i++)
	{
			var value = time+i;
			document.getElementById("subject_"+day+value).style.display = "none";
			document.getElementById("lab_subject_"+day+value).style.display = "block";
			document.getElementById("lab_subject_"+day+value).innerHTML  = subject_name;
			
			document.getElementById("faculty_"+day+value).style.display = "none";
			document.getElementById("lab_faculty_"+day+value).style.display = "block";
			document.getElementById("lab_faculty_"+day+value).innerHTML  = faculty_name;
			
			document.getElementById("venue_"+day+value).style.display = "none";
			document.getElementById("lab_venue_"+day+value).style.display = "block";
			document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
			var subject_type = 0;
			var dataString = "day_id="+day+"&time_id="+value+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+subject_type+"&faculty_id="+faculty_id+"&venue_id="+venue_id;
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/updateAllRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
			} 
			});
	}
	
}
	
function getSubjectname(day,time){
		var subject_name = document.getElementById("subject_"+day+time).selectedOptions[0].text;
		return subject_name;
	}
	
function getFacultyName(day,time){
		var name = document.getElementById("faculty_"+day+time).selectedOptions[0].text;
		return name;
	}
	
function getVenueName(day,time){
		var name = document.getElementById("venue_"+day+time).selectedOptions[0].text;
		return name;
	}	

function getSubjectPeriod(day,time){
	var period=0;
	var subject_id = document.getElementById("subject_"+day+time).value;
	if(subject_id > 0)
		period = document.getElementById("subjecttype_"+subject_id).value;
	
	return period;
	}

function principalCheckSubjectRecord(day,time,tableid){
	//alert("aaaa");
	var period = 0;
	var subject_name = "";
	var subject_id = document.getElementById("subject_"+day+time).value;
	if(subject_id == '')
		subject_id = 0 ;
	
	period = getSubjectPeriod(day,time);
		
	if(period > 1)
	{
		resetField(tableid,3,day,time);
		setField(tableid,period,day,time);
	}
	else
	{
		resetField(tableid,3,day,time);
	}
		var dataString = "day_id="+day+"&time_id="+time+"&table_id="+tableid+"&subject_id="+subject_id+"&subject_type="+period;
		$.ajax({ 
		type: "POST",url: "php/timetable/addSubjectRecord.php" ,data: dataString,cache: false,
		success: function(html)
			{ 
					if(subject_id > 0)
						document.getElementById("subject_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("subject_"+day+time).style.backgroundColor="#FFE0D9";
			}	 
			});
	}
	



function principalCheckFacultyRecord(session_id,day,time,tableid){
	
	var user_id = document.getElementById("faculty_"+day+time).value;
	if(user_id == '')
		user_id = 0 ;

	var faculty_name="";
	var period = getSubjectPeriod(day,time);
	if(user_id > 0)
		faculty_name = getFacultyName(day,time);	
	
	var i=1;
	for(i ; i < period ;i++)
	{
			var value = time+i;
			document.getElementById("lab_faculty_"+day+value).innerHTML  = faculty_name;
			
			var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&faculty_id="+user_id;
			$.ajax({ 
			type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
			} 
			});
	}
	
	
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&faculty_id="+user_id;
	$.ajax({ 
	type: "POST",url: "php/timetable/addFacultyRecord.php" ,data: dataString,cache: false,
	success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.results)
				if(dobj.results == 1)
				{
					if(user_id > 0)
						document.getElementById("faculty_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
				}
				else
				{
					alert(dobj.error);
					document.getElementById("faculty_"+day+time).value = 0;
					document.getElementById("faculty_"+day+time).style.backgroundColor="#FFE0D9";
					
				}
			} 
			});
	
	
}
	
	
	
	
	function principalCheckVenueRecord(session_id,day,time,tableid){
	//sssalert("aaa");
	var venue_id = document.getElementById("venue_"+day+time).value;
	if(venue_id == '')
		venue_id = 0 ;
	
	
	var venue_name="";
	var period = getSubjectPeriod(day,time);
	if(venue_id > 0)
		venue_name = getVenueName(day,time);	
	//alert("aaaa:"+period);
	var i=1;
	for(i ; i < period ;i++)
	{
			var value = time+i;
			document.getElementById("lab_venue_"+day+value).innerHTML  = venue_name;
			//venue_id = 0;
			var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+value+"&table_id="+tableid+"&venue_id="+venue_id;
			
			//alert("Data:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
			success: function(html)
			{ 
					
			} 
			});
			
			
	}
	
	
	
	
	var dataString = "session_id="+session_id+"&day_id="+day+"&time_id="+time+"&table_id="+tableid+"&venue_id="+venue_id;
	//alert("Data:"+dataString);
	$.ajax({ 
	type: "POST",url: "php/timetable/addVenueRecord.php" ,data: dataString,cache: false,
	success: function(html)
			{ 
				var dobj=jQuery.parseJSON(html);
				if(dobj.results == 1)
				{
					if(venue_id > 0)
						document.getElementById("venue_"+day+time).style.backgroundColor="#CCF2CC";
					else
						document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
				}
				else
				{
					alert(dobj.error);
					document.getElementById("venue_"+day+time).value = 0;
					document.getElementById("venue_"+day+time).style.backgroundColor="#FFE0D9";
					
				}		
					
					
					
			} 
			});
	
	}
	
	
	function savePrincipalData1(){
		//alert("aaa");
		var table_id = document.getElementById("tableid").value ;
		if(table_id == "")
		{
			alert("Select Semester...");
			return;
		}
		var status = document.getElementById("principal_approve").value ;
		var remark = document.getElementById("principal_remarks").value ;
		var dataString = "table_id="+table_id+"&status="+status+"&remark="+remark;
		//alert("Data:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/princpaltime/updatePrincpalData.php" ,data: dataString,cache: false,
		success: function(html)
			{ 
				//alert("aaaaaaa");
				var dobj=jQuery.parseJSON(html);
				if(dobj.results == 1)
				{
					alert("Succesfully Saved...");		
				}
					
					
			} 
			});
		
	
	}
	
	
		