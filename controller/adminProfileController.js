var profile=angular.module('Channabasavashwara');
profile.controller('adminProfileController',function($scope,$state,$http,editProfile){
	$scope.buttonName="Update";
	$scope.hidebtn=true;
	$scope.showpass=true;
	$http({
		method:'GET',
		url:"php/userprofile/getProfileData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log(response.data);
		$scope.uname=response.data[0].user_name;
		$scope.emailid=response.data[0].email;
		$scope.mob_no=response.data[0].mob_no;
		$scope.login_name=response.data[0].login_name;
	},function errorCallback(response) {
	});
	$scope.clearData=function(){
		$state.go('dashboard.editprofile',{},{reload:true});
	}
	$scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $scope.updateProfileData=function(billdata){
		 if(billdata.$valid){
		 if($scope.uname==null || $scope.uname==''){
			 alert('user name field can not be blank');
			 editProfile.borderColor('u_name');
		 }else if($scope.emailid==""){
			 alert('email field can not be blank');
			 editProfile.borderColor('u_email');
		 }else if($scope.mob_no==""){
			 alert('mobile no field can not be blank');
			 editProfile.borderColor('u_mob');
		 }else if($scope.login_name==null){
			 alert('login name field can not be blank'); 
		 }else if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
			 alert('Password field can not be blank....');
			 editProfile.borderColor('u_pass');
		 }else{
			 var updatedata={'user_name':$scope.uname,'email':$scope.emailid,'mob_no':$scope.mob_no,'password':$scope.password,'login_name':$scope.login_name};
			 console.log('update',updatedata);
			 $http({
				 method:'POST',
				 url:"php/userprofile/updateProfileData.php",
				 data:updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 console.log('res',response);
				 alert(response.data['msg']);
				 $state.go('dashboard.editprofile',{},{reload:true});
			 },function errorCallback(response) {
				 alert(response.data['msg']);
			 })
		 }
		 }else{
			 if( billdata.email.$invalid){
			 alert("Please enter the valid email address");
			 }
			 if(billdata.mobno.$invalid){
				 alert("Please enter the valid mobile no");
			 }
			 if(billdata.pass.$invalid && $scope.showpass==false){
				 alert("Please enter the valid password");
			 }
		 }
	 }
	 $scope.resetPasswod=function(){
		 $scope.hidebtn=false;
	     $scope.showpass=false;
		 $scope.rejectpass=true;
	 }
	 $scope.closePass=function(){
		  $scope.hidebtn=true;
		  $scope.showpass=true;
		  $scope.rejectpass=false;
	 }
	 $scope.clearField=function(id){
		 editProfile.removeBorderColor(id);
	 }
});
profile.factory('editProfile',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#cccccc";
				}
			})
		}
	}
});