var dept=angular.module('Channabasavashwara');
dept.controller('adminDepartmentManagementController',function($scope,$http,$state,$window){
	$scope.Depttabs = {
    1: ($state.current.name === 'dashboard.deptmanagement.stream'),
    2: ($state.current.name === 'dashboard.deptmanagement.dept'),
    3: ($state.current.name === 'dashboard.deptmanagement.course'),
	4: ($state.current.name === 'dashboard.deptmanagement.sub')
    };
});