var dashboard=angular.module('Channabasavashwara');
dashboard.controller('princpalResourceController',function($scope,$http,$state,$window){
	$scope.principalRestabs = {
    1: ($state.current.name === 'principal.resourse.userrole'),
    2: ($state.current.name === 'principal.resourse.class'),
    3: ($state.current.name === 'principal.resourse.section'),
	4: ($state.current.name === 'principal.resourse.session'),
	5: ($state.current.name === 'principal.resourse.unit'),
	6: ($state.current.name === 'principal.resourse.venue'),
	7: ($state.current.name === 'principal.resourse.day'),
	8: ($state.current.name === 'principal.resourse.time'),
	9: ($state.current.name === 'principal.resourse.lecturePlan')
    };
});
