var hoduser=angular.module('Channabasavashwara');
hoduser.controller('hodUserController',function($state,$http,$scope,$window,userField){
	$scope.buttonName="Add";
	 var id='';
	 var log_name='';
	 $scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $http({
		method: 'GET',
		url: "php/user/readHodUserData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objHodUserData=response.data;
	},function errorCallback(response) {
		
	});
	 $scope.addUserData=function(billdata){
		 //console.log('button name',$('#addProfileData')[0].defaultValue);
		 if(billdata.$valid){
		 if($scope.buttonName=="Add"){
			if($scope.first_name==null || $scope.first_name==''){
			 alert("first name field can not be blank");
			 userField.borderColor('fstname');
		 }else if($scope.last_name==null || $scope.last_name==''){
			 alert("last name field can not be blank");
			 userField.borderColor('lstname');
		 }else if($scope.mob_no==null || $scope.mob_no==''){
			 alert("Mobile no  field can not be blank");
			 userField.borderColor('mobno');
		 }else if($scope.user_email==null || $scope.user_email==''){
			 alert("Email field can not be blank");
			 userField.borderColor('uemail');
		 }else if($scope.login_name==null || $scope.login_name==''){
			 alert("user Name  field can not be blank");
			 userField.borderColor('uname');
		 }else if($scope.password==null || $scope.password=='' ){
			 alert("password field can not be blank");
			 userField.borderColor('passno');
		 }else if($scope.user_status==null || $scope.user_status==''){
			  alert("select user status");
			  userField.borderColor('status');
		 }else{
			 var userdata={'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status};
			 //console.log('user',userdata);
			 $http({
				 method: 'POST',
				 url: "php/user/addHodUserData.php",
				 data: userdata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				// console.log(response);
				  alert(response.data['msg']);
			      $state.go('hod.user',{}, { reload: true });
			 },function errorCallback(response) {
				 alert(response.data['msg']);
				 if($scope.user_email==response.data.email){
					 userField.borderColor('uemail');
				 }else if($scope.mob_no==response.data.mob_no){
					userField.borderColor('mobno'); 
				 }else if($scope.login_name==response.data.login_name){
					 userField.borderColor('uname');
				 }else{
				  $state.go('hod.user',{}, { reload: true });
				 }
			 });
		 }
		 }
		 if($scope.buttonName=="Update"){
			if($scope.first_name==null || $scope.first_name==''){
			 alert("first name field can not be blank");
			 userField.borderColor('fstname');
		 }else if($scope.last_name==null || $scope.last_name==''){
			 alert("last name field can not be blank");
			 userField.borderColor('lstname');
		 }else if($scope.mob_no==null || $scope.mob_no==''){
			 alert("Mobile no  field can not be blank");
			 userField.borderColor('mobno');
		 }else if($scope.user_email==null || $scope.user_email==''){
			 alert("Email field can not be blank");
			 userField.borderColor('uemail');
		 }else if($scope.login_name==null || $scope.login_name==''){
			 alert("user Name  field can not be blank");
			 userField.borderColor('uname');
		 }else if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
			 alert("password field can not be blank");
			 userField.borderColor('passno');
		 }else if($scope.user_status==null || $scope.user_status==''){
			  alert("select user status");
			  userField.borderColor('status');
		 }else{
			 var updatedata={'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status,'user_id':id,'log_name':log_name};
			 console.log('user',updatedata);
			 $http({
				 method: 'POST',
				 url: "php/user/updateHodUserData.php",
				 data: updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				 //console.log('updated',response);
				 alert(response.data['msg']);
			     $state.go('hod.user',{}, { reload: true });
			 },function errorCallback(response) {
				 console.log('err response',response);
				alert(response.data['msg']);
				if($scope.user_email==response.data.email){
					 userField.borderColor('uemail');
				 }else if($scope.mob_no==response.data.mob_no){
					userField.borderColor('mobno'); 
				 }else if($scope.login_name==response.data.login_name){
					 userField.borderColor('uname');
				 }else{
				  $state.go('hod.user',{}, { reload: true });
				 }
			 });
		 }
		 }
		 }else{
			 if(billdata.mobno.$invalid){
			  alert('Please enter a valid mobile no');
			 }
			 if(billdata.email.$invalid){
				 alert('Please enter valid email id');
			 }
			 if(billdata.uname.$invalid){
				  alert('Please enter valid User name');
			 }
			 if(billdata.pass.$invalid){
				   alert('Please enter valid password');
			 }
		 }
	 }
	 $scope.editUserData=function(uid){
		 id=uid;
		 var userdata={'userid':id};
		 $http({
			method: 'POST',
			url: "php/user/editCollegeUserData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.first_name=response.data[0].first_name;
			$scope.last_name=response.data[0].last_name;
			$scope.mob_no=response.data[0].mob_no;
			$scope.user_email=response.data[0].email;
			$scope.login_name=response.data[0].login_name;
			log_name=response.data[0].login_name;
			$scope.user_status=response.data[0].user_status;
			$scope.buttonName="Update";
			//$scope.readcolg=true;
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
			$scope.showpass=true;
			$scope.hidebtn=true;
		},function errorCallback(response) {
		});
	 }
	 $scope.deleteUserData=function(uid){
		 var deleteUser=$window.confirm('Are you absolutely sure want to delete?');
		 if(deleteUser){
			 id=uid;
			 var userdata={'userid':id};
			 $http({
			method: 'POST',
			url: "php/user/deleteCollegeUserData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data['msg']);
			 $state.go('hod.user',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			 $state.go('hod.user',{}, { reload: true });
		});
		 }
	 }
	 $scope.clearField=function(id){
		userField.clearBorderColor(id);
	}
	$scope.cancelUserData=function(){
		$state.go('hod.user',{}, { reload: true });
	}
	$scope.resetPasswod=function(){
		$scope.hidebtn=false;
		$scope.showpass=false;
		$scope.rejectpass=true;
	}
	$scope.closePass=function(){
		$scope.showpass=true;
		$scope.rejectpass=false;
		$scope.hidebtn=true;
	}
	 
});
hoduser.factory('userField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});