var Admin=angular.module('Channabasavashwara',['ui.router', '720kb.datepicker','ngMessages','ngCapsLock','ui.bootstrap']);
Admin.run(function($rootScope, $state) {
      $rootScope.$state = $state;
    });
Admin.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
	 .state('/', { /*....This state defines All type of user login...*/
            url: '/',
            templateUrl: 'dashboardview/login.html',
			controller: 'loginController'
        })
		
		/*.........START OF ROOT ADMIN STATES.........*/
		.state('dashboard', { /* This state defines The root admin login page. */
            url: '/dashboard',
			templateUrl: 'dashboardview/dashboard.html',
			controller: 'adminController'
        })
		.state('dashboard.profile', {  /* This states defines College profile page */
        url: '/profile',
        templateUrl: 'dashboardview/profile.html',
        controller: 'adminCollegeProfileController'
    })
	.state('dashboard.res', {  /* This state defines  the home page of Resource management ...*/
        url: '/res',
        templateUrl: 'dashboardview/res.html',
        controller: 'adminResController'
    })
	.state('dashboard.res.userrole', {  /* ....This state defines the user role page..*/
        url: '/userrole',
        templateUrl: 'dashboardview/userrole.html',
        controller: 'adminResourceUserRoleController'
    })
	.state('dashboard.res.class', { /* This state defines the add class page of resource management..*/
        url: '/SubjectType',
        templateUrl: 'dashboardview/class.html',
        controller: 'adminResourceClassController'
    })
	.state('dashboard.res.section', { /* This state defines the add section page of resource management..*/
        url: '/section',
        templateUrl: 'dashboardview/section.html',
        controller: 'adminResourceSectionController'
    })
	.state('dashboard.res.session', { /* This state defines the add session page of resource management..*/
        url: '/session',
        templateUrl: 'dashboardview/session.html',
        controller: 'adminResourceSessionController'
    })
	.state('dashboard.res.unit', { /* This state defines the add unit page of resource management..*/
        url: '/unit',
        templateUrl: 'dashboardview/unit.html',
        controller: 'adminResourceUnitController'
    })
	.state('dashboard.res.venue', { /* This state defines the add venue page of resource management..*/
        url: '/venue',
        templateUrl: 'dashboardview/venue.html',
        controller: 'adminResourceVenueController'
    })
	.state('dashboard.res.day', { /* This state defines the add day page of resource management..*/
        url: '/class_day',
        templateUrl: 'dashboardview/day.html',
        controller: 'adminResourceDayController'
    })
	.state('dashboard.res.time', { /* This state defines the add time page of resource management..*/
        url: '/class_time',
        templateUrl: 'dashboardview/time.html',
        controller: 'adminResourceTimeController'
    })
	.state('dashboard.res.lecturePlan', { /* This state defines the add Lecture Plan page of resource management..*/
        url: '/lecturePlan',
        templateUrl: 'dashboardview/lecturePlan.html',
        controller: 'adminResourceLecturePlanController'
    })
	.state('dashboard.deptmanagement', { /* This state defines the department management page */
        url: '/dept',
        templateUrl: 'dashboardview/deptmanagement.html',
        controller: 'adminDepartmentManagementController'
    })
	.state('dashboard.deptmanagement.stream', { /* This state defines Add Stream page from department management module */
        url: '/stream',
        templateUrl: 'dashboardview/stream.html',
        controller: 'adminDeptManagementStreamController'
    })
	.state('dashboard.deptmanagement.course', { /* This state defines Add Course page from department management module */
        url: '/course',
        templateUrl: 'dashboardview/course.html',
        controller: 'adminDeptManagementCourseController'
    })
	.state('dashboard.deptmanagement.sub', { /* This state defines Add Subject page from department management module */
        url: '/subject',
        templateUrl: 'dashboardview/subject.html',
        controller: 'adminDeptManagementSubjectController'
    })
	.state('dashboard.deptmanagement.dept', { /* This state defines Add Department page from department management module */
        url: '/dept',
        templateUrl: 'dashboardview/dept.html',
        controller: 'adminDeptManagementDeptController'
    })
	.state('dashboard.user', {   /* This state defines user management page */
        url: '/user',
        templateUrl: 'dashboardview/user.html',
        controller: 'adminUserManagementController'
    })
	.state('dashboard.user.usermanagement', {  /* This state defines Add User of User management Module */
        url: '/usermanagement',
        templateUrl: 'dashboardview/usermangement.html',
        controller: 'adminUserManagementUserController'
    })
	.state('dashboard.user.role', {   /* This state defines Add User Role of User management Module */
        url: '/role',
        templateUrl: 'dashboardview/role.html',
        controller: 'adminUserManagementRoleController'
    })
	.state('dashboard.editprofile', {  /* This state defines to edit admin's own profile */
        url: '/editprofile',
        templateUrl: 'dashboardview/editprofile.html',
        controller: 'adminProfileController'
    })
	.state('dashboard.plan', {   /* This state defines Plan Management page */
        url: '/plan',
        templateUrl: 'dashboardview/plan.html',
        controller: 'adminPlanManagementController'
    })
	.state('dashboard.plan.timetable', {  /* This state defines Add timetable from Plan Management Module */
        url: '/TimeTable',
        templateUrl: 'dashboardview/timeTable.html',
        controller: 'adminPlanManagementTimeTableController'
    })
	.state('dashboard.plan.facultytimetable', {  /* This state defines Add Faculty timetable from Plan Management Module */
        url: '/FacultyTimeTable',
        templateUrl: 'dashboardview/facultytimeTable.html',
        controller: 'adminPlanManagementFacultyTimeTableController'
    })
	.state('dashboard.plan.facultyplan', {   /* This state defines Add Plan timetable from Plan Management Module */
        url: '/FacultyPlan',
        templateUrl: 'dashboardview/myplan.html',
        controller: 'adminPlanManagementPlanController'
    })
	.state('dashboard.plan.facwds', {  /* This state defines Add WDS timetable from Plan Management Module */
        url: '/WDS',
        templateUrl: 'dashboardview/wds.html',
        controller: 'adminPlanManagementWdsController'
    })
	
	
	<!-------------- DEPARTMENT ADMIN START ------------------->
	.state('home',{
		url: '/home',
		templateUrl: 'homeview/home.html',
		controller: 'deptAdminHomeController'
	})
	.state('home.subject', {
        url: '/subject',
		templateUrl: 'homeview/subject.html',
        controller: 'deptAdminSubjectController'
    })
	.state('home.user', {
        url: '/user',
		templateUrl: 'homeview/usermanagement.html',
        controller: 'deptAdminUserController'
    })
	.state('home.userrole',{
		url: '/userrole',
		templateUrl: 'homeview/userrole.html',
        controller: 'deptAdminUserRoleController'
	})
	.state('home.timeTable', {
        url: '/timeTable',
        templateUrl: 'homeview/timeTable.html',
        controller: 'deptAdmintimeTableController'
    })
	.state('home.facultytime',{
		url: '/facultytime',
		templateUrl: 'homeview/facultytimeTable.html',
		controller: 'deptAdminFacultyTimeTableController'
	})
	.state('home.plan', {
        url: '/FacultyPlan',
        templateUrl: 'homeview/plan.html',
        controller: 'deptAdminPlanController'
    })
	.state('home.wds', {
        url: '/WDS',
        templateUrl: 'homeview/wds.html',
        controller: 'deptAdminWdsController'
    })
	.state('home.profile', {
        url: '/profile',
        templateUrl: 'homeview/profile.html',
        controller: 'deptAdminProfileController'
    })
	
	
	<!-------------- DEPARTMENT ADMIN END ------------------->
	<!-------------- USER START ------------------->
	.state('user',{
		url: '/user',
		templateUrl: 'userview/user.html',
		controller: 'userController'
	})
	
	.state('user.mytimetable',{
		url: '/mytimetable',
		templateUrl: 'userview/mytimetable.html',
		controller: 'userTimeTableController'
	})
	
	.state('user.myplan',{
		url: '/myplan',
		templateUrl: 'userview/myplan.html',
		controller: 'userPlanController'
	})
	
	.state('user.mywds',{
		url: '/mywds',
		templateUrl: 'userview/mywds.html',
		controller: 'userWDSController'
	})
	.state('user.profile',{
		url: '/profile',
		templateUrl: 'userview/profile.html',
		controller: 'userProfileController'
	})
	<!-------------- USER END ------------------->
	
	<!-------------- PRINCIPAL START ------------------->
	.state('principal',{   /* This state defines The principal Home page */
		url: '/principal',
		templateUrl: 'princpalview/princpal.html',
		controller: 'princpalHomeController'
	})
	.state('principal.dept', { /* This state defines The Department Management Home page */
        url: '',
        templateUrl: 'princpalview/dept.html',
        controller: 'pricipalDeptController'
    })
	.state('principal.dept.stream', { /* This state defines Add Stream from Department Management Module */
        url: '/stream',
        templateUrl: 'princpalview/stream.html',
        controller: 'pricipalDeptStreamController'
    })
	
	.state('principal.dept.department', { /* This state defines Add Department from Department Management Module */
        url: '/department',
        templateUrl: 'princpalview/department.html',
        controller: 'pricipalDeptDepartmentController'
    })
	
	.state('principal.dept.course', { /* This state defines Add Course from Department Management Module */
        url: '/course',
        templateUrl: 'princpalview/course.html',
        controller: 'pricipalDeptCourseController'
    })
	
	.state('principal.dept.subject', { /* This state defines Add Subject from Department Management Module */
        url: '/subject',
        templateUrl: 'princpalview/subject.html',
        controller: 'pricipalDeptSubjectController'
    })
	.state('principal.resourse', { /* This state defines the Resource Management Module */
        url: '/resourse',
        templateUrl: 'princpalview/res.html',
        controller: 'princpalResourceController'
    })
	.state('principal.resourse.userrole', { /* This state defines Add User Role  from Resource Management Module */
        url: '/userrole',
        templateUrl: 'princpalview/userrole.html',
        controller: 'princpalResourceUserRoleController'
    })
	.state('principal.resourse.class', { /* This state defines Add Class  from Resource Management Module */
        url: '/class',
        templateUrl: 'princpalview/class.html',
        controller: 'princpalResourceClassController'
    })
	.state('principal.resourse.section', { /* This state defines Add Section  from Resource Management Module */
        url: '/section',
        templateUrl: 'princpalview/section.html',
        controller: 'princpalResourceSectionController'
    })
	.state('principal.resourse.session', { /* This state defines Add Session  from Resource Management Module */
        url: '/session',
        templateUrl: 'princpalview/session.html',
        controller: 'princpalResourceSessionController'
    })
	.state('principal.resourse.unit', { /* This state defines Add Unit  from Resource Management Module */
        url: '/unit',
        templateUrl: 'princpalview/unit.html',
        controller: 'princpalResourceUnitController'
    })
	.state('principal.resourse.venue', { /* This state defines Add Venue  from Resource Management Module */
        url: '/venue',
        templateUrl: 'princpalview/venue.html',
        controller: 'princpalResourceVenueController'
    })
	.state('principal.resourse.day', { /* This state defines the add Day page from Resource Management Module..*/
        url: '/class_day',
        templateUrl: 'princpalview/day.html',
        controller: 'princpalResourceDayController'
    })
	.state('principal.resourse.lecturePlan', { /* This state defines the add Day page from Resource Management Module..*/
        url: '/lecturePlan',
        templateUrl: 'princpalview/lecturePlan.html',
        controller: 'princpalResourceLecturePlanController'
    })
	.state('principal.resourse.time', { /* This state defines the add Time page from Resource Management Module..*/
        url: '/class_time',
        templateUrl: 'princpalview/time.html',
        controller: 'princpalResourceTimeController'
    })
	.state('principal.planmanagement', {
        url: '/unit',
        templateUrl: 'princpalview/planmanagement.html',
        controller: 'planmanagementPrincpalController'
    })
	.state('principal.user', {  /* This state defines the Home page User Management Module */
        url: '/user',
        templateUrl: 'princpalview/userprincpal.html',
        controller: 'princpalUserManagementController'
    })
	.state('principal.user.usermanagement', {  /* This state defines the Add user section of  User Management Module */
        url: '/adduser',
        templateUrl: 'princpalview/usermanagement.html',
        controller: 'princpalUserManagementUserController'
    })
	.state('principal.user.userrole', {  /* This state defines the Add user Role section of  User Management Module */
        url: '/role',
        templateUrl: 'princpalview/role.html',
        controller: 'princpalUserManagementUserRoleController'
    })
	.state('principal.plan', { /* This state defines the Home page Of Plan Management Module */
        url: '/plan',
        templateUrl: 'princpalview/planprincpal.html',
        controller: 'princpalPlan'
    })
	.state('principal.plan.timetable', { /* This state defines Time Table Of Plan Management Module */
        url: '/timetable',
        templateUrl: 'princpalview/timeTable.html',
        controller: 'princpalPlanTimetableController'
    })
	.state('principal.plan.facultytimetable', { /* This state defines  Faculty Time Table Of Plan Management Module */
        url: '/facultytimetable',
        templateUrl: 'princpalview/facultytimeTable.html',
        controller: 'princpalPlanFacultyTimetableController'
    })
	.state('principal.plan.facultyplan', { /* This state defines  Faculty Plan Of Plan Management Module */
        url: '/facultyplan',
        templateUrl: 'princpalview/myplan.html',
        controller: 'princpalPlanController'
    })
	.state('principal.plan.facultywds', { /* This state defines WDS Of Plan Management Module */
        url: '/facultywds',
        templateUrl: 'princpalview/wds.html',
        controller: 'princpalwdsController'
    })
	.state('principal.profile', { /* This state defines WDS Of Plan Management Module */
        url: '/profile',
        templateUrl: 'princpalview/profile.html',
        controller: 'princpalProfileController'
    })
	.state('principal.myplanManagement', { /* This state defines WDS Of Plan Management Module */
        url: '/myplanManagement',
        templateUrl: 'princpalview/myplanManagement.html',
        controller: 'princpalMyplanManagementController'
    })
	.state('principal.myplanManagement.myPlan', { /* This state defines Plan  OF My Plan Management Module */
        url: '/myPlan',
        templateUrl: 'princpalview/myIndividualplan.html',
        controller: 'princpalMyplanController'
    })
	.state('principal.myplanManagement.myTimeTable', { /* This state defines TimeTable Of My Plan Management Module */
        url: '/myTimeTable',
        templateUrl: 'princpalview/mytimetable.html',
        controller: 'princpalMyTimeTableController'
    })
	.state('principal.myplanManagement.myWDS', { /* This state defines WDS Of Plan Management Module */
        url: '/myWDS',
        templateUrl: 'princpalview/mywds.html',
        controller: 'princpalMyWDSController'
    })
	<!-------------- PRINCIPAL END ------------------->
	
	
	
	
	<!-------------- HOD START ------------------->
	.state('hod',{   /* This state defines Home Page Of HOD Module */
		url: '/hod',
		templateUrl: 'hodview/hod.html',
		controller: 'hodHomeController'
	})
	.state('hod.subject',{ /* This state defines Subject Page Of HOD Module */
		url: '/subject',
		templateUrl: 'hodview/subject.html',
        controller: 'hodSubjectController'
	})
	.state('hod.user',{ /* This state defines Add User Page Of HOD Module */
		url: '/user',
		templateUrl: 'hodview/usermanagement.html',
        controller: 'hodUserController'
	})
	.state('hod.userrole',{ /* This state defines Add User Role Page Of HOD Module */
		url: '/userrole',
		templateUrl: 'hodview/userrole.html',
        controller: 'hodUserRoleController'
	})
	.state('hod.timeTable',{ /* This state defines Add TimeTable Page Of HOD Module */
		url: '/timeTable',
		templateUrl: 'hodview/timeTable.html',
		controller: 'hodTimeTableController'
	})
	.state('hod.facultytime',{  /* This state defines Add Faculty TimeTable Page Of HOD Module */
		url: '/facultytime',
		templateUrl: 'hodview/facultytimeTable.html',
		controller: 'hodFacultyTimeTableController'
	})
	.state('hod.plan',{ /* This state defines Add plan Page Of HOD Module */
		url: '/plan',
		templateUrl: 'hodview/myplan.html',
		controller: 'hodPlanController'
	})
	.state('hod.wds',{ /* This state defines Add wds Page Of HOD Module */
		url: '/wds',
		templateUrl: 'hodview/wds.html',
        controller: 'hodWDSController'
	})
	.state('hod.profile',{ /* This state defines edit Profile Page Of HOD Module */
		url: '/profile',
		templateUrl: 'hodview/profile.html',
        controller: 'hodProfileController'
	})
	.state('hod.myplanManagement', { /* This state defines WDS Of Plan Management Module */
        url: '/myplanManagement',
        templateUrl: 'hodview/myplanManagement.html',
        controller: 'hodMyplanManagementController'
    })
	.state('hod.myplanManagement.myPlan', { /* This state defines My Plan Of Plan Management Module */
        url: '/myPlan',
        templateUrl: 'hodview/myIndividualplan.html',
        controller: 'hodMyplanController'
    })
	.state('hod.myplanManagement.myTimeTable', { /* This state defines Time Table Of My Plan Management Module */
        url: '/myTimeTable',
        templateUrl: 'hodview/mytimetable.html',
        controller: 'hodMyTimeTableController'
    })
	.state('hod.myplanManagement.myWDS', { /* This state defines WDS Of Plan Management Module */
        url: '/myWDS',
        templateUrl: 'hodview/mywds.html',
        controller: 'hodMyWDSController'
    })
	/*$locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });	*/
	<!-------------- HOD END ------------------->
})
