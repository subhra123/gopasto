var plan=angular.module('Channabasavashwara');
plan.controller('planController',function($scope,$http,$state,$window){
	
	$scope.user_readonly= false;
	
	
	$scope.buttonName="Add";
	$scope.showAddUnit=false;
	
	$scope.unit_list=true;
	$scope.plan_list=false;
	
	$scope.topic = "";
	$scope.clearButtonName="Cancel";
	$scope.showCancel=false;
	var id='';
	$scope.listOfLession=[{
		name:'Select Lession plan',
		value:''
	}
	];
	
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];	
	
	
	$http({
		method: 'GET',
		url: "php/facultytimetable/readFaculty.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
	},function errorCallback(response) {
		
	});
	
	
	
	$scope.new_unit_name="";
	
	$scope.viewUnitData=[];
	$scope.viewPlanData=[];
	$scope.listUnitData=[];
	$scope.listUnitData=[{
		name:'Select Unit',
		value:''
	}
	];
	$scope.unit_name=$scope.listUnitData[0];
	
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Session',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];
	
	
	/*$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Subject',
		value: ''
	}]*/
	
	$scope.listSubject=[{
			name: 'Select Subject',
			value: ''
		}]
	
	$scope.sub_name = $scope.listSubject[0];
	
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	
	$http({
		method:'GET',
		url:"php/plan/getLession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.lession_name,'value':obj.lession_id};
			$scope.listOfLession.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.lession=$scope.listOfLession[0];
	$scope.listOfCourse=[{
		name:'Select Course',
		value:''
		}];
	$http({
		method:'GET',
		url:"php/plan/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var data={'name':obj1.course_name,'value':obj1.course_id};
			$scope.listOfCourse.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.course_name=$scope.listOfCourse[0];
	
	$scope.noSemesters=[{
		name:'Select Semester',
		value:''
	}];
	
	$scope.semester=$scope.noSemesters[0];
	
	$scope.listOfSubject=[{
		name:'Select Subject',
		value:''
	}];
	
	
	$scope.subject_name=$scope.listOfSubject[0];
	
	$scope.listOfSection=[{
		name:'Select section',
		value:''
	}];
	$http({
		method:'GET',
		url:"php/plan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSection.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.section=$scope.listOfSection[0];
	$scope.noSemesters=[];
	$scope.selectedCourse=function(){
		     $scope.noSemesters=null;
			 $scope.noSemesters=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'id':$scope.course_name.value};
			 $http({
				 method:'POST',
				 url:"php/plan/getSemesterData.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 if($scope.course_name.name==response.data[0].course_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.noSemesters.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	
	
	$scope.selectedSemester=function(){
		
		$scope.listOfSubject=null;
		$scope.listOfSubject=[];
		$scope.listOfSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		//alert("semester_id::"+semester_id);
		var userdata={'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/plan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			//alert("::"+obj.subject_name);									
			var Subject={'name':obj.subject_type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSubject.push(Subject);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
		
	}
	
	
	
	$http({
		method:'GET',
		url:"php/plan/readUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response.data);
		$scope.viewUnitData=response.data;
	},function errorCallback(response) {
	});
	
	
	/*
	$http({
		method:'GET',
		url:"php/plan/readPlanData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		$scope.listOfPlanData=response.data;
	},function errorCallback(response) {
	});
	*/
	
	
	$scope.addPlanData=function(){
		//alert("::"+$scope.buttonName);
		if($scope.buttonName=="Add"){
		
		if($scope.unit_name.value==""){
			alert('Please Select Unit Name');
		}else if($scope.date==null){
			alert('Please select the date');
		}else if($scope.plan==''){
			alert('Please add Lession Plan');
		}else if($scope.topic==''){
			alert('Please add the topic');
		}else{
			
			//alert(":::Topic:"+$scope.topic);
			
			var dataString = "unit_id="+$scope.unit_name.value+"&date="+$scope.date+"&lession_plan="+$scope.plan+"&topic="+$scope.topic;
			//alert("dataString:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/plan/addPlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					$scope.viewUnitData= null;
					$http({
					method:'GET',
					url:"php/plan/readUnitData.php",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
					},function errorCallback(response) {
					});
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					alert("New Plan added successfully...");
				}
			} 
			});
			
			
			
			
		}
		
		
		}
		if($scope.buttonName=="Update"){
			//alert("aaaaaaa");
			if($scope.date==null){
			alert('Please select the date');
			}else if($scope.plan==null){
			alert('Please add the plan');
			}else if($scope.topic==null){
			alert('Please add the topic');
			}else{
			
			var dataString = "unit_id="+temp_unit_id+"&plan_id="+id+"&date="+$scope.date+"&lession_plan="+$scope.plan+"&topic="+$scope.topic;
			//alert("::"+dataString);
			
			$.ajax({ 
			type: "POST",url: "php/plan/updatePlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					var updatedata={'unit_id':temp_unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/plan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						console.log('res',response.data);
						$scope.viewPlanData=response.data;
						//document.getElementById("unit_list").style.display = "none";
						//document.getElementById("plan_list").style.display = "block";
					},function errorCallback(response) {
						
						
					});
					
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					$scope.clearPlanData();
					alert("Plan updated successfully...");
				
				}
			} 
			});
			
			
			
			
			
		}
		}
	}
	
	$scope.addUnit=function(){
		if($scope.session_name.value==''){
			alert('Please select Session');
		}else if($scope.course_name.value==''){
			alert('Please select the course name');
		}else if($scope.semester.value==''){
			alert('Please select the semester');
		}else if($scope.subject_name.value==''){
			alert('Please select the subject name');
		}else if($scope.section.value==''){
			alert('Please select the section');
		}else{
				//alert("Add Unit");
				document.getElementById("addunit").style.display ="block";
			}
	}
	
	
	$scope.checkTableData=function(){
		if($scope.session_name.value != "" && $scope.subject_name.value != "" && $scope.section.value != "" && $scope.faculty_name.value != "")
		{
			//alert("get unit");
			
			$scope.listUnitData=[];
			$scope.listUnitData=[{
				name:'Select Unit',
				value:''
			}
			];
	
			var updatedata={'session_id':$scope.session_name.value,'course_id':$scope.course_name.value,'semester_id':$scope.semester.value,'subject_id':$scope.subject_name.value,'section_id':$scope.section.value,'user_id':$scope.faculty_name.value};
			$http({
				method:'POST',
				url:"php/plan/getUnitName.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				//alert("::"+obj.unit_name);									
				var Session={'name':obj.unit_name , 'value':obj.unit_id};
					$scope.listUnitData.push(Session);
				});
				
			},function errorCallback(response) {
				
				
			});
		}
		
	}
	
	
	$scope.closeUnitDialog=function(){
			document.getElementById("addunit").style.display ="none";
	}
	
	
	$scope.getUnitData=function(){

		var user_id = $scope.faculty_name.value;
		var session_id = $scope.session_name.value;
		//alert("unit_name::"+unit_name+"::");
		if(user_id != "" && session_id != "")
		{
			$scope.viewUnitData= null;
			var userid={'user_id':user_id , 'session_id':session_id};
			$http({
				method:'POST',
				url:"php/plan/readUnitData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
			
			$scope.viewUnitData=response.data;
		
			},function errorCallback(response) {
			});	
			
		}
	
	}
	
	
	
	$scope.editPlanData=function(pid,unit_id){
		id=pid;
		temp_unit_id = unit_id;
		var planid={'plan_id':id};
		$http({
			method:'POST',
			url:"php/plan/editPlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
			$scope.date=response.data[0].date;
			$scope.plan=response.data[0].plan;
			$scope.topic=response.data[0].topic;
			$scope.user_readonly= true;
			$scope.showCancel =  true;
			$scope.buttonName="Update";
		
		},function errorCallback(response) {
		});
		
	}
	
	
	$scope.showPlanData=function(unit_id){
		//alert("unit_id:"+unit_id);
		var updatedata={'unit_id':unit_id};
		
		$scope.viewPlanData=null;
		$scope.viewPlanData=[];
			
			$http({
				method:'POST',
				url:"php/plan/readPlanData.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res',response.data);
				$scope.viewPlanData=response.data;
				document.getElementById("unit_list").style.display = "none";
				document.getElementById("plan_list").style.display = "block";
			},function errorCallback(response) {
				
				
			});
		
		
	
	}
	
	$scope.showUnitData=function(){
		document.getElementById("unit_list").style.display = "block";
		document.getElementById("plan_list").style.display = "none";
	
	}
	
	
	
	
	
	
	
	$scope.deletePlanData=function(pid,unit_id){
		id=pid;
		var planid={'plan_id':id , 'unit_id':unit_id};
		var deleteUser = $window.confirm('Are you sure you want to delete?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/plan/deletePlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
				var updatedata={'unit_id':unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/plan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('res',response.data);
						$scope.viewPlanData=response.data;
					},function errorCallback(response) {
						
						
					});
		
					
		
		
		},function errorCallback(response) {
			//alert(response.data);
			//$state.go('user.plan',{}, { reload: true });
		});
		}
	}
	$scope.clearPlanData=function(){
		
		$scope.user_readonly= false;
		$scope.showCancel =  false;
		$scope.buttonName ="Add";
		//$state.go('user.plan',{}, { reload: true });
	}
});


/*
function addNewUnitName(){
		
		var unit_name = document.getElementById("unit_name").value;
		alert("unit_name::"+unit_name);
		if(unit_name = "")
		{
			alert("Add Unit Name");
			return;
		}
		var session_id = document.getElementById("session_name").value;
		var course_id = document.getElementById("course_name").value;
		var semester_id = document.getElementById("semester").value;
		var subject_id = document.getElementById("subject_name").value;
		var section_id = document.getElementById("section").value;
		
		var dataString = "session_id="+session_id+"&course_id="+course_id+"&semester_id="+semester_id+"&subject_id="+subject_id+"&section_id="+section_id+"&unit_name="+unit_name;
	alert("Data:"+dataString);
		$.ajax({ 
		type: "POST",url: "php/plan/addUnitName.php" ,data: dataString,cache: false,
		success: function(html)
		{
			var dobj=jQuery.parseJSON(html);
			alert()
		} 
		});

}*/