var resoursecourse=angular.module('Channabasavashwara');
resoursecourse.controller('adminDeptManagementCourseController',function($scope,$http,$state,$window,courseField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	$scope.listOfCollege=[{
		name:'Select your college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response);
		$scope.courseData=response.data;
	},function errorCallback(response) {
	});
	$scope.addCourseData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select your college');
				courseField.borderColor('colg_name');
			}else if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('please select the stream');
				courseField.borderColor('stream_name');
			}else if($scope.course_name==null || $scope.course_name==''){
				alert('Course name field could not be blank');
				courseField.borderColor('resourcecoursename');
			}else if($scope.short_name==null || $scope.short_name==''){
				alert('Code field could not be blank');
				courseField.borderColor('resourceshortname');
			}else if($scope.semester==null || $scope.semester==''){
				alert('Please select the No of semester');
				courseField.borderColor('resourcesem');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value,'course_name':$scope.course_name,'short_name':$scope.short_name,'semester':$scope.semester};
				$http({
					method:'POST',
					url:"php/resource/addCourseData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.course_name=null;
					$scope.short_name=null;
					$scope.semester='';
					if($scope.courseData=='null'){
						$scope.courseData=[];
						$scope.courseData.push(response.data);
					}else{
						$scope.courseData.unshift(response.data);
					}
					//$state.go('dashboard.deptmanagement.course',{}, { reload: true });
					},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.course_name==response.data.course_name){
						courseField.borderColor('resourcecoursename');
					}else if($scope.short_name==response.data.short_name){
						courseField.borderColor('resourceshortname');
					}else{
					//$state.go('dashboard.deptmanagement.course',{}, { reload: true });
					}
				})
			}
		}
		if($scope.buttonName=="Update"){
			console.log('update',$scope.colg_name.value);
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('Please select your college');
				courseField.borderColor('colg_name');
			}else if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('please select the stream');
				courseField.borderColor('stream_name');
			}else if($scope.course_name==null || $scope.course_name==''){
				alert('Course name field could not be blank');
				courseField.borderColor('resourcecoursename');
			}else if($scope.short_name==null || $scope.short_name==''){
				alert('Code field could not be blank');
				courseField.borderColor('resourceshortname');
			}else if($scope.semester==null || $scope.semester==''){
				alert('Please select the No of semester');
				courseField.borderColor('resourcesem');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value,'course_name':$scope.course_name,'short_name':$scope.short_name,'semester':$scope.semester,'id':id};
				$http({
					method:'POST',
					url:"php/resource/updateCourseData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.deptmanagement.course',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.course_name==response.data.course_name){
						courseField.borderColor('resourcecoursename');
					}else if($scope.short_name==response.data.short_name){
						courseField.borderColor('resourceshortname');
					}else{
					$state.go('dashboard.deptmanagement.course',{}, { reload: true });
					}
				})
			}
		}
	}
	$scope.editRoleData=function(cid){
		id=cid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editCourseData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.getStreamValue(response.data[0].stream_id);
			$scope.course_name=response.data[0].course_name;
			$scope.short_name=response.data[0].short_name;
			$scope.semester=response.data[0].semester;
			$scope.readcolg=true;
			$scope.readstrm=true;
			$scope.colgdis=true;
			$scope.strmdis=true;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteRoleData=function(cid){
		id=cid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Course');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/resource/deleteCourseData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.deptmanagement.course',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				$state.go('dashboard.deptmanagement.course',{}, { reload: true });
			});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			courseField.clearBorderColor(id);
			$scope.listOfStream=null;
			$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
			var colgid={'colg_name':$scope.colg_name.value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
				});
			},function errorCallback(response) {
			});
		}
	}
	$scope.clearField=function(id){
		courseField.clearBorderColor(id);
	}
	$scope.activeclass='active';
	$scope.getDeptData=function(mod,id){
		if(mod.value!=''){
			courseField.clearBorderColor(id);
		}
	}
	$scope.CancelCourseData=function(){
		$state.go('dashboard.deptmanagement.course',{}, { reload: true });
	}
	$scope.getStreamValue=function(value){
		$scope.listOfStream=null;
			$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
			var colgid={'colg_name':$scope.colg_name.value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
					$scope.stream_name.value=value;
				});
			},function errorCallback(response) {
			});
	}
	$scope.clearsemBorder=function(id,mod){
		if(mod.value!=''){
			courseField.clearBorderColor(id);
		}
	}
	if($scope.course_name != '' || $scope.course_name !=null){
		courseField.clearBorderColor('resourcecoursename');
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resoursecourse.factory('courseField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});