var deptsub=angular.module('Channabasavashwara');
deptsub.controller('pricipalDeptSubjectController',function($http,$scope,$window,$state,subjectField){
	$scope.buttonName="Add";
	var id='';
	
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	
	
	$scope.listOfdept=[{
		name:'Select Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfdept[0];
	
	$scope.noCourse=null;
	$scope.noCourse=[{
			name:'select Course',
			value:''
	}]
	$scope.courseName=$scope.noCourse[0];
	
	
	$scope.noSemesters=null;
	$scope.noSemesters=[{
			name:'select Semester',
			value:''
	}]
	
	$scope.noSubjectType=[{
			name:'Select subject type',
			value:''
	}]
	$scope.subtype=$scope.noSubjectType[0];
	
	
	$http({
		method:'GET',
		url:"php/subject/readCollegeSubjectData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response);
		$scope.subjectData=response.data;
	},function errorCallback(response) {
	});
	
	
	
		
	$http({
		method: 'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	
	
	$http({
		method: 'GET',
		url:"php/college/getCollegeSubjectType.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
				var data={'name':obj.type,'value':obj.id};
				$scope.noSubjectType.push(data);
			});
		},function errorCallback(response) {
		
		});
	
	$scope.GetDeptData=function(id,mod){
		if(mod.value!=''){
			subjectField.clearBorderColor(id);
		}
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'stream_id':$scope.stream_name.value}
		$http({
			method:'POST',
			url:"php/department/readCollegeDeptData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
			});
		},function errorCallback(response) {
		});
		
		$scope.noCourse=null;
		$scope.noCourse=[{
			name:'select Course',
			value:''
		}]
		
		$scope.courseName=$scope.noCourse[0];
		var courseid={'stream_id':$scope.stream_name.value};
		$http({
			method:'POST',
			url:"php/course/readCollegePrincipalCourseData.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourse.push(data);
			});
		},function errorCallback(response) {
		});
		
	}
	
	
		$scope.semester=$scope.noSemesters[0];
	
		$scope.selectSemester=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('coursename');
		}
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select Semester',
			value:''
		}]
		
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':$scope.courseName.value};
		$http({
			method:'POST',
			url:"php/course/getCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				//alert("no_semester:"+no_semester);
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
				}
			});
		},function errorCallback(response) {
		});
	}
	
	$scope.selectCourse=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('colg_name');
		}
		
		
	}
	
	
	$scope.noSemesters=null;
	$scope.noSemesters=[{
			name:'select semester',
			value:''
	}]
	
	$scope.selectdept=function(mod){
		if(mod != ''){
			subjectField.clearBorderColor('dept_name');
		}
	}
	$scope.addSubjectData=function(){
			if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('Please select Stream..');
				subjectField.borderColor('stream_name');
				return;
			}else if($scope.dept_name.value=='' || $scope.dept_name.value==null){
				alert('Please select Department..');
				subjectField.borderColor('dept_name');
				return;
			}else if($scope.courseName.value=='' || $scope.courseName.value==null){
				alert('Please select Course..');
				subjectField.borderColor('coursename');
				return;
			}else if($scope.semester.value=='' || $scope.semester.value==null){
				alert('Please select Semester..');
				subjectField.borderColor('sem');
				return;
			}else if($scope.subtype.value=='' || $scope.subtype.value==null){
				alert('Please select subject type... ');
				subjectField.borderColor('sub_type');
				return;
			}else if($scope.subjectname=='' || $scope.subjectname==null){
				alert('Subject name field could not be blank...');
				subjectField.borderColor('subname');
				return;
			}else if($scope.subject_code=='' || $scope.subject_code==null){
				alert('Subject code field could not be blank..');
				subjectField.borderColor('subcode');
				return;
			}
		
		if($scope.buttonName=="Add"){
				
				var adddata={'stream_name':$scope.stream_name.value,'dept_name':$scope.dept_name.value,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code};
				$http({
					method:'POST',
					url:"php/subject/addCollegeSubjectData.php",
					data:adddata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					//$state.go('principal.dept.subject',{},{reload:true});
					$scope.subjectname=null;
					$scope.subject_code=null;
					if($scope.subjectData=='null'){
						$scope.subjectData=[];
						$scope.subjectData.push(response.data);
					}else{
						$scope.subjectData.unshift(response.data);
					}
				},function errorCallback(response) {
					//console.log('err',response);
					alert(response.data['msg']);
					if(response.data['type']== 1){
						subjectField.borderColor('subname');
					}else if(response.data['type']== 2){
						subjectField.borderColor('subcode');
					}else{
						//$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
				});
			
		}
		if($scope.buttonName=="Update"){
				var updatedata={'stream_name':$scope.stream_name.value,'dept_name':$scope.dept_name.value,'course_name':$scope.courseName.value,'semester':$scope.semester.value,'sub_type':$scope.subtype.value,'sub_name':$scope.subjectname,'sub_code':$scope.subject_code,'id':id};
				$http({
					method:'POST',
					url:"php/deptsubject/updateCollegeSubjectData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.dept.subject',{},{reload:true});
				},function errorCallback(response) {
					//console.log('err1',response);
					alert(response.data['msg']);
					if($scope.subjectname==response.data.subject_name){
						subjectField.borderColor('subname');
					}else if($scope.subject_code==response.data.short_name){
						subjectField.borderColor('subcode');
					}else{
					//$state.go('dashboard.deptmanagement.sub',{},{reload:true});
					}
				});
		}
		
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.getDept=function(){
		$scope.listOfSearchdept=null;
		$scope.listOfSearchdept=[];
		var colgid={'stream_id':$scope.streamSearch.value};
	$http({
		method:'POST',
		url:"php/department/readCollegeDeptData.php",
		data:colgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.dept_name,'value':obj.dept_id};
			$scope.listOfSearchdept.push(data);
		});
	},function errorCallback(response) {
	});
	}
	$scope.getCourse=function(){
		$scope.noCourseTable=null;
		$scope.noCourseTable=[];
		var colgid={'stream_id':$scope.streamSearch.value};
		$http({
			method:'POST',
			url:"php/course/readCollegePrincipalCourseData.php",
			data:colgid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				data={'name':obj.course_name,'value':obj.course_id};
				$scope.noCourseTable.push(data);
			});
		},function errorCallback(response) {
		});
	}
	
	
	$scope.editSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/subject/editDeptSubjectData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('colg_id',response.data[0].dept_id);
			$scope.stream_name.value=response.data[0].stream_id;
			$scope.selectEditDept(response.data[0].dept_id);
			$scope.selectEditCourse(response.data[0].course_name);
			$scope.selectEditSemester(response.data[0].semester,response.data[0].course_name);
			//$scope.semester.value=response.data[0].semester;
			$scope.selectEditSubject(response.data[0].subject_type);
			//$scope.subtype.value=response.data[0].subject_type;
			$scope.subjectname=response.data[0].subject_name;
			$scope.subject_code=response.data[0].short_name;
			$scope.readcolg=true;
			$scope.readstrm=true;
			$scope.readdept=true;
			$scope.readcors=true;
			$scope.readsem=true;
			$scope.readsub=true;
			$scope.strmdis=true;
			$scope.deptdis=true;
			$scope.corsdis=true;
			$scope.semdis=true;
			$scope.subdis=true;
			$scope.buttonName="Update";
			$scope.clearButtonName="Cancel";
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	
	$scope.selectEditDept=function(value){
		//alert("value:"+value);
		console.log('value',value);
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		$http({
		method:'GET',
		url:"php/department/getCollegeDeptData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response);
		angular.forEach(response.data,function(obj){
			var data={'name':obj.dept_name,'value':obj.dept_id};
			$scope.listOfdept.push(data);
		});
		$scope.dept_name.value=value;
	},function errorCallback(response) {
	});
	
	}
	
	
	$scope.selectEditCourse=function(course_id){
		//alert("Courcse:"+course_id);
		$scope.noCourse=null;
		$scope.noCourse=[{
			name:'select Course',
			value:''
		}]
		$scope.courseName=$scope.noCourse[0];
		//console.log('edit course',courseid,value);
		
		$http({
		method:'GET',
		url:"php/course/getCollegeCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		//console.log('res',response);
		angular.forEach(response.data,function(obj){
			//alert(":::"+obj.course_name);
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.noCourse.push(data);
			
		});
		$scope.courseName.value = course_id;
		},function errorCallback(response) {
		});
		
	}
	
	
	$scope.selectEditSemester=function(value,course_id){
		$scope.noSemesters=null;
		$scope.noSemesters=[{
			name:'select semester',
			value:''
		}]
		$scope.semester=$scope.noSemesters[0];
		var semid={'course_id':course_id};
		console.log('semester',semid);
		$http({
			method:'POST',
			url:"php/course/getCollegeSemester.php",
			data:semid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data, function(obj){
				var no_semester = obj.semester;
				var key = ['I','II','III','IV','V','VI','VII','VIII','IX','X'];
				for(var i = 0; i < no_semester; i++){
					var val = i+1;
					var roman_val = key[i];
					var seme = {'name':roman_val , 'value':val};
					$scope.noSemesters.push(seme);
					
				}
			});
			$scope.semester.value=value;
		},function errorCallback(response) {
		});
	}
	$scope.selectEditSubject=function(value){
	$scope.noSubjectType=null;
		$scope.noSubjectType=[{
			name:'Select subject type',
			value:''
		}]
		$scope.subtype=$scope.noSubjectType[0];
		//console.log('get dept',courseid,value)
		$http({
			method:'GET',
			url:"php/deptsubject/getCollegeSubjectType.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.type,'value':obj.id};
				$scope.noSubjectType.push(data);
				$scope.subtype.value=value;
			});
		},function errorCallback(response) {
		});
	}
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		var msg=$window.confirm('Are you  sure to delete this subject ?');
		if(msg){
		$http({
			method:'POST',
			url:"php/deptsubject/deleteDeptSubject.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('principal.dept.subject',{},{reload:true});
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('principal.dept.subject',{},{reload:true});
		});
		}
	}
	$scope.clearSubjectData=function(){
		$state.go('principal.dept.subject',{},{reload:true});
	}
	$scope.selectSubjectType=function(mod,id){
		subjectField.clearBorderColor(id);
	}
	$scope.removeBorder=function(id){
		subjectField.clearBorderColor(id);
	}
	
	

		$scope.listOfSearchStream=null;
		$scope.listOfSearchStream=[];
		$http({
			method:'GET',
			url:"php/stream/readCollegeStreamData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.stream_name,'value':obj.stream_id};
				$scope.listOfSearchStream.push(data);
			});
		},function errorCallback(response) {
		});
	$scope.selectEditStream=function(value,streamid){
		$scope.listOfStream=null;
		$scope.listOfStream=[{
		name:'Select stream',
		value:''
	    }]
		var colgid={'colg_name':value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
					$scope.stream_name.value=streamid;
				});
			},function errorCallback(response) {
			});
	    $scope.stream_name=$scope.listOfStream[0];
	}
});
deptsub.factory('subjectField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});