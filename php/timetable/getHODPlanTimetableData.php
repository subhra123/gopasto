<?php
require_once '../../include/dbconfig.php'; 
$output = array();

$clg_id = $_SESSION["admin_clg_id"];
$stream_id= $_POST['stream_id'];
$dept_id= $_SESSION["admin_dept_id"];
$course_id= $_POST['course_id'];
$semester_id= $_POST['semester_id'];;
$session_id= $_POST['session_id'];;
$section_id= $_POST['section_id'];;

/////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////
//$clg_id = 20;
//$stream_id= 47;
//$dept_id= 40;
//$course_id= 54;
//$semester_id= 1;
//$session_id= 10;
//$section_id= 15;

//////////////////////////////////////////////////////////////////////////////////////////////////////////


function getSubjectData($connect,$table_id,$day_id,$time_id){
		$record = array();
		$db_result = mysqli_query($connect, "select *  from db_time_table where table_id='".$table_id."'  and day_id = '".$day_id."' and time_id = '".$time_id."'  ");
		while ($row =mysqli_fetch_assoc($db_result)) {
  		$record[] = $row;
		}
		return $record;	
	}
	
function getSubjectName($connect ,$subject_id){
		$name = "";
		$db_result = mysqli_query($connect, "select *  from db_subject where subject_id='".$subject_id."'");
		$db_result = mysqli_query($connect, "select s.short_name, p.type from db_subject s INNER JOIN db_subject_type p ON s.subject_type=p.id  where s.subject_id='".$subject_id."'  ");
		
		$row = mysqli_fetch_assoc($db_result);
  		$name = $row['type'].' : '.$row['short_name'];
		return $name;	
	}	

function getFacultyName($connect,$user_id){
		$name = "";
		$db_result = mysqli_query($connect, "select s.first_name, s.last_name, p.role  from db_user s INNER JOIN db_role p ON s.role_id= p.role_id where s.user_id ='".$user_id."' ");
		while ($row =mysqli_fetch_assoc($db_result)) {
  		$name = $row['role'].":".$row['last_name']." ".$row['first_name'];
		}
		return $name;	
	}
	
function getVenueName($connect,$venue_id){
		$name = "";
		$db_result = mysqli_query($connect, "select *  from db_venue where venue_id='".$venue_id."'  ");
		while ($row =mysqli_fetch_assoc($db_result)) {
  		$name = $row['venue_name'];
		}
		return $name;	
	}	

//////////////////////////////////////////////////////////////////////////////////////////////////////////

	$data="";
	$datahidden="";
	$day_count = "";
	$start_day="";
	$time_count="";
	
	$clg_head_array = array();
	$db_result = mysqli_query($connect, "select a.user_id , a.first_name , a.last_name , b.role  from db_user a INNER JOIN db_role b ON a.role_id = b.role_id WHERE b.type='1' and a.colg_id = '".$clg_id ."' ");
	if($row =mysqli_fetch_assoc($db_result)) {
  		$clg_head_array[] = $row;
	}
	
	
	$db_result = mysqli_query($connect, "select *  from db_class_day where clg_id='".$clg_id."' ");
	if($row =mysqli_fetch_assoc($db_result)) {
  		$day_count = $row['days'];
		$start_day= $row['first_day'];
		$time_count= $row['periods'];
	}
	
	$day_array = array();
	if($start_day == "Monday" )
	{
		$db_result = mysqli_query($connect, "select *  from sys_day_type2 ORDER BY id asc");
		while($row = mysqli_fetch_assoc($db_result)){ 
  			$day_array[] = $row;
		}
	}
	else
	{
		$db_result = mysqli_query($connect, "select *  from sys_day_type1 ORDER BY id asc");
		while($row = mysqli_fetch_assoc($db_result)){ 
  			$day_array[] = $row;
		}
	}
	
	
	$time_array = array();
	$db_result = mysqli_query($connect, "select *  from db_class_time where clg_id='".$clg_id."' ORDER BY period_id asc ");
	while ($row =mysqli_fetch_assoc($db_result)) {
  		$time_array[] = $row;
	}
	
	
	

	
	$all_subject_array = array();
	$all_subject_array[0]['subject_id'] = "-1";
	$all_subject_array[0]['type'] = "NONE";
	$all_subject_array[0]['short_name'] = "NO CLASS";
	
	$db_result = mysqli_query($connect, "select s.subject_type, s.subject_id, s.short_name, p.type , p.period  from db_subject s INNER JOIN db_subject_type p ON s.subject_type=p.id  where s.dept_id='".$dept_id."'  and s.semester = '".$semester_id."' and s.course_name = '".$course_id."' ORDER BY s.subject_name asc ");

	while ($row =mysqli_fetch_assoc($db_result)) {
  		$all_subject_array[] = $row;
	}
	
	
		
	$theory_array = array();
	$theory_array[0]['subject_id'] = "-1";
	$theory_array[0]['type'] = "NONE";
	$theory_array[0]['short_name'] = "NO CLASS";
	
	$db_result = mysqli_query($connect, "select s.subject_type, s.subject_id, s.short_name, p.type  from db_subject s INNER JOIN db_subject_type p ON s.subject_type=p.id  where s.dept_id='".$dept_id."'  and s.semester = '".$semester_id."' and s.course_name = '".$course_id."' and p.period = '1' ORDER BY s.subject_name asc ");
	while ($row =mysqli_fetch_assoc($db_result)) {
  		$theory_array[] = $row;
	}
	
	
	$faculty_array = array();
	$faculty_array[0]['user_id'] = $clg_head_array[0]['user_id'];
	$faculty_array[0]['first_name'] = $clg_head_array[0]['first_name'];
	$faculty_array[0]['last_name'] = $clg_head_array[0]['last_name'];
	$faculty_array[0]['role'] = $clg_head_array[0]['role'];
	
	$db_result = mysqli_query($connect, "select s.user_id, s.first_name, s.last_name, p.role  from db_user s INNER JOIN db_role p ON s.role_id= p.role_id where s.dept_id='".$dept_id."'  and s.role_id > '0' ORDER BY s.user_id asc ");
	while ($row =mysqli_fetch_assoc($db_result)) {
  		$faculty_array[] = $row;
	}
	
	$type_array = array();
	$db_result = mysqli_query($connect, "select *  from db_subject_type ORDER BY id asc ");
	while ($row =mysqli_fetch_assoc($db_result)) {
  		$type_array[] = $row;
	}
	
	
	$all_venue_array = array();
	$db_result = mysqli_query($connect, "select *  from db_venue where clg_id='".$clg_id."'  ORDER BY venue_id asc ");
	while ($row =mysqli_fetch_assoc($db_result)) {
  		$all_venue_array[] = $row;
	}
		
	
	$all_subject_count = count($all_subject_array);
	$theory_count = count($theory_array);
	$faculty_count = count($faculty_array);
	$all_venue_count = count($all_venue_array);
	


	$data .='<tr>
    		 <td width="100" align="center">Time <i class="fa fa-long-arrow-right"></i>
      		 <BR>Day <i class="fa fa-long-arrow-down"></i>
    		 </td>';
	 for($j= 0 ; $j < $time_count ; $j++ ) 
	 {
		$data .='<td width="100" align="center">'.$time_array[$j]['time_slot'].'</td>';
	 }
	 $data .='</tr>';	

$result = mysqli_query($connect, "select *  from db_time_table_info where clg_id='".$clg_id."' and dept_id='".$dept_id."' and session_id='".$session_id."' and course_id='".$course_id."' and semester_id='".$semester_id."' and section_id='".$section_id."'  ");
	
$Num = mysqli_num_rows($result);

if(!$Num)
{
	$qry ='INSERT INTO db_time_table_info (clg_id,dept_id,session_id,course_id,semester_id,section_id) values ("'.$clg_id.'","'.$dept_id.'","'. $session_id.'","'.$course_id.'","'.$semester_id.'","'.$section_id.'")';
$qry_res = mysqli_query($connect,$qry);

	$result = mysqli_query($connect, "select *  from db_time_table_info where clg_id='".$clg_id."' and dept_id='".$dept_id."' and session_id='".$session_id."' and course_id='".$course_id."' and semester_id='".$semester_id."' and section_id='".$section_id."'  ");
	
	$Num = mysqli_num_rows($result);
}

if($Num > 0)
{
	$row =mysqli_fetch_assoc($result);
	$table_id = $row['id'];
	$status = $row['status'];
	$hod_status = $row['hod_status'];
	$hod_comment = $row['hod_comment'];
	$principal_status = $row['principal_status'];
	$principal_comment = $row['principal_comment'];
	
	$output["status"] = $status;
	$output["hod_status"] = $hod_status;
	$output["hod_comment"] = $hod_comment;
	$output["principal_status"] = $principal_status;
	$output["principal_comment"] = $principal_comment;
	$output["table_id"] = $table_id;
	$lab_day = 0;
	$lab_time = 0;
	$lab_period = 0;
	$lab_subject_name = "";
	$lab_faculty_name = "";
	$lab_venue_name = "";
	
	for($j= 0 ; $j < $day_count ; $j++ ) 
	 {
		$data .='<tr>
               <td width="100" align="center" style=" vertical-align:middle">'.$day_array[$j]['day'].'</td>';
			   
		for($i= 0 ; $i < $time_count ; $i++ ) 
		{
			if($i == 0 || $i == 4 )
			{
				$subject_array = $all_subject_array;
				$subject_count = $all_subject_count;
				$venue_array = $all_venue_array;
				$venue_count = $all_venue_count;
			}
			else
			{
				$subject_array = $theory_array;
				$subject_count = $theory_count;
				$venue_array = $all_venue_array;
				$venue_count = $all_venue_count;
			}
			
			///////////////////////////////Get Subject From DB///////////////
			$db_record = getSubjectData($connect,$table_id,$day_array[$j]['id'],$time_array[$i]['period_id']);		 
			//echo $vari = $table_id."::".$day_array[$j]['id']."::".$time_array[$i]['id'];
			///////////////////////////////
			 $color_code1="#FFE0D9";
			 $color_code2="#FFE0D9";
			 $color_code3="#FFE0D9";
			 if(count($db_record) > 0 && $db_record[0]['subject_id'] != 0 )
			 	$color_code1 = "#CCF2CC";
			
			 if(count($db_record) > 0 && $db_record[0]['faculty_id'] > 0 )
			 	$color_code2 = "#CCF2CC";
				
			 if(count($db_record) > 0 && $db_record[0]['venue_id'] > 0 )
			 	$color_code3 = "#CCF2CC";
			
			//echo "::::".$db_record[0]['subject_type']."<br>";
			
			if($db_record[0]['subject_type'] > 1)
			{
				$lab_day =  $day_array[$j]['id'];
				$lab_time =  $time_array[$i]['period_id'];
				$lab_period = $db_record[0]['subject_type'];
				$lab_subject_name = getSubjectName($connect,$db_record[0]['subject_id']);
				$lab_faculty_name = getFacultyName($connect,$db_record[0]['faculty_id']);
				$lab_venue_name = getVenueName($connect,$db_record[0]['venue_id']);
			}
			
			
			if($lab_day == $day_array[$j]['id'] && $lab_time != $time_array[$i]['period_id'] && $time_array[$i]['period_id'] < $lab_time+$lab_period )
			{
				$subject_select_display = "none";
				$faculty_select_display = "none";
				$venue_select_display = "none";
				$subject_lab_display = "block";
				$faculty_lab_display = "block";
				$venue_lab_display = "block";
			}
			else
			{
				$subject_select_display = "block";
				$faculty_select_display = "block";
				$venue_select_display = "block";
				$subject_lab_display = "none";
				$faculty_lab_display = "none";
				$venue_lab_display = "none";
			}
			
			//for NO CLASS
			if($db_record[0]['subject_id'] == -1 ) // for no class
			{
				$faculty_select_display = "none";
				$venue_select_display = "none";
				$faculty_lab_display = "block";
				$venue_lab_display = "block";
			}
			else
			{
				$faculty_select_display = "block";
				$venue_select_display = "block";
				$faculty_lab_display = "none";
				$venue_lab_display = "none";
			}
			
			$data .='<td width="100" align="center" style="padding:0px;" >
                     <table style="margin:0px; padding:0px; width:100%">
               	 
				 <tr><td >
                 <select $readOnly class="form-control oditek-form" onChange="adminCheckSubjectRecord('.$day_array[$j]['id'].' ,'.$time_array[$i]['period_id'].','.$table_id.');" id="subject_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" style="background-color:'.$color_code1.'; display:'.$subject_select_display.'">
					 <option value="0">Select Subject/Code</option>';
					for($k= 0 ; $k < $subject_count ; $k++ )
					{
						 $selected="";
						 if(count($db_record) > 0 && $db_record[0]['subject_id'] == $subject_array[$k]['subject_id'] )
						 {
						 	$selected = "selected";
							$selected_period = $subject_array[$k]['period'];
						 }
						 $data .='<option title="'.$subject_array[$k]['subject_name'].'"  '.$selected.'   value='.$subject_array[$k]['subject_id'].'  style="background-color:#E0F7FF;"   >'.$subject_array[$k]['type']." : ".$subject_array[$k]['short_name'].'</option>';
						 $datahidden .='<input type="hidden"  value="'.$subject_array[$k]['period'].'"  id="subjecttype_'.$subject_array[$k]['subject_id'].'" >';
						 //}
					}
					
			$data .='</select>';
			$data .='<div class="oditek-td" style="display:'.$subject_lab_display.'" id="lab_subject_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" >'.$lab_subject_name.'</div>';
			
            $data .='</td></tr>
	                 <tr><td>
                     <select class="form-control oditek-form" onChange="adminCheckFacultyRecord('.$session_id.','.$day_array[$j]['id'].' ,'.$time_array[$i]['period_id'].','.$table_id.');" id="faculty_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" style="background-color:'.$color_code2.';display:'.$faculty_select_display.'">
					 <option value="0">Select Faculty</option>';
					for($k= 0 ; $k < $faculty_count ; $k++ )
					{
						 $selected="";
						 if(count($db_record) > 0 && $db_record[0]['faculty_id'] == $faculty_array[$k]['user_id'] )
						 	$selected = "selected";
							
						 $data .='<option  value='.$faculty_array[$k]['user_id'].' '.$selected.' style="background-color:#E0F7FF;">'.$faculty_array[$k]['role'].':'.$faculty_array[$k]['last_name'].' '.$faculty_array[$k]['first_name'].'</option>';
					}
			$data .='</select>
					 <div class="oditek-td" style="display:'.$faculty_lab_display.'" id="lab_faculty_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" >'.$lab_faculty_name.'</div>
                     </td>
                     </tr>
					
					<tr><td >
                 	<select class="form-control oditek-form" onChange="adminCheckVenueRecord('.$session_id.','.$day_array[$j]['id'].' ,'.$time_array[$i]['period_id'].','.$table_id.');" id="venue_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" style="background-color:'.$color_code3.';display:'.$venue_select_display.'">
					 <option value="0">Select Venue</option>';
			
			
			for($k= 0 ; $k < $venue_count ; $k++ )
			{
				 $selected="";
				 if(count($db_record) > 0 && $db_record[0]['venue_id'] == $venue_array[$k]['venue_id'] )
				 	$selected = "selected";
				
				
				$data .='<option  value='.$venue_array[$k]['venue_id'].' '.$selected.' style="background-color:#E0F7FF;">'.$venue_array[$k]['venue_name'].'</option>';
			}
			
			
			$data .='</select>
			 			<div class="oditek-td" style="display:'.$venue_lab_display.'" id="lab_venue_'.$day_array[$j]['id'].''.$time_array[$i]['period_id'].'" >'.$lab_venue_name.'</div>
                     </td></tr>
		             
					 </td>
                     </table>
                     </td>';
		}
		$data .='</tr>';  
    }
	 $output["data"] = $data;
	 $output["data_hide"] = $datahidden;
	
	//fetch from db
}



	
	
print json_encode($output);

?>

