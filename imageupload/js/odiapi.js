var jcrop_api;
var ratio = 1;
var load = 0;
var inprogress = 0;




function updateCoords(e) {
    //alert("updateCoords");
    $("#x").val(e.x);
    $("#y").val(e.y);
    $("#w").val(e.w);
    $("#h").val(e.h)
}

function imageload() 
{
    $("#flimg").change(function () {
								 
        var e = new FileReader;
        var t = document.getElementById("flimg").files[0];
        if (t != null && !fltypefilter.test(t.type)) {
            document.getElementById("flimg").value = "";
            alert("Unsupported file type! Please select image file only.");
            return
        }
		
        if (t == null) {
            loadDefaultImage()
        } else {
            e.readAsDataURL(t);
            e.onload = function (e) {
                var t = e.target.result;
                var n, r;
                updateNewImage(t, 1)
            }
        }
    })
}

function beforeSubmit() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        
		
		var e = document.getElementById("disboxid2");
        var t = $("#flimg")[0].files[0].size;
        var n = $("#flimg")[0].files[0].type;
        switch (n) {
        case "image/png":
        case "image/gif":
        case "image/jpeg":
        case "image/pjpeg":
            break;
        default:
            alert("Unsupported file type! Please select image file only.");
            return false
        }
		
        if (e.style.width == "0px" && e.style.height == "0px") {
            alert("Please select crop area then press submit.");
            return false
        }
        if (t > 10485760) {
            alert(" Too big Image file! Please reduce the size of your photo using an image editor!");
            return false
        }
		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';
		progressbox.show(); //show progressbar
		progressbar.width(completed); //initial value 0% of progressbar
		statustxt.html(completed); //set status text
		statustxt.css('color','#000'); //initial color of status text
		//alert("before done");
		inprogress = 1;

    } else {
        return false
    }
}

function resetProgressBar(){
	document.getElementById("progressbox").style.display="none";
}

function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	var statustxt1       = $('#statustxt');
	var progressbar1     = $('#progressbar');
	progressbar1.width(percentComplete + '%') //update progressbar percent complete
	statustxt1.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
	{
		statustxt1.css('color','#fff'); //change status text to white after 50%
	}
}

function afterSuccess() {
    var e = document.getElementById("output").innerHTML;
    var t = document.getElementById("imageid").value;
    document.getElementById(t).src = e;
	inprogress = 0;
    closepop()
}


function updateNewImage(e, t) {
	
    var n = new Image;
    n.src = e;
    var r;
    var i;
    var s = $("#cropbox");
    var o = $("#cropbox2");
    var u = document.getElementById("cropbox");
    var a = document.getElementById("cropbox2");
    var f = document.getElementById("holderid1");
    var l = document.getElementById("trackid1");
    var c = document.getElementById("disboxid1");
    var h = document.getElementById("disboxid2");
    var p = document.getElementById("aw");
    var d = document.getElementById("ah");
    var v = document.getElementById("nw");
    var m = document.getElementById("nh");
    var g = document.getElementById("tw").value;
    var y = document.getElementById("th").value;
		
    p.value = n.width;
    d.value = n.height;
    if (t == 1 && (n.width < g || n.height < y)) {
        var b = "Sorry! Selected Image size is less than Required Image size. Select Image: (" + n.width + " x " + n.height + ") Required Image size: (" + g + " x " + y + ")";
        alert(b);
        document.getElementById("flimg").value = "";
        return
    }
	
	
    if (n.width > n.height) {
        r = 300;
        i = r * n.width / n.height;
        i = Math.ceil(i);
        if (i > 1e3) {
            i = 600;
            r = i * n.height / n.width;
            r = Math.ceil(r)
        }
    } else if (n.width < n.height) {
        i = 300;
        r = i * n.height / n.width;
        r = Math.ceil(r);
        if (r > 500) {
            r = 500;
            i = r * n.width / n.height;
            i = Math.ceil(i)
        }
    } else {
        i = 300;
        r = 300
    }
    v.value = i;
    m.value = r;
    jcrop_api.setNewImage(i, r);
    u.style.width = i + "px";
    a.style.width = i + "px";
    u.style.height = r + "px";
    a.style.height = r + "px";
    s.attr("src", e);
    o.attr("src", e);
    f.style.width = i + "px";
    f.style.height = r + "px";
    l.style.width = i + "px";
    l.style.height = r + "px";
    c.style.display = "none";
    if (t == 1) {
        document.getElementById("imginfoid2").innerHTML = "Current Image Size: " + n.width + "px X " + n.height + "px";
        jcrop_api.enable();
        h.style.width = 0;
        h.style.height = 0;
        h.style.left = 0;
        h.style.top = 0;
        u.style.opacity = .6
    } else {
        document.getElementById("imginfoid2").innerHTML = "";
        jcrop_api.disable();
        h.style.width = 0;
        h.style.height = 0;
        h.style.left = 0;
        h.style.top = 0;
        u.style.opacity = 1
    }
  resizeFrame()
}
function resizeFrame() {
    imagepop()
}
function loadDefaultImage() {
    var e = "imageupload/images/noimage.png";
    updateNewImage(e, 0);
    document.getElementById("flimg").value = ""
}
function setImageRatio() {
    var e = document.getElementById("tw").value;
    var t = document.getElementById("th").value;
    var n = e / t;
    jcrop_api.setRatio(n)
}

function removeImageRatio() {
    jcrop_api.setRatio(0)
}
function checkCoords() {
	if(inprogress){
		alert("File upload in progress...");
		return false;
	}
    var e = document.getElementById("aw").value;
    var t = document.getElementById("disboxid2");
    var n = document.getElementById("flimg");
    if (!n.value) {
        alert("Please select a image...");
        return false
    }
    if (!e) {
        alert("Please select a image then press upload.");
        return false
    }
    updateBoxSize();
    return true
}
function changeChk(){
	var hidcheckid = document.getElementById('checkboxid');
	if(document.getElementById('chkid').checked)
	{
		setImageRatio();
		hidcheckid.value=1;
	}
	else
	{
		hidcheckid.value=0;
		removeImageRatio();	
	}
	
	}
function updateBoxSize() {
    var e = document.getElementById("disboxid2");
    $("#x").val(e.style.left);
    $("#y").val(e.style.top);
    $("#w").val(e.style.width);
    $("#h").val(e.style.height)
}

function checkFileUpload(){
	if(inprogress)
	{
		alert("File upload in progress...");
		return false;
	}
	return true;
}



$(function () {
    $("#cropbox").Jcrop({
        aspectRatio: 1,
        onSelect: updateCoords
    })
});
fltypefilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;






$(window).load(function () {
	//alert("load function");
    imageload();
    var e = {
        target: "#output",
        beforeSubmit: beforeSubmit,
		uploadProgress: OnProgress,
        success: afterSuccess,
        resetForm: true
    };
	
    $("#frmcategory").submit(function () {
        $(this).ajaxSubmit(e);
        return false
    })
});




jQuery(function (e) {
	//load = 1;
	//alert("jQuery Load::");
    function n() {
        e("#cropbox").Jcrop({
            onRelease: r
        }, function () {
            jcrop_api = this;
            jcrop_api.disable();
        })
    }
    function r() {}
	n();
	//var t = document.getElementById("holderid2");
    //t.parentNode.removeChild(t);
});

