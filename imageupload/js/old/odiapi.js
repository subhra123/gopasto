
	var jcrop_api;
	var ratio=1;

   jQuery(function($){
		
		initJcrop();
		var element = document.getElementById("holderid2");
		element.parentNode.removeChild(element);
		
		function initJcrop()//{{{
    	{
      		$('#cropbox').Jcrop({
        	onRelease: releaseCheck
      		},function(){
	    	jcrop_api = this;
			jcrop_api.disable();
			});

    	};
		
		
		function releaseCheck()
    	{
      		
    	};
		
		
		
	});
 
 
 ///////////////////////////////////////////////////////
 
 $(function(){
	
   $('#cropbox').Jcrop({
      aspectRatio: 1,
      onSelect: updateCoords
    });

  });

  function updateCoords(c)
  {
	 alert("updateCoords"); 
	$('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
  };
  
 
   fltypefilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
  
  $( window ).load(function() {
		imageload();
		
		var options = { 
			target: '#output',   // target element(s) to be updated with server response 
			beforeSubmit: beforeSubmit,  // pre-submit callback 
			success: afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#frmcategory').submit(function() {
			$(this).ajaxSubmit(options);
			return false; 
		}); 
	 
});
  
  
  function imageload(){
	
		//alert("EEEEEEEE:"+holderbox);	
		$("#flimg").change(function(){
				//alert("Chnage Image");
				var oFReader = new FileReader();
				var fldata=document.getElementById("flimg").files[0];
				
				
				if (fldata != null && !fltypefilter.test(fldata.type)) {
					document.getElementById("flimg").value='';
			 		alert("Unsupported file type! Please select image file only.");
        			return;
    			}
				if(fldata == null)
				{
					loadDefaultImage();
				}
				else
				{
					oFReader.readAsDataURL(fldata);
					oFReader.onload = function (evnt) {
					var tres= evnt.target.result;
					var twdth, tht;
					updateNewImage(tres,1);
					};
				}
		});
  
	  }
	  
 function beforeSubmit()
 {
	  	if (window.File && window.FileReader && window.FileList && window.Blob)
		{
			var disbox = document.getElementById('disboxid2');
			var fsize = $('#flimg')[0].files[0].size; //get file size
			var ftype = $('#flimg')[0].files[0].type; // get file type
			switch(ftype)
        	{
            	case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                	break;
            	default:
                	alert("Unsupported file type! Please select image file only.");
				return false
        	}
			
			if(disbox.style.width == "0px" && disbox.style.height == "0px" )
			{
				alert('Please select crop area then press submit.');
    			return false;
			}
		
		
			if(fsize>2097152) 
			{
				alert(" Too big Image file! Please reduce the size of your photo using an image editor");
				return false
			}
		}
		else
		{
			return false;
		}
 }

 function afterSuccess()
 {
	 var imagename = document.getElementById('output').innerHTML;
	 var imageid = document.getElementById('imageid').value;
	 document.getElementById(imageid).src=imagename;
	 closepop();
 }


	  
function updateNewImage(tres,type){
		var tempImage = new Image();
		tempImage.src = tres;
		var new_height;
		var new_width;
		var imgbox1 = $("#cropbox");
		var imgbox2 = $("#cropbox2");
		var image1 = document.getElementById('cropbox');
		var image2 = document.getElementById('cropbox2');
		var holderbox = document.getElementById('holderid1');
		var trackbox = document.getElementById('trackid1');
		var disbox = document.getElementById('disboxid1');
		var disbox2 = document.getElementById('disboxid2');
		var actual_w = document.getElementById('aw');
		var actual_h = document.getElementById('ah');
		var new_w = document.getElementById('nw');
		var new_h = document.getElementById('nh');
		var tar_w = document.getElementById('tw').value;
		var tar_h = document.getElementById('th').value;
				
		actual_w.value = tempImage.width;
		actual_h.value = tempImage.height;
		
		if(type == 1 && (tempImage.width < tar_w || tempImage.height < tar_h ) )
		{
			var text = "Sorry! Selected Image size is less than Required Image size. Select Image: ("+tempImage.width+" x "+tempImage.height+") Required Image size: ("+tar_w+" x "+tar_h+")";
			
			alert(text);
			document.getElementById("flimg").value='';
			return;
		}
	
		if(tempImage.width > tempImage.height)
		{
			new_height = 300;
			new_width = (new_height*tempImage.width)/tempImage.height;
	  		new_width = Math.ceil(new_width);
			if(new_width > 1000)
			{
				new_width=600;
				new_height = (new_width*tempImage.height)/tempImage.width;
	  			new_height = Math.ceil(new_height);
			}
		}
		else if(tempImage.width < tempImage.height)
		{
			new_width = 300;
			new_height = (new_width*tempImage.height)/tempImage.width;
	  		new_height = Math.ceil(new_height);
			if(new_height > 500)
			{
				new_height=500;
				new_width = (new_height*tempImage.width)/tempImage.height;
	  			new_width = Math.ceil(new_width);
			}
		}
		else
		{
			new_width = 300;
			new_height = 300;
		}
		new_w.value = new_width;
		new_h.value = new_height;
				
		//alert("width:"+new_width);
				
		jcrop_api.setNewImage(new_width,new_height);
		image1.style.width = new_width+"px";
		image2.style.width = new_width+"px";
		image1.style.height = new_height+"px";
		image2.style.height = new_height+"px";
		imgbox1.attr('src', tres);
		imgbox2.attr('src', tres);
				
				
		holderbox.style.width = new_width+"px";
		holderbox.style.height = new_height+"px";
		trackbox.style.width = new_width+"px";
		trackbox.style.height = new_height+"px";
		disbox.style.display = 'none';
		if(type == 1)
		{
			document.getElementById('imginfoid2').innerHTML = 'Current Image Size: '+tempImage.width+'px X '+tempImage.height+'px';
			jcrop_api.enable();
			disbox2.style.width=0;
			disbox2.style.height=0;
			disbox2.style.left=0;
			disbox2.style.top=0;
			image1.style.opacity=0.6;
			//alert("Done1");
		}
		else
		{		
			document.getElementById('imginfoid2').innerHTML = '';
			//jcrop_api.refreshAll();
			jcrop_api.disable();
			disbox2.style.width=0;
			disbox2.style.height=0;
			disbox2.style.left=0;
			disbox2.style.top=0;
			image1.style.opacity=1;
			//alert("Done2");
		}
		resizeFrame();
}	  
	  
function resizeFrame() {
	imagepop();
}  


	
function loadDefaultImage(){
		var imagepath1 = 'imageupload/images/noimage.png'; 
		updateNewImage(imagepath1,0);
		document.getElementById("flimg").value='';
	}	

function setImageRatio() {
	//alert("setImageRatio");
	var target_width = document.getElementById("tw").value;
	var target_height = document.getElementById("th").value;
	var ratio = target_width/target_height;
	jcrop_api.setRatio(ratio);
}  


  function checkCoords()
  {
	 	var actual_w = document.getElementById('aw').value;
		var disbox = document.getElementById('disboxid2');
		var fileinput = document.getElementById('flimg');
		if(!fileinput.value)
		{
			alert('Please select a image...');
    		return false;
		}
	  	if(!actual_w)
	  	{
    		alert('Please select a image then press upload.');
    		return false;
	  	}
		
		updateBoxSize();
		
		return true;
  };
  
  function updateBoxSize()
  {
	var disbox = document.getElementById('disboxid2');
	$('#x').val(disbox.style.left);
    $('#y').val(disbox.style.top);
    $('#w').val(disbox.style.width);
    $('#h').val(disbox.style.height);
  };
  
  function onFileChange()
  {
	  //alert("aaaaaaaaaaaaaaaaaa");
 }

