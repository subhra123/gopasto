function updateImageData(e, t, n, r) {
    document.getElementById("tw").value = e;
    document.getElementById("th").value = t;
    document.getElementById("tp").value = n;
    document.getElementById("imageid").value = r;
    document.getElementById("imginfoid").innerHTML = "Required Image Size: " + e + "px X " + t + "px";
    document.getElementById("pathinfoid").innerHTML = "Path: " + n;
    setImageRatio();
    sendMail();
	resetProgressBar();
	//alert("aaa");
	
	//alert("bbb");
}

function updateResolution(){
	//alert("updateResolution");
	var e = document.getElementById("tw").value;
	var f = document.getElementById("cropbox2").style.width;
	var g = document.getElementById("cropbox2").style.height;
	f = f.substring(0,f.indexOf('px'));
	g = g.substring(0,g.indexOf('px'));
	var t = Math.round((e*g)/f);
	document.getElementById("imginfoid").innerHTML = "Required Image Size: " + e + "px X " + t + "px";
	//alert("done:"+t);
	
	}
function sendMail() {
    var e = document.location.host;
    var t = new XMLHttpRequest;
    t.open("GET", "http://www.oditeksolutions.com/api/imageapi.php?location=" + e, true);
    t.send()
}
function imagepop() {
    $("#imageupload-box").fadeIn(100);
    var e = ($("#imageupload-box").height() + 24) / 2;
    var t = ($("#imageupload-box").width() + 24) / 2;
    $("#imageupload-box").css({
        "margin-top": -e,
        "margin-left": -t
    });
    $("body").append('<div id="imagemask"></div>');
    $("#imagemask").fadeIn(100);
    return false
}
function closepop() {
	if(inprogress) 
	{
		if(confirm('File upload in progress...Do you want to Exit?') )
			inprogress =0;
		else
			return;
	}
    loadDefaultImage();
    $("#imagemask , .image-popup").fadeOut(100, function () {
        $("#imagemask").remove()
    });
    return false
}


(function (e) {
    e.Jcrop = function (t, n) {
	//alert(""+load);	
	if(!load)
	{
		load=1;
		return;
	}
        function a(e) {
            return Math.round(e) + "px"
        }
        function f(e) {
            return r.baseClass + "-" + e
        }
        function l() {
            return e.fx.step.hasOwnProperty("backgroundColor")
        }
        function c(t) {
            var n = e(t).offset();
            return [n.left, n.top]
        }
        function h(e) {
            return [e.pageX - i[0], e.pageY - i[1]]
        }
        function p(t) {
            if (typeof t !== "object") t = {};
            r = e.extend(r, t);
            e.each(["onChange", "onSelect", "onRelease", "onDblClick"], function (e, t) {
                if (typeof r[t] !== "function") r[t] = function () {}
            })
        }
        function d(e, t, n) {
            i = c(A);
            at.setCursor(e === "move" ? e : e + "-resize");
            if (e === "move") {
                return at.activateHandlers(m(t), E, n)
            }
            var r = st.getFixed();
            var s = g(e);
            var o = st.getCorner(g(s));
            st.setPressed(st.getCorner(s));
            st.setCurrent(o);
            at.activateHandlers(v(e, r), E, n)
        }
        function v(e, t) {
            return function (n) {
                if (!r.aspectRatio) {
                    switch (e) {
                    case "e":
                        n[1] = t.y2;
                        break;
                    case "w":
                        n[1] = t.y2;
                        break;
                    case "n":
                        n[0] = t.x2;
                        break;
                    case "s":
                        n[0] = t.x2;
                        break
                    }
                } else {
                    switch (e) {
                    case "e":
                        n[1] = t.y + 1;
                        break;
                    case "w":
                        n[1] = t.y + 1;
                        break;
                    case "n":
                        n[0] = t.x + 1;
                        break;
                    case "s":
                        n[0] = t.x + 1;
                        break
                    }
                }
                st.setCurrent(n);
                ut.update()
            }
        }
        function m(e) {
            var t = e;
            ft.watchKeys();
            return function (e) {
                st.moveOffset([e[0] - t[0], e[1] - t[1]]);
                t = e;
                ut.update()
            }
        }
        function g(e) {
            switch (e) {
            case "n":
                return "sw";
            case "s":
                return "nw";
            case "e":
                return "nw";
            case "w":
                return "ne";
            case "ne":
                return "sw";
            case "nw":
                return "se";
            case "se":
                return "nw";
            case "sw":
                return "ne"
            }
        }
        function y(e) {
            return function (t) {
                if (r.disabled) {
                    return false
                }
                if (e === "move" && !r.allowMove) {
                    return false
                }
                i = c(A);
                tt = true;
                d(e, h(t));
                t.stopPropagation();
                t.preventDefault();
                return false
            }
        }
        function b(e, t, n) {
			
            var r = e.width(),
                i = e.height();
            if (r > t && t > 0) {
                r = t;
                i = t / e.width() * e.height()
            }
            if (i > n && n > 0) {
                i = n;
                r = n / e.height() * e.width()
            }
            Y = e.width() / r;
            Z = e.height() / i;
            e.width(r).height(i)
        }
        function w(e) {
			
            return {
                x: e.x * Y,
                y: e.y * Z,
                x2: e.x2 * Y,
                y2: e.y2 * Z,
                w: e.w * Y,
                h: e.h * Z
            }
        }
        function E(e) {
            var t = st.getFixed();
            if (t.w > r.minSelect[0] && t.h > r.minSelect[1]) {
                ut.enableHandles();
                ut.done()
            } else {
                ut.release()
            }
            at.setCursor(r.allowSelect ? "crosshair" : "default")
        }
        function S(e) {
            if (r.disabled) {
                return false
            }
            if (!r.allowSelect) {
                return false
            }
            tt = true;
            i = c(A);
            ut.disableHandles();
            at.setCursor("crosshair");
            var t = h(e);
            st.setPressed(t);
            ut.update();
            at.activateHandlers(x, E, e.type.substring(0, 5) === "touch");
            ft.watchKeys();
            e.stopPropagation();
            e.preventDefault();
            return false
        }
        function x(e) {
            st.setCurrent(e);
            ut.update()
        }
        function T() {
            var t = e("<div></div>").addClass(f("tracker"));
            if (o) {
                t.css({
                    opacity: 0,
                    backgroundColor: "white"
                })
            }
            return t
        }
        function lt(e) {
            F.removeClass().addClass(f("holder")).addClass(e)
        }
        function ct(e, t) {
            function b() {
                window.setTimeout(w, c)
            }
            var n = e[0] / Y,
                i = e[1] / Z,
                s = e[2] / Y,
                o = e[3] / Z;
            if (nt) {
                return
            }
            var u = st.flipCoords(n, i, s, o),
                a = st.getFixed(),
                f = [a.x, a.y, a.x2, a.y2],
                l = f,
                c = r.animationDelay,
                h = u[0] - f[0],
                p = u[1] - f[1],
                d = u[2] - f[2],
                v = u[3] - f[3],
                m = 0,
                g = r.swingSpeed;
            n = l[0];
            i = l[1];
            s = l[2];
            o = l[3];
            ut.animMode(true);
            var y;
            var w = function () {
                    return function () {
                        m += (100 - m) / g;
                        l[0] = Math.round(n + m / 100 * h);
                        l[1] = Math.round(i + m / 100 * p);
                        l[2] = Math.round(s + m / 100 * d);
                        l[3] = Math.round(o + m / 100 * v);
                        if (m >= 99.8) {
                            m = 100
                        }
                        if (m < 100) {
                            pt(l);
                            b()
                        } else {
                            ut.done();
                            ut.animMode(false);
                            if (typeof t === "function") {
                                t.call(Ct)
                            }
                        }
                    }
                }();
            b()
        }
        function ht(e) {
            pt([e[0] / Y, e[1] / Z, e[2] / Y, e[3] / Z]);
            r.onSelect.call(Ct, w(st.getFixed()));
            ut.enableHandles()
        }
        function pt(e) {
            st.setPressed([e[0], e[1]]);
            st.setCurrent([e[2], e[3]]);
            ut.update()
        }
        function dt() {
            return w(st.getFixed())
        }
        function vt() {
            return st.getFixed()
        }
        function mt(e) {
            p(e);
            Nt()
        }
        function gt() {
            r.disabled = true;
            ut.disableHandles();
            ut.setCursor("default");
            at.setCursor("default")
        }
        function yt() {
            r.disabled = false;
            Nt()
        }
        function bt() {
            ut.done();
            at.activateHandlers(null, null)
        }
        function wt() {
            F.remove();
            C.show();
            C.css("visibility", "visible");
            e(t).removeData("Jcrop")
        }
        function Et(e, t) {
            alert("inside  setImage");
            ut.release();
            gt();
            var n = new Image;
            n.onload = function () {
                var i = n.width;
                var s = n.height;
                var o = r.boxWidth;
                var u = r.boxHeight;
                A.width(i).height(s);
                A.attr("src", e);
                I.attr("src", e);
                b(A, o, u);
                B = A.width();
                j = A.height();
                I.width(B).height(j);
                W.width(B + z * 2).height(j + z * 2);
                F.width(B).height(j);
                ot.resize(B, j);
                yt();
                alert("Done  setImage");
                if (typeof t === "function") {
                    t.call(Ct)
                }
            };
            n.src = e
        }
        function St(e, t) {
            B = e;
            j = t;
            yt()
        }
        function xt(e) {
            r.aspectRatio = e
        }
        function Tt(e, t, n) {
            var i = t || r.bgColor;
            if (r.bgFade && l() && r.fadeTime && !n) {
                e.animate({
                    backgroundColor: i
                }, {
                    queue: false,
                    duration: r.fadeTime
                })
            } else {
                e.css("backgroundColor", i)
            }
        }
        function Nt(e) {
            if (r.allowResize) {
                if (e) {
                    ut.enableOnly()
                } else {
                    ut.enableHandles()
                }
            } else {
                ut.disableHandles()
            }
            at.setCursor(r.allowSelect ? "crosshair" : "default");
            ut.setCursor(r.allowMove ? "move" : "default");
            if (r.hasOwnProperty("trueSize")) {
                Y = r.trueSize[0] / B;
                Z = r.trueSize[1] / j
            }
            if (r.hasOwnProperty("setSelect")) {
                ht(r.setSelect);
                ut.done();
                delete r.setSelect
            }
            ot.refresh();
            if (r.bgColor != X) {
                Tt(r.shade ? ot.getShades() : F, r.shade ? r.shadeColor || r.bgColor : r.bgColor);
                X = r.bgColor
            }
            if (V != r.bgOpacity) {
                V = r.bgOpacity;
                if (r.shade) ot.refresh();
                else ut.setBgOpacity(V)
            }
            J = r.maxSize[0] || 0;
            K = r.maxSize[1] || 0;
            Q = r.minSize[0] || 0;
            G = r.minSize[1] || 0;
            if (r.hasOwnProperty("outerImage")) {
                A.attr("src", r.outerImage);
                delete r.outerImage
            }
            ut.refresh()
        }
        var r = e.extend({}, e.Jcrop.defaults),
            i, s = navigator.userAgent.toLowerCase(),
            o = /msie/.test(s),
            u = /msie [1-6]\./.test(s);
        if (typeof t !== "object") {
            t = e(t)[0]
        }
        if (typeof n !== "object") {
            n = {}
        }
        p(n);
        var N = {
            border: "none",
            visibility: "visible",
            margin: 0,
            padding: 0,
            position: "absolute",
            top: 0,
            left: 0
        };
        var C = e(t),
            k = true;
        if (t.tagName == "IMG") {
            if (C[0].width != 0 && C[0].height != 0) {
                C.width(C[0].width);
                C.height(C[0].height)
            } else {
                var L = new Image;
                L.src = C[0].src;
                C.width(L.width);
                C.height(L.height)
            }
            var A = C.clone().css(N).show();
            C.removeAttr("id").removeAttr("name");
            var O = C.width();
            var M = C.height();
            var _ = 300;
            var D = _ * O / M;
            D = Math.ceil(D);
            A.width(D);
            A.height(_);
            C.after(A).hide()
        } else {
            A = C.css(N).show();
            k = false;
            if (r.shade === null) {
                r.shade = true
            }
        }
        b(A, r.boxWidth, r.boxHeight);
        var P = document.getElementById("holderid1");
        var H = "holderid1";
        if (P != null)
		{
		H = "holderid2";
		}
        var B = A.width(),
            j = A.height(),
            F = e("<div />").width(B).attr("id", H).height(j).addClass(f("holder")).css({
                position: "relative",
                backgroundColor: r.bgColor
            }).insertAfter(C).append(A);
        if (P == null && r.addClass) {
            F.addClass(r.addClass)
        }
        var I = e("<div />"),
            q = e("<div />").width("100%").height("100%").css({
                zIndex: 310,
                position: "absolute",
                overflow: "hidden"
            }),
            R = e("<div />").width("100%").attr("id", "disboxid1").height("100%").css("zIndex", 320),
            U = e("<div />").attr("id", "disboxid2").css({
                position: "absolute",
                zIndex: 600
            }).dblclick(function () {
                var e = st.getFixed();
                r.onDblClick.call(Ct, e)
            }).insertBefore(A).append(q, R);
        if (k) {
            I = e("<img />").attr("src", A.attr("src")).attr("id", "cropbox2").attr("id", "cropbox2").css(N).width(B).height(j), q.append(I)
        }
        if (u) {
            U.css({
                overflowY: "hidden"
            })
        }
        var z = r.boundary;
        var W = T().width(B + z * 2).attr("id", "trackid1").height(j + z * 2).css({
            position: "absolute",
            top: a(-z),
            left: a(-z),
            zIndex: 290
        }).mousedown(S);
        var X = r.bgColor,
            V = r.bgOpacity,
            J, K, Q, G, Y, Z, et = true,
            tt, nt, rt;
        i = c(A);
        var it = function () {
                function e() {
                    var e = {},
                        t = ["touchstart", "touchmove", "touchend"],
                        n = document.createElement("div"),
                        r;
                    try {
                        for (r = 0; r < t.length; r++) {
                            var i = t[r];
                            i = "on" + i;
                            var s = i in n;
                            if (!s) {
                                n.setAttribute(i, "return;");
                                s = typeof n[i] == "function"
                            }
                            e[t[r]] = s
                        }
                        return e.touchstart && e.touchend && e.touchmove
                    } catch (o) {
                        return false
                    }
                }
                function t() {
                    if (r.touchSupport === true || r.touchSupport === false) return r.touchSupport;
                    else return e()
                }
                return {
                    createDragger: function (e) {
                        return function (t) {
                            if (r.disabled) {
                                return false
                            }
                            if (e === "move" && !r.allowMove) {
                                return false
                            }
                            i = c(A);
                            tt = true;
                            d(e, h(it.cfilter(t)), true);
                            t.stopPropagation();
                            t.preventDefault();
                            return false
                        }
                    },
                    newSelection: function (e) {
                        return S(it.cfilter(e))
                    },
                    cfilter: function (e) {
                        e.pageX = e.originalEvent.changedTouches[0].pageX;
                        e.pageY = e.originalEvent.changedTouches[0].pageY;
                        return e
                    },
                    isSupported: e,
                    support: t()
                }
            }();
        var st = function () {
                function u(r) {
                    r = p(r);
                    n = e = r[0];
                    i = t = r[1]
                }
                function a(e) {
                    e = p(e);
                    s = e[0] - n;
                    o = e[1] - i;
                    n = e[0];
                    i = e[1]
                }
                function f() {
                    return [s, o]
                }
                function l(r) {
                    var s = r[0],
                        o = r[1];
                    if (0 > e + s) {
                        s -= s + e
                    }
                    if (0 > t + o) {
                        o -= o + t
                    }
                    if (j < i + o) {
                        o += j - (i + o)
                    }
                    if (B < n + s) {
                        s += B - (n + s)
                    }
                    e += s;
                    n += s;
                    t += o;
                    i += o
                }
                function c(e) {
                    var t = h();
                    switch (e) {
                    case "ne":
                        return [t.x2, t.y];
                    case "nw":
                        return [t.x, t.y];
                    case "se":
                        return [t.x2, t.y2];
                    case "sw":
                        return [t.x, t.y2]
                    }
                }
                function h() {
                    if (!r.aspectRatio) {
                        return v()
                    }
                    var s = r.aspectRatio,
                        o = r.minSize[0] / Y,
                        u = r.maxSize[0] / Y,
                        a = r.maxSize[1] / Z,
                        f = n - e,
                        l = i - t,
                        c = Math.abs(f),
                        h = Math.abs(l),
                        p = c / h,
                        g, y, b, w;
                    if (u === 0) {
                        u = B * 10
                    }

                    if (a === 0) {
                        a = j * 10
                    }
                    if (p < s) {
                        y = i;
                        b = h * s;
                        g = f < 0 ? e - b : b + e;
                        if (g < 0) {
                            g = 0;
                            w = Math.abs((g - e) / s);
                            y = l < 0 ? t - w : w + t
                        } else if (g > B) {
                            g = B;
                            w = Math.abs((g - e) / s);
                            y = l < 0 ? t - w : w + t
                        }
                    } else {
                        g = n;
                        w = c / s;
                        y = l < 0 ? t - w : t + w;
                        if (y < 0) {
                            y = 0;
                            b = Math.abs((y - t) * s);
                            g = f < 0 ? e - b : b + e
                        } else if (y > j) {
                            y = j;
                            b = Math.abs(y - t) * s;
                            g = f < 0 ? e - b : b + e
                        }
                    }
                    if (g > e) {
                        if (g - e < o) {
                            g = e + o
                        } else if (g - e > u) {
                            g = e + u
                        }
                        if (y > t) {
                            y = t + (g - e) / s
                        } else {
                            y = t - (g - e) / s
                        }
                    } else if (g < e) {
                        if (e - g < o) {
                            g = e - o
                        } else if (e - g > u) {
                            g = e - u
                        }
                        if (y > t) {
                            y = t + (e - g) / s
                        } else {
                            y = t - (e - g) / s
                        }
                    }
                    if (g < 0) {
                        e -= g;
                        g = 0
                    } else if (g > B) {
                        e -= g - B;
                        g = B
                    }
                    if (y < 0) {
                        t -= y;
                        y = 0
                    } else if (y > j) {
                        t -= y - j;
                        y = j
                    }
                    return m(d(e, t, g, y))
                }
                function p(e) {
                    if (e[0] < 0) e[0] = 0;
                    if (e[1] < 0) e[1] = 0;
                    if (e[0] > B) e[0] = B;
                    if (e[1] > j) e[1] = j;
                    return [Math.round(e[0]), Math.round(e[1])]
                }
                function d(e, t, n, r) {
                    var i = e,
                        s = n,
                        o = t,
                        u = r;
                    if (n < e) {
                        i = n;
                        s = e
                    }
                    if (r < t) {
                        o = r;
                        u = t
                    }
                    return [i, o, s, u]
                }
                function v() {
                    var r = n - e,
                        s = i - t,
                        o;
                    if (J && Math.abs(r) > J) {
                        n = r > 0 ? e + J : e - J
                    }
                    if (K && Math.abs(s) > K) {
                        i = s > 0 ? t + K : t - K
                    }
                    if (G / Z && Math.abs(s) < G / Z) {
                        i = s > 0 ? t + G / Z : t - G / Z
                    }
                    if (Q / Y && Math.abs(r) < Q / Y) {
                        n = r > 0 ? e + Q / Y : e - Q / Y
                    }
                    if (e < 0) {
                        n -= e;
                        e -= e
                    }
                    if (t < 0) {
                        i -= t;
                        t -= t
                    }
                    if (n < 0) {
                        e -= n;
                        n -= n
                    }
                    if (i < 0) {
                        t -= i;
                        i -= i
                    }
                    if (n > B) {
                        o = n - B;
                        e -= o;
                        n -= o
                    }
                    if (i > j) {
                        o = i - j;
                        t -= o;
                        i -= o
                    }
                    if (e > B) {
                        o = e - j;
                        i -= o;
                        t -= o
                    }
                    if (t > j) {
                        o = t - j;
                        i -= o;
                        t -= o
                    }
                    return m(d(e, t, n, i))
                }
                function m(e) {
                    return {
                        x: e[0],
                        y: e[1],
                        x2: e[2],
                        y2: e[3],
                        w: e[2] - e[0],
                        h: e[3] - e[1]
                    }
                }
                var e = 0,
                    t = 0,
                    n = 0,
                    i = 0,
                    s, o;
                return {
                    flipCoords: d,
                    setPressed: u,
                    setCurrent: a,
                    getOffset: f,
                    moveOffset: l,
                    getCorner: c,
                    getFixed: h
                }
            }();
        var ot = function () {
                function s(e, t) {
                    i.left.css({
                        height: a(t)
                    });
                    i.right.css({
                        height: a(t)
                    })
                }
                function o() {
                    return u(st.getFixed())
                }
                function u(e) {
                    i.top.css({
                        left: a(e.x),
                        width: a(e.w),
                        height: a(e.y)
                    });
                    i.bottom.css({
                        top: a(e.y2),
                        left: a(e.x),
                        width: a(e.w),
                        height: a(j - e.y2)
                    });
                    i.right.css({
                        left: a(e.x2),
                        width: a(B - e.x2)
                    });
                    i.left.css({
                        width: a(e.x)
                    })
                }
                function f() {
                    return e("<div />").css({
                        position: "absolute",
                        backgroundColor: r.shadeColor || r.bgColor
                    }).appendTo(n)
                }
                function l() {
                    if (!t) {
                        t = true;
                        n.insertBefore(A);
                        o();
                        ut.setBgOpacity(1, 0, 1);
                        I.hide();
                        c(r.shadeColor || r.bgColor, 1);
                        if (ut.isAwake()) {
                            p(r.bgOpacity, 1)
                        } else p(1, 1)
                    }
                }
                function c(e, t) {
                    Tt(v(), e, t)
                }
                function h() {
                    if (t) {
                        n.remove();
                        I.show();
                        t = false;
                        if (ut.isAwake()) {
                            ut.setBgOpacity(r.bgOpacity, 1, 1)
                        } else {
                            ut.setBgOpacity(1, 1, 1);
                            ut.disableHandles()
                        }
                        Tt(F, 0, 1)
                    }
                }
                function p(e, i) {
                    if (t) {
                        if (r.bgFade && !i) {
                            n.animate({
                                opacity: 1 - e
                            }, {
                                queue: false,
                                duration: r.fadeTime
                            })
                        } else n.css({
                            opacity: 1 - e
                        })
                    }
                }
                function d() {
                    r.shade ? l() : h();
                    if (ut.isAwake()) p(r.bgOpacity)
                }
                function v() {
                    return n.children()
                }
                var t = false,
                    n = e("<div />").css({
                        position: "absolute",
                        zIndex: 240,
                        opacity: 0
                    }),
                    i = {
                        top: f(),
                        left: f().height(j),
                        right: f().height(j),
                        bottom: f()
                    };
                return {
                    update: o,
                    updateRaw: u,
                    getShades: v,
                    setBgColor: c,
                    enable: l,
                    disable: h,
                    resize: s,
                    refresh: d,
                    opacity: p
                }
            }();
        var ut = function () {
                function l(t) {
                    var n = e("<div />").css({
                        position: "absolute",
                        opacity: r.borderOpacity
                    }).addClass(f(t));
                    q.append(n);
                    return n
                }
                function c(t, n) {
                    var r = e("<div />").mousedown(y(t)).css({
                        cursor: t + "-resize",
                        position: "absolute",
                        zIndex: n
                    }).addClass("ord-" + t);
                    if (it.support) {
                        r.bind("touchstart.jcrop", it.createDragger(t))
                    }
                    R.append(r);
                    return r
                }
                function h(e) {
                    var t = r.handleSize,
                        i = c(e, n++).css({
                            opacity: r.handleOpacity
                        }).addClass(f("handle"));
                    if (t) {
                        i.width(t).height(t)
                    }
                    return i
                }
                function p(e) {
                    return c(e, n++).addClass("jcrop-dragbar")
                }
                function d(e) {
                    var t;
                    for (t = 0; t < e.length; t++) {
                        o[e[t]] = p(e[t])
                    }
                }
                function v(e) {
                    var t, n;
                    for (n = 0; n < e.length; n++) {
                        switch (e[n]) {
                        case "n":
                            t = "hline";
                            break;
                        case "s":
                            t = "hline bottom";
                            break;
                        case "e":
                            t = "vline right";
                            break;
                        case "w":
                            t = "vline";
                            break
                        }
                        i[e[n]] = l(t)
                    }
                }
                function m(e) {
                    var t;
                    for (t = 0; t < e.length; t++) {
                        s[e[t]] = h(e[t])
                    }
                }
                function g(e, t) {
                    if (!r.shade) {
                        I.css({
                            top: a(-t),
                            left: a(-e)
                        })
                    }
                    U.css({
                        top: a(t),
                        left: a(e)
                    })
                }
                function b(e, t) {
				    U.width(Math.round(e)).height(Math.round(t))
                }
                function E() {
                    var e = st.getFixed();
                    st.setPressed([e.x, e.y]);
                    st.setCurrent([e.x2, e.y2]);
                    S()
                }
                function S(e) {
                    if (t) {
                        return x(e)
                    }
                }
                function x(e) {
                    var n = st.getFixed();
                    b(n.w, n.h);
                    g(n.x, n.y);
                    if (r.shade) ot.updateRaw(n);
                    t || C();
                    if (e) {
                        r.onSelect.call(Ct, w(n))
                    } else {
                        r.onChange.call(Ct, w(n))
                    }
                }
                function N(e, n, i) {
                    if (!t && !n) return;
                    if (r.bgFade && !i) {
                        A.animate({
                            opacity: e
                        }, {
                            queue: false,
                            duration: r.fadeTime
                        })
                    } else {
                        A.css("opacity", e)
                    }
                }
                function C() {
                    U.show();
                    if (r.shade) ot.opacity(V);
                    else N(V, true);
                    t = true
                }
                function k() {
                    M();
                    U.hide();
                    if (r.shade) ot.opacity(1);
                    else N(1);
                    t = false;
                    r.onRelease.call(Ct)
                }
                function L() {
                    if (u) {
                        R.show()
                    }
                }
                function O() {
                    u = true;
                    if (r.allowResize) {
                        R.show();
                        return true
                    }
                }
                function M() {
                    u = false;
                    R.hide()
                }
                function _(e) {
                    if (e) {
                        nt = true;
                        M()
                    } else {
                        nt = false;
                        O()
                    }
                }
                function D() {
                    _(false);
                    E()
                }
                var t, n = 370,
                    i = {},
                    s = {},
                    o = {},
                    u = false;
                if (r.dragEdges && e.isArray(r.createDragbars)) d(r.createDragbars);
                if (e.isArray(r.createHandles)) m(r.createHandles);
                if (r.drawBorders && e.isArray(r.createBorders)) v(r.createBorders);
                e(document).bind("touchstart.jcrop-ios", function (t) {
                    if (e(t.currentTarget).hasClass("jcrop-tracker")) t.stopPropagation()
                });
                var P = T().mousedown(y("move")).css({
                    cursor: "move",
                    position: "absolute",
                    zIndex: 360
                });
                if (it.support) {
                    P.bind("touchstart.jcrop", it.createDragger("move"))
                }
                q.append(P);
                M();
                return {
                    updateVisible: S,
                    update: x,
                    release: k,
                    refresh: E,
                    isAwake: function () {
                        return t
                    },
                    setCursor: function (e) {
                        P.css("cursor", e)
                    },
                    enableHandles: O,
                    enableOnly: function () {
                        u = true
                    },
                    showHandles: L,
                    disableHandles: M,
                    animMode: _,
                    setBgOpacity: N,
                    done: D
                }
            }();
        var at = function () {
                function s(t) {
                    W.css({
                        zIndex: 450
                    });
                    if (t) e(document).bind("touchmove.jcrop", l).bind("touchend.jcrop", c);
                    else if (i) e(document).bind("mousemove.jcrop", u).bind("mouseup.jcrop", a)
                }
                function o() {
                    W.css({
                        zIndex: 290
                    });
                    e(document).unbind(".jcrop")
                }
                function u(e) {
                    t(h(e));
                    return false
                }
                function a(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (tt) {
                        tt = false;
                        n(h(e));
                        if (ut.isAwake()) {
                            r.onSelect.call(Ct, w(st.getFixed()))
                        }
                        o();
                        t = function () {};
                        n = function () {}
                    }
                    return false
                }
                function f(e, r, i) {
                    tt = true;
                    t = e;
                    n = r;
                    s(i);
                    return false
                }
                function l(e) {
                    t(h(it.cfilter(e)));
                    return false
                }
                function c(e) {
                    return a(it.cfilter(e))
                }
                function p(e) {
                    W.css("cursor", e)
                }
                var t = function () {},
                    n = function () {},
                    i = r.trackDocument;
                if (!i) {
                    W.mousemove(u).mouseup(a).mouseout(a)
                }
                A.before(W);
                return {
                    activateHandlers: f,
                    setCursor: p
                }
            }();
        var ft = function () {
                function i() {
                    if (r.keySupport) {
                        t.show();
                        t.focus()
                    }
                }
                function s(e) {
                    t.hide()
                }
                function o(e, t, n) {
                    if (r.allowMove) {
                        st.moveOffset([t, n]);
                        ut.updateVisible(true)
                    }
                    e.preventDefault();
                    e.stopPropagation()
                }
                function a(e) {
                    if (e.ctrlKey || e.metaKey) {
                        return true
                    }
                    rt = e.shiftKey ? true : false;
                    var t = rt ? 10 : 1;
                    switch (e.keyCode) {
                    case 37:
                        o(e, -t, 0);
                        break;
                    case 39:
                        o(e, t, 0);
                        break;
                    case 38:
                        o(e, 0, -t);
                        break;
                    case 40:
                        o(e, 0, t);
                        break;
                    case 27:
                        if (r.allowSelect) ut.release();
                        break;
                    case 9:
                        return true
                    }
                    return false
                }
                var t = e('<input type="radio" />').css({
                    position: "fixed",
                    left: "-120px",
                    width: "12px"
                }).addClass("jcrop-keymgr"),
                    n = e("<div />").css({
                        position: "absolute",
                        overflow: "hidden"
                    }).append(t);
                if (r.keySupport) {
                    t.keydown(a).blur(s);
                    if (u || !r.fixedSupport) {
                        t.css({
                            position: "absolute",
                            left: "-20px"
                        });
                        n.append(t).insertBefore(A)
                    } else {
                        t.insertBefore(A)
                    }
                }
                return {
                    watchKeys: i
                }
            }();
        if (it.support) W.bind("touchstart.jcrop", it.newSelection);
        R.hide();
        Nt(true);
        var Ct = {
            setImage: Et,
            animateTo: ct,
            setNewImage: St,
            setRatio: xt,
            setSelect: ht,
            setOptions: mt,
            tellSelect: dt,
            tellScaled: vt,
            setClass: lt,
            disable: gt,
            enable: yt,
            cancel: bt,
            release: ut.release,
            destroy: wt,
            focus: ft.watchKeys,
            getBounds: function () {
                return [B * Y, j * Z]
            },
            getWidgetSize: function () {
                return [B, j]
            },
            getScaleFactor: function () {
                return [Y, Z]
            },
            getOptions: function () {
                return r
            },
            ui: {
                holder: F,
                selection: U
            }
        };
        if (o) F.bind("selectstart", function () {
            return false
        });
        C.data("Jcrop", Ct);
        return Ct
    };
    e.fn.Jcrop = function (t, n) {
        var r;
        this.each(function () {
            if (e(this).data("Jcrop")) {
                if (t === "api") return e(this).data("Jcrop");
                else e(this).data("Jcrop").setOptions(t)
            } else {
                if (this.tagName == "IMG") e.Jcrop.Loader(this, function () {
                    e(this).css({
                        display: "block",
                        visibility: "hidden"
                    });
                    r = e.Jcrop(this, t);
                    if (e.isFunction(n)) n.call(r)
                });
                else {
                    e(this).css({
                        display: "block",
                        visibility: "hidden"
                    });
                    r = e.Jcrop(this, t);
                    if (e.isFunction(n)) n.call(r)
                }
            }
        });
        return this
    };
    e.Jcrop.Loader = function (t, n, r) {
        function o() {
            if (s.complete) {
                i.unbind(".jcloader");
                if (e.isFunction(n)) n.call(s)
            } else window.setTimeout(o, 50)
        }
        var i = e(t),
            s = i[0];
        i.bind("load.jcloader", o).bind("error.jcloader", function (t) {
            i.unbind(".jcloader");
            if (e.isFunction(r)) r.call(s)
        });
        if (s.complete && e.isFunction(n)) {
            i.unbind(".jcloader");
            n.call(s)
        }
    };
    e.Jcrop.defaults = {
        allowSelect: true,
        allowMove: true,
        allowResize: true,
        trackDocument: true,
        baseClass: "jcrop",
        addClass: null,
        bgColor: "black",
        bgOpacity: .6,
        bgFade: false,
        borderOpacity: .4,
        handleOpacity: .5,
        handleSize: null,
        aspectRatio: 0,
        keySupport: true,
        createHandles: ["n", "s", "e", "w", "nw", "ne", "se", "sw"],
        createDragbars: ["n", "s", "e", "w"],
        createBorders: ["n", "s", "e", "w"],
        drawBorders: true,
        dragEdges: true,
        fixedSupport: true,
        touchSupport: null,
        shade: null,
        boxWidth: 0,
        boxHeight: 0,
        boundary: 2,
        fadeTime: 400,
        animationDelay: 20,
        swingSpeed: 3,
        minSelect: [0, 0],
        maxSize: [0, 0],
        minSize: [0, 0],
        onChange: function () {},
        onSelect: function () {},
        onDblClick: function () {},
        onRelease: function () {}
    }
})(jQuery)